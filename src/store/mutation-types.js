export const ITEMPURPOSE = 'ITEMPURPOSE' // 
export const YYZVER = 'YYZVER' // 
export const IS_EDITING_STYLE = 'IS_EDITING_STYLE' // 小店自定义开启
export const USER_INFO = 'USER_INFO' // 用户信息
export const MANAGER_INFO = 'MANAGER_INFO' // 客户经理信息
export const POSTER_INFO = 'POSTER_INFO' // 海报信息
export const NAV_INFO = 'NAV_INFO' // 导航栏信息
export const SCENE = 'SCENE' // 导航栏信息
export const LOADING = 'LOADING' // 导航栏信息
export const TOOL_POSTER_INFO = "TOOL_POSTER_INFO" //工具栏海报信息

export const SENDMATERIALINFO = 'SENDMATERIALINFO' // 导航栏信息

export const SHOPINFO = 'SHOPINFO' // 导航栏信息

export const CUSTOMER_INFO = 'CUSTOMER_INFO' // 客户信息

export const MATERIAL_RECORD = 'MATERIAL_RECORD' // 四类素材客户访问记录

export const PREVIEWDOC_LIST = 'PREVIEWDOC_LIST' // 四类素材客户访问记录

export const COMPONENT_STATE = 'COMPONENT_STATE' // 个性化小店组件状态


export const COMPONENT_STATE_BACKUP = 'COMPONENT_STATE_BACKUP' // 个性化小店组件状态 暂存

export const WX_AUTH_STATUS = 'WX_AUTH_STATUS' // 小店授权状态

export const FKYB_INFO = 'FKYB_INFO' //风口研报授权信息
export const USER_SHARE_NAME = 'USER_SHARE_NAME' // 客户分享名字

export const TRADE_DAY = 'TRADE_DAY'

