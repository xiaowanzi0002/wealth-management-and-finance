// 新发资管产品详情页
import Vue from 'vue'
import Vuex from 'vuex'
import base from '@/vuex/base/index'
import productDetails from '@/vuex/common/productDetails/index'
import prodDetail from '@/api/mall/prodDetail'
import entrustModule from '@/vuex/prodDetail/modules/entrust.vuex'

Vue.use(Vuex)

const actions = {
  getZgProDescription ({commit, state}, param) {
    return prodDetail.getZgProDescription(
      state.productDetails.prodCode,
      param.prodId,
      param.prodType,
      param.prodTypeChild
    ).then(function (res) {
      let data = res.data
      return data
    })
  },
  getInvestorRight () {
    return prodDetail.getInvestorRight().then(function (res) {
      let data = res.data
      return data
    })
  },
  getZgRateStructure ({commit, state}, prodId) {
    return prodDetail.getZgRateStructure(state.productDetails.prodCode, prodId).then(function (res) {
      let data = res.data
      return data
    })
  }
}

export default new Vuex.Store({
  actions,
  modules: {
    base,
    productDetails,
    entrustModule
  }
})
