// 银行理财、收益凭证、信托、新发资管协议页
import Vue from 'vue'
import Vuex from 'vuex'
import prodDetail from '@/api/mall/prodDetail'

Vue.use(Vuex)

const actions = {
  // 合同协议
  getProtocol ({commit, state}, proTocol) {
    return prodDetail.getProtocol(proTocol.prodCode, proTocol.prodId).then(function (res) {
      let data = res.data
      if (data.success !== false) {}
      return data
    })
  }
}

export default new Vuex.Store({
  actions
})
