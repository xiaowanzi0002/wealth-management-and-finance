// 产品详情页
import Vue from 'vue'
import Vuex from 'vuex'
import base from '../base/index'
import productDetails from '../common/productDetails/index'
import homeModule from '@/vuex/common/modules/home.vuex.js'
import fundModule from '@/vuex/prodDetail/modules/fund.vuex.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    base,
    productDetails,
    homeModule,
    fundModule
  }
})
