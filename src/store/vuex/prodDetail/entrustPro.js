// 产品详情页
import Vue from 'vue'
import Vuex from 'vuex'
import base from '../base/index'
import productDetails from '../common/productDetails/index'
import entrustModule from '@/vuex/prodDetail/modules/entrust.vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    base,
    productDetails,
    entrustModule
  }
})
