// 实时估值误差页
import Vue from 'vue'
import Vuex from 'vuex'
import prodDetail from '@/api/funds/prodDetail'

Vue.use(Vuex)

const actions = {
  getFundNewstNavErrors ({commit, state}, navPar) {
    return prodDetail.getFundNewstNavError(navPar.prodCode).then(function (res) {
	  console.log(res)
      let data = res.data
      return data
    })
  }
}

export default new Vuex.Store({
  actions
})
