// 收益凭证、银行理财、信托产品详情页vuex
import prodDetail from '@/api/mall/prodDetail'

const PRODTYPE = 'PRODTYPE'
const PRODTYPECHILD = 'PRODTYPECHILD'
const PRODSPONSOR = 'PRODSPONSOR'
const PRODID = 'PRODID'

const state = {
  prodType: '',
  prodTypeChild: '',
  prodSponsor: '',
  prodId: ''
}

const getters = {
  prodType: state => {
    return state.prodType
  },
  prodId: state => {
    return state.prodId
  },
  prodTypeChild: state => {
    return state.prodTypeChild
  },
  prodSponsor: state => {
    return state.prodSponsor
  }
}

const mutations = {
  [PRODTYPE] (state, {prodType}) {
    state.prodType = prodType
  },
  [PRODTYPECHILD] (state, {prodTypeChild}) {
    state.prodTypeChild = prodTypeChild
  },
  [PRODSPONSOR] (state, {prodSponsor}) {
    state.prodSponsor = prodSponsor
  },
  [PRODID] (state, {prodId}) {
    state.prodId = prodId
  }
}

const actions = {
  /** commonBtn.vue */
  // 人工响应
  getClientResponser () {
    return prodDetail.getClientResponser().then(function (res) {
      let data = res.data
      return data
    })
  },
  /** commonBtn.vue end */
  recordLogOfGDTC () {
    return prodDetail.recordLogOfGDTC('HGTZZCN')
  },
  getHistoryNavs ({commit, state}, historyPar) {
    return prodDetail.getHistoryNav(historyPar.prodCode, historyPar.prodId, historyPar.retRows, historyPar.prodType).then(function (res) {
      let data = res.data
      return data
    })
  },
  // 查询产品简介和产品交易规则
  getTradeRule ({commit, state}, prodCode) {
    if (state.prodType === '3') {
      state.prodSponsor = ''
    }
    return prodDetail.getTradeRule(prodCode, state.prodType, state.prodTypeChild, state.prodSponsor).then(function (res) {
      let data = res.data
      if (data.success !== false) {}
      return data
    })
  },
  commonGetTradeRule ({commit, state}, params) {
    return prodDetail.getTradeRule(
      params.prodCode,
      params.prodType,
      params.prodTypeChild,
      params.prodSponsor
    ).then(res => {
      let data = res.data
      return data
    })
  },
  // 获取产品剩余额度
  getRemainAmount ({commit, state}, prodCode) {
    return prodDetail.getRemainAmount(prodCode, state.prodType, state.prodTypeChild).then(function (res) {
      let data = res.data
      if (data.success !== false) {}
      return data
    })
  }
}

export default {
  actions,
  getters,
  state,
  mutations
}
