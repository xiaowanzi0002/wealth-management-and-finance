// 基金产品详情页vuex action
import prodDetail from '@/api/funds/prodDetail.js'
import fundIndex from '@/api/funds/fundIndex.js'
import customizationFund from '@/api/funds/customizationFund.js'
 var tagObj={yyzver:'1.0.1',
		   itemPurpose:'1'}  
const actions = {
	/** newFund 新发基金 */
	  // getProdDetails ({commit, state}, params) {
		 // params = {
		 //   walletCode: '',
		 //   outWalletCode:'',
		 //   tgljInfo: 'tglj_channel:lcpd;tglj_userCode:38765500;tglj_timeStamp:16003103264',//global.MGC.queryUriSingleParam('tglj_info'),
		 //   prodCode: '350004',//state.prodCode,
		 //   sourceFlag: ''
		 // }
	  //   return fundIndex.getProdDetails(params.prodCode, params.sourceFlag, params.walletCode, params.outWalletCode, params.tgljInfo).then(function (res) {
		 //  let data = res
	  //     if (data.success !== false) {
	  //       commit('PRODDATA', {prodData: data})
	  //     }
	  //     return data
	  //   })
	  // },
	  // getMarketPosition ({ commit, state }, params) {
	  //   return fundIndex.getMarketPosition(params.prodCode, params.effectSceneType).then(function (res) {
	  //     return res.data
	  //   })
	  // },
	  getProdNotice ({commit, state}, params) {
	    return prodDetail.getProdNotice(params.prodCode, params.noticeType, params.pageSize, params.pageNum).then(function (res) {
	      return res
	    })
	  },
	/**小店首页产品fund改版
	 * ***/
	   // new T2800001
	   getImagesStyleInfo ({commit, state}, params) {
	     return fundIndex.getImagesStyleInfo(params.type, tagObj.itemPurpose, tagObj.yyzver, params.channelCode).then(res => {
	       let data = res['imagesStyleInfo'].sort((x, y) => {
	         return parseInt(y.PRIORITY) - parseInt(x.PRIORITY)
	       })
	       return data
	     })
	   },
	  // new T2800004
	  
	  getProdListInfoWithLight ({commit, state}, params) {
	    return fundIndex.getProdListInfoWithLight(params.type, tagObj.itemPurpose, tagObj.yyzver, params.channelCode).then(res => {
	      let data = res['list'].sort((x, y) => {
	        return parseInt(y.PRIORITY) - parseInt(x.PRIORITY)
	      })
	      return data
	    })
	  },
	    // new T2800003
	    getAreaStructureInfo ({commit, state}, params) {
	      return fundIndex.getAreaStructureInfo(params.type, tagObj.itemPurpose, tagObj.yyzver, params.channelCode).then(res => {
	        let data = res['areaStructureInfo'].sort((x, y) => {
	          return parseInt(y.PRIORITY) - parseInt(x.PRIORITY)
	        })
	        return data
	      })
	    },
	  // new T2800007
	  getOptimalCombinationInfo({commit, state}, params) {
	    return fundIndex.getOptimalCombinationInfo(params.type, tagObj.itemPurpose, tagObj.yyzver, params.channelCode).then(res => {
	      let data = res['list'].sort((x, y) => {
	        return parseInt(y.PRIORITY) - parseInt(x.PRIORITY)
	      })
	      return data
	    })
	  },
  /** commonBtn.vue */
  getCfhEnter ({commit, state}, managerId) {
    return prodDetail.getCfhEnter(managerId).then(function (res) {
      let data = res.data
      return data
    })
  },
  // 自选基金管理
  selfFundManage ({commit, state}, params) {
    return customizationFund.selfFundManage(params).then(function (res) {
      let data = res.data
      return data
    })
  },
  // 自选基金查询
  querySelfFund ({commit, state}, params) {
    return customizationFund.querySelfFund(params.groupNo, params.acctCode).then(function (res) {
      let data = res.data
      return data
    })
  },
  /** commonBtn.vue end */


  
  
  getFundRate ({commit, state}, params) {
    return prodDetail.getFundRate(params.prodCode, params.prodId, params.refSta).then(function (res) {
      let data = res
      return data
    })
  },
  /** newFund 新发基金 end */

  /** gmProDetail 公募基金 cashDetail现金宝详情页 */
  getBenchMarkIndex ({commit, state}, prodCode) {
    return prodDetail.getBenchMarkIndex(prodCode).then(function (res) {
      return res
    })
  },
  getChartDatas ({commit, state}, jjjlPar) {
    return prodDetail.getChartData(jjjlPar.prodCode, jjjlPar.prodId, jjjlPar.retRows, jjjlPar.prodType, jjjlPar.flag, jjjlPar.prodCd, 'YJZS', jjjlPar.endDate).then(function (res) {
      let data = res
      return data
    })
  },
  getProdManagers ({commit, state}, jjjlPar) {
    return prodDetail.getProdManager(jjjlPar.prodCode, jjjlPar.prodId, jjjlPar.prodType).then(function (res) {
      let data = res
      return data
    })
  },
  getTradeTimeLine () {
    return prodDetail.getTradeTimeLine().then(function (res) {
      let data = res
      if (data.success !== false) {}
      return data
    })
  },
  getRadarInfos ({commit, state}, radarPar) {
    return prodDetail.getRadarInfo(radarPar.prodCode, radarPar.investType).then(function (res) {
      let data = res
      return data
    })
  },
  getRadarInfoAll ({commit, state}, radarPar) {
    return prodDetail.getRadarInfoAll(radarPar.prodCode, radarPar.investValue).then(function (res) {
      let data = res
      return data
    })
  },
  getBuyAndSellPoints ({commit, state}, mmPar) {
    return prodDetail.getBuyAndSellPoint(mmPar.prodCode, mmPar.type, mmPar.prodId, mmPar.flag, mmPar.prodCd, mmPar.beginDate, mmPar.endDate).then(function (res) {
      return res
    })
  },
  getFundNewstNavs ({commit, state}, xxgzPar) {
    return prodDetail.getFundNewstNav(xxgzPar.prodCode, xxgzPar.prodId, xxgzPar.prodType).then(function (res) {
      let data = res
      return data
    })
  },
  getPerformances ({commit, state}, itemDataA) {
    return prodDetail.getPerformance(itemDataA).then(function (res) {
      let data = res
      return data
    })
  },
  getHistoryNavs ({commit, state}, historyPar) {
    return prodDetail.getHistoryNav(historyPar.prodCode, historyPar.prodId, historyPar.retRows, historyPar.prodType).then(function (res) {
      let data = res
      return data
    })
  },
  getHoldStock ({commit, state}, proId) {
    return prodDetail.getHoldStock(proId).then(function (res) {
      let data = res
      return data
    })
  },
  // 实时估值
  getFundNewstNavErrors ({commit, state}, navPar) {
    return prodDetail.getFundNewstNavError(navPar.prodCode).then(function (res) {
      return res
    })
  },
  // 分红
  getShareBonus({commit, state}, prodInfo) {
	 return prodDetail.getShareBonus(
	   prodInfo.prodId,
	   prodInfo.prodCode,
	   prodInfo.curPage).then(res => {
	   return res
	 }) 
  },
  // 基金公司
  getFundCompany({commit, state}, prodInfo){
	  return prodDetail.getFundCompany(prodInfo.comId,prodInfo.comName).then(res =>{
		  return res
	  })
  },
	// 获取公募基金基本资料
	getFundIntroduce ({commit, state}, params) {
		return prodDetail.getFundIntroduce(params.prodId, params.prodCode).then(function (res) {
			return res
		})
	},
	// 获取公募基金投资规模
	getFundScale ({commit, state}, params) {
	  return prodDetail.getFundScale(params.prodCode).then(function (res) {
	    return res
	  })
	},
	// 获取公募基金合同协议
	getPublicOnlyShowAgreement({commit, state}, param) {
		return prodDetail.getPublicOnlyShowAgreement(param.prodCode, param.prodType).then(res =>{
			return res
		})
	},
	// 获取基金分红
	getFundsBonusList({commit, state}, param) {
		return prodDetail.getFundsBonusList(param.prodCode, param.prodId, param.prodType, param.curPage).then(res =>{
			return res
		})
	} 
}

export default {
  actions
}
