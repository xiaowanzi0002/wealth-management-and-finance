// 首页 货架vuex T1000001 -- T1000003
// 用于：首页、专区、详情页与下单结构页营销位、产品手册（小红点）专区
import * as funds from '@/api/funds/index'

const YYZVER = 'YYZVER'
const ITEMPURPOSE = 'ITEMPURPOSE'
const COLUMNS = 'COLUMNS'
const ITEMS = 'ITEMS'
const ADIMGS = 'ADIMGS'
const SELECT = 'SELECT'
const ADPRODS = 'ADPRODS'
const THEME = 'THEME'
const CHANNELCODE = 'CHANNELCODE'
const CUSTCODE = 'CUSTCODE'

const state = {
    // // 易阳指版本号
    // yyzver: '',
    // // 用途字段
    // itemPurpose: '1',
    // T1000001 一级列表数据
    columns: [],
    // T1000001 每个栏目具体数据
    items: {},
    // T1000003数据
    adImgs: {},
    // 一级栏目选项
    select: [],
    // T1000002数据
    adProds: {},
    // 主题
    theme: '',
    // 渠道
    channelCode: '',
    // 客户代码 通过外部渠道唯一ID查询国泰君安客户代码
    custCode: ''
}

const getters = {
    yyzver: state => state.yyzver,
    itemPurpose: state => state.itemPurpose,
    columns: state => state.columns,
    items: state => state.items,
    adImgs: state => state.adImgs,
    select: state => state.select,
    adProds: state => state.adProds,
    theme: state => state.theme,
    channelCode: state => state.channelCode,
    custCode: state => state.custCode
}

const actions = {
    getColumns({dispatch, commit, state}, itemCodeSuper) {
        let channelCode = ''
        let yyzver = 'jhw'
        let itemPurpose = '4'
        if (itemCodeSuper.hasOwnProperty('channelCode') && itemCodeSuper['channelCode'] !== 'qdjgid' && itemCodeSuper['channelCode'] !== 'undefined' && itemCodeSuper['channelCode']) {
            let itemCodeSuperTmp = itemCodeSuper
            itemCodeSuper = itemCodeSuperTmp['itemCode']
            channelCode = itemCodeSuperTmp['channelCode']
        } else if (itemCodeSuper.hasOwnProperty('channelCode')) {
            let itemCodeSuperTmp = itemCodeSuper
            itemCodeSuper = itemCodeSuperTmp['itemCode']
            yyzver = state.yyzver
            itemPurpose = state.itemPurpose
        } else {
            yyzver = state.yyzver
            itemPurpose = state.itemPurpose
        }
        return funds.getColumns(itemCodeSuper, yyzver, itemPurpose, channelCode)
            .then(function (res) {
                let data = res
                if (data.success !== false) {
                    if (itemCodeSuper === '-1') {
                        state.columns = []
                        state.items = {}
                        state.adImgs = {}
                        state.adProds = {}
                        state.columns = data.sort((x, y) => parseInt(y.PRIORITY) - parseInt(x.PRIORITY))
                        for (let index in state.columns) {
                            dispatch('getColumns', state.columns[index].ITEM_CODE)
                        }
                    } else {
                        let childItems = data.sort((x, y) => parseInt(y.PRIORITY) - parseInt(x.PRIORITY))
                        // Vue.set(state.items, itemCodeSuper, childItems)
                        commit('SET_ITEMS', {itemCodeSuper, childItems});
                        //console.log(state.items)
                        // debugger
                    }
                }
                return data
            })
    },
    getAdImgs({commit, state}, obj) {
        let channelCode = ''
        let yyzver = state.yyzver
        let itemPurpose = state.itemPurpose
        let cachFlagVar = ''
        // if (utils.valideNotNull(obj.channelCode) && obj.channelCode !== 'qdjgid') {
        //   channelCode = obj.channelCode
        //   yyzver = 'jhw'
        //   itemPurpose = '4'
        // }
        if (obj.shelfBrowseWay === '17') {
            // 理财任务货架不走缓存
            cachFlagVar = '1'
        }
        return funds.getAdImgs(obj.itemCode, yyzver, itemPurpose, channelCode, cachFlagVar, state.custCode).then(function (res) {
            let data = res
            if (data.success !== false) {
                // Vue.set(
                //   state.adImgs,
                //   obj.itemCode,
                //   data.sort(function (x, y) {
                //     return parseInt(y.PRIORITY) - parseInt(x.PRIORITY)
                //   })
                // )
                const sortedData = data.sort((x, y) => parseInt(y.PRIORITY) - parseInt(x.PRIORITY));
                commit('SET_AD_IMGS', {itemCode: obj.itemCode, sortedData});
            }
            return data
        })
    },
    getProds({dispatch, commit, state}, itemCode) {
        let channelCode = ''
        let yyzver = 'jhw'
        let itemPurpose = '4'
        if (itemCode.hasOwnProperty('channelCode') && itemCode['channelCode'] !== 'qdjgid' && itemCode['channelCode'] !== 'undefined' && itemCode['channelCode']) {
            let itemCodeTmp = itemCode
            itemCode = itemCodeTmp['itemCode']
            channelCode = itemCodeTmp['channelCode']
        } else if (itemCode.hasOwnProperty('channelCode')) {
            itemCode = itemCode['itemCode']
            yyzver = state.yyzver
            itemPurpose = state.itemPurpose
        } else {
            yyzver = state.yyzver
            itemPurpose = state.itemPurpose
        }
        return funds.getProdList(itemCode, yyzver, itemPurpose, channelCode)
            .then(function (res) {
                let data = res
                if (data.success !== false) {
                    // Vue.set(
                    //   state.adProds,
                    //   itemCode,
                    //   data.sort(function (x, y) {
                    //     return parseInt(y.PRIORITY) - parseInt(x.PRIORITY)
                    //   })
                    // )
                    const sortedData = data.sort((x, y) => parseInt(y.PRIORITY) - parseInt(x.PRIORITY));
                    commit('SET_AD_PRODS', {itemCode, sortedData});
                }
                return data
            })
    },
    getBrowserNum({commit, state}, actionTargetId) {
        return funds.getBrowserNum(actionTargetId, '').then(function (res) {
            return res.data
        })
    },
    getStockMarket({commit, state}, stockList) {
        return funds.getStockMarket('pre', stockList).then(function (res) {
            return res.data
        })
    },
    // getMarketPosition({commit, state}, params) {
    //     return funds.getMarketPosition(params.prodCode, params.effectSceneType).then(function (res) {
    //         return res.data
    //     })
    // }
}

const mutations = {
    SET_AD_PRODS(state, {itemCode, sortedData}) {
        // 直接更新 state 中的属性
        state.adProds[itemCode] = sortedData;
    },
    SET_AD_IMGS(state, {itemCode, sortedData}) {
        // 直接更新 state 中的属性
        state.adImgs[itemCode] = sortedData;
    },
    SET_ITEMS(state, {itemCodeSuper, childItems}) {
        // 直接更新 state 中的属性
        state.items[itemCodeSuper] = childItems;
    },
    [YYZVER](state, {yyzver}) {
        state.yyzver = yyzver
    },
    [ITEMPURPOSE](state, {itemPurpose}) {
        state.itemPurpose = itemPurpose
    },
    [COLUMNS](state, {columns}) {
        state.columns = columns
    },
    [ITEMS](state, {items, method, para}) {//{code,attr,newVal}
        if (method === 'set') {
            state.items = items
        } else if (method === 'update') {
            if (state.items[para.code]) {
                // Vue.set(state.items[para.code][0], para.attr, para.newVal)
                state.items[para.code][0][para.attr] = para.newVal
            }
        }
    },
    [ADIMGS](state, {adImgs}) {
        state.adImgs = adImgs
    },
    [SELECT](state, {index}) {
        let clength = state.columns === null ? 0 : state.columns.length
        let slength = state.select === null ? 0 : state.select.length
        if (slength !== clength) {
            state.select = []
            for (let i = 0; i < clength; i++) {
                state.select.push(false)
            }
        }
        for (let i = 0; i < clength; i++) {
            if (i === index) {
                // Vue.set(state.select, i, true)
                state.select[i] = true
            } else if (state.select[i] === true) {
                // Vue.set(state.select, i, false)
                state.select[i] = false
            }
        }
    },
    [ADPRODS](state, {adProds}) {
        state.adProds = adProds
    },
    [THEME](state, {theme}) {
        state.theme = theme
    },
    [CHANNELCODE](state, {channelCode}) {
        state.channelCode = channelCode
    },
    [CUSTCODE](state, {custCode}) {
        state.custCode = custCode
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}
