import newFundRank from '@/api/funds/newFundRank'
import selfProd from '@/api/funds/customizationProd'
import dateTime from '@/api/funds/dateTime'
export const actions = {
  getNewFundRankDisplay ({commit, state}, params) {
    return newFundRank.getNewFundRankDisplay(
      params.rankType,
      params.investType,
      params.timeFlag,
      params.initSort,
      params.sortColumn,
      params.order,
      params.pageSize,
      params.pageNum,
      params.queryType,
      params.oneKey,
      params.lhjjType,
      params.riskType,
      params.scaleMin,
      params.scaleMax,
      params.createMin,
      params.createMax,
      params.proProvider,
      params.topicCode,
      params.hasDiscount,
      params.buyStatus,
      params.stockType,
      params.originMoneyMin,
      params.originMoneyMax,
      params.dateType,
      params.rankTermsFlag).then(res => {
      return res
    })
  },
  getFundCompanyOrTopicList ({commit, state}, params) {
    return newFundRank.getFundCompanyOrTopicList(
      params.listType).then(res => {
      let data = res.data
      return data
    })
  },
  // 查询自选基金：T3100066
  async queryListSelfChoiceProduct({commit,state}, phoneNumber, prodType) {
	  // prodType: 定期=3, groupNo=0写死, inListFlag不传表示列表页调用
	  prodType = prodType ? prodType : '3'
	 let res = await selfProd.queryListSelfChoiceProduct(prodType, '0', '', phoneNumber) 
	 let list = ''
	 for (let i in res) {
		 list += res[i].OFUND_CODE + ','
	 }
	 state.selfFunds = list
	 return res
  },
  // 管理自选基金 增删改后，selfFunds改变，参数以数组的形式组织
  async selfChoiceProductManagement({commit, state}, params) {
	  let res = await selfProd.selfChoiceProductManagement(params.actionFlag, params.acctType, params.acctCode, params.batchData, params.remark)
	  let batchData = JSON.parse(params.batchData)
	  let regex = ''
	  switch(params.actionFlag) {
		  case '00': state.selfFunds += batchData[0].OFUND_CODE + ','; break;
		  case '01': 
			regex = new RegExp(batchData[0].OFUND_CODE + ',');
			state.selfFunds = state.selfFunds.replace(regex, '');
			break;
		  case '10': 
			for(let i in batchData) {
				state.selfFunds += batchData[i].OFUND_CODE + ','
			};
			break;
		  case '12':
			for(let i in batchData) {
				regex = new RegExp(batchData[i].OFUND_CODE + ',');
				state.selfFunds = state.selfFunds.replace(regex, '');
			};
			break;
	  }
	  return res
  },
	/**
	 * 获取当前服务器时间及指定日期的下一个交易日
	 * @param toBeComputedDate toBeComputedDate 非必传，可传空；获取这个日期的下一个交易日，为空默认为当前日期，格式为20190806
	 * @param stageNum 步长 当为0时，获取下一个交易日（即T+1），格式为20190808，为1时，获取T+2日期，即获取T+1+stageNum交易日日期
	 */
	getCurrentDateAndNextTradeDate ({commit, state}, params) {
	  return dateTime.getCurrentDateAndNextTradeDate(params.toBeComputedDate, params.stageNum).then(function (res) {
	    return res
	  })
	},
	/* 自选基金加密手机号 */
	async getEncryptionPhone({dispatch, state}, mobile) {
		let res = await selfProd.getEncryptionPhone(mobile)
		uni.setStorageSync('encryptionPhone', res.yyMobile)
		await dispatch('queryListSelfChoiceProduct', res.yyMobile)
		return res.yyMobile
	}
}
