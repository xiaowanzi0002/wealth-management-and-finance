/*
 * @Author: WangRui
 * @Date: 2020-09-04 16:59:37
 * @Last Modified by:   WangRui
 * @Last Modified time: 2020-09-04 16:59:37
 */
export const SET_SORT_TEXT_SHOW = 'set_sort_text_show'
export const SET_COMPANY_DATA_LIST = 'companyDataList'
export const SET_TOPIC_DATA_LIST = 'topicDataList'
export const SET_COMPANY_INDEX_LIST = 'companyIndexList'
export const SET_TOPIC_INDEX_LIST = 'topicIndexList'
export const SET_RANK_TERMS = 'set_rank_terms'
export const SET_IS_CURRENCY = 'set_is_currency'
export const SET_FUND_LIST_LENGTH = 'set_fund_list_length'
export const SET_SELECT_COMPANY = 'set_select_company'
export const SET_SELECT_THEME = 'set_select_theme'
export const SET_SELECT_THEME_CODE = 'set_select_theme_code'
export const SET_HALF_LOADING_LINE = 'set_half_loading_line'
export const SET_FULL_LOADING_LINE = 'set_full_loading_line'
export const SET_FIRST_LOADING = 'set_first_loading'
export const SET_CUR_FONT_SIZE = 'set_cur_font_size'
export const SELF_FUNDS = 'selfFunds'
export const FUND_SORT_COLUMN = 'fundSortColumn'
