/*
 * @Author: WangRui
 * @Date: 2020-09-04 15:16:33
 * @Last Modified by: WangRui
 * @Last Modified time: 2020-10-22 10:47:44
 */

export const getters = {
  sortTextShow: state => {
    return state.sortTextShow
  },
  companyDataList: state => {
    return state.companyDataList
  },
  topicDataList: state => {
    return state.topicDataList
  },
  companyIndexList: state => {
    return state.companyIndexList
  },
  topicIndexList: state => {
    return state.topicIndexList
  },
  rankTerms: state => {
    return state.rankTerms
  },
  isCurrency: state => {
    return state.isCurrency
  },
  fundListLength: state => {
    return state.fundListLength
  },
  selectCompany: state => {
    return state.selectCompany
  },
  selectTheme: state => {
    return state.selectTheme
  },
  selectThemeCode: state => {
    return state.selectThemeCode
  },
  isShowHalfLoadingLine: state => {
    return state.isShowHalfLoadingLine
  },
  isShowFullLoadingLine: state => {
    return state.isShowFullLoadingLine
  },
  isFirstLoading: state => {
    return state.isFirstLoading
  },
  curFontSize: state => {
    return state.curFontSize
  },
  selfFunds: state => {
	  return state.selfFunds
  },
  fundSortColumn: state => {
	  return state.fundSortColumn
  }
}
