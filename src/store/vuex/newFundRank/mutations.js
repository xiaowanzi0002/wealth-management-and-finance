/*
 * @Author: WangRui
 * @Date: 2020-09-04 16:59:12
 * @Last Modified by: WangRui
 * @Last Modified time: 2020-10-22 10:36:13
 */
import * as types from './types'
export const mutations = {
  [types.SET_SORT_TEXT_SHOW] (state, sortTextShow) {
    state.sortTextShow = sortTextShow
  },
  [types.SET_COMPANY_DATA_LIST] (state, companyDataList) {
    state.companyDataList = companyDataList
  },
  [types.SET_TOPIC_DATA_LIST] (state, topicDataList) {
    state.topicDataList = topicDataList
  },
  [types.SET_COMPANY_INDEX_LIST] (state, companyIndexList) {
    state.companyIndexList = companyIndexList
  },
  [types.SET_TOPIC_INDEX_LIST] (state, topicIndexList) {
    state.topicIndexList = topicIndexList
  },
  [types.SET_RANK_TERMS] (state, rankTerms) {
    state.rankTerms = Object.assign({}, rankTerms)
    console.log('vuex', state.rankTerms)
  },
  [types.SET_IS_CURRENCY] (state, isCurrency) {
    state.isCurrency = isCurrency
  },
  [types.SET_FUND_LIST_LENGTH] (state, fundListLength) {
    state.fundListLength = fundListLength
  },
  [types.SET_SELECT_COMPANY] (state, selectCompany) {
    state.selectCompany = selectCompany
  },
  [types.SET_SELECT_THEME] (state, selectTheme) {
    state.selectTheme = selectTheme
  },
  [types.SET_SELECT_THEME_CODE] (state, selectThemeCode) {
    state.selectThemeCode = selectThemeCode
  },
  [types.SET_HALF_LOADING_LINE] (state, halfLoadingLine) {
    state.isShowHalfLoadingLine = halfLoadingLine
  },
  [types.SET_FULL_LOADING_LINE] (state, fullLoadingLine) {
    state.isShowFullLoadingLine = fullLoadingLine
  },
  [types.SET_FIRST_LOADING] (state, isFirstLoading) {
    state.isFirstLoading = isFirstLoading
  },
  [types.SET_CUR_FONT_SIZE] (state, curFontSize) {
    state.curFontSize = curFontSize
  },
  [types.SELF_FUNDS] (state, selfFunds) {
	  state.selfFunds = selfFunds
  },
  [types.FUND_SORT_COLUMN](state, fundSortColumn) {
	  state.fundSortColumn = fundSortColumn
  }
}
