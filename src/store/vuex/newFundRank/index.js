/*
 * @Author: WangRui
 * @Date: 2020-09-03 14:05:11
 * @Last Modified by: WangRui
 * @Last Modified time: 2020-09-04 17:02:24
 */
import Vue from 'vue'
import Vuex from 'vuex'
// import base from '../base/index'
import {actions} from './actions'
import { getters } from './getters'
import { mutations } from './mutations'
Vue.use(Vuex)
const state = {
 sortTextShow: {
    // 收益
    'income': {
      nonCurrency: [
        {
          text: '净值',
          column: 'NEW_NAV'
        },
        {
          text: '日涨跌幅',
          column: 'DAY_ADD'
        },
        {
          text: '近一周',
          column: 'COMM_1W'
        },
        {
          text: '近1月',
          column: 'COMM_1M'
        },
        {
          text: '近3月',
          column: 'COMM_3M'
        },
        {
          text: '近6月',
          column: 'COMM_6M'
        },
        {
          text: '今年以来',
          column: 'COMM_YEAR'
        },
        {
          text: '近1年',
          column: 'COMM_1Y'
        },
        {
          text: '近2年',
          column: 'COMM_2Y'
        },
        {
          text: '近3年',
          column: 'COMM_3Y'
        },
        {
          text: '成立以来',
          column: 'COMM'
        },
        {
          text: '规模',
          column: 'NEW_SCALE'
        }
        // {
        //   text: '操作',
        //   column: ''
        // }
      ],
      currency: [
        {
          text: '万份收益',
          column: 'UNIT_YLD_RMB'
        },
        {
          text: '7日年化',
          column: 'EXP_RATE_PER_SEVENDAY'
        },
        // {
        //   text: '近1月',
        //   column: 'COMM_1M'
        // },
        // {
        //   text: '近3月',
        //   column: 'COMM_3M'
        // },
        // {
        //   text: '近6月',
        //   column: 'COMM_6M'
        // },
        // {
        //   text: '今年以来',
        //   column: 'COMM_YEAR'
        // },
        // {
        //   text: '近1年',
        //   column: 'COMM_1Y'
        // },
        {
          text: '规模',
          column: 'NEW_SCALE'
        }
        // {
        //   text: '操作',
        //   column: ''
        // }
      ]
    },
    // 定投， 定投热销
    'autoInvest': [
      {
        text: '普通定投',
        column: 'N_RATE'
      },
      {
        text: '灵犀定投',
        column: 'S_RATE'
      }
    ],
    // 热销，访问
    'other': [
      {
        text: '净值',
        column: 'NEW_NAV'
      },
      {
        text: '日涨跌幅',
        column: 'DAY_ADD'
      },
      {
        text: '规模',
        column: 'NEW_SCALE'
      },
      {
        text: '操作',
        column: ''
      }
    ]
  },
  // 基金公司/主题信息及选中状态
  companyDataList: [],
  topicDataList: [],
  companyIndexList: [],
  topicIndexList: [],
  // 选中的基金公司,为空则选中全部
  selectCompany: '',
  // 选中的基金主题和代码,为空则不限
  selectTheme: '',
  selectThemeCode: '',
  // 调用T2000082查询榜单列表的参数
  rankTerms: {
    rankType: '1',
    investType: '',
    timeFlag: '',
    initSort: '',
    // sortColumn: 'COMM_1M',
    sortColumn: 'COMM_1Y',
    order: 'desc',
    pageSize: '',
    pageNum: '',
    queryType: '',
    riskType: '',
    oneKey: '',
    lhjjType: '',
    scaleMin: '',
    scaleMax: '',
    createMin: '',
    createMax: '',
    proProvider: '',
    topicCode: '',
    hasDiscount: '',
    buyStatus: '',
    stockType: '',
    originMoneyMin: '',
    originMoneyMax: '',
    dateType: ''
  },
  // 是否是货币型
  isCurrency: false,
  // 查到的出参总条数
  fundListLength: 0,
  // 半截加载线
  isShowHalfLoadingLine: false,
  // 全部加载线
  isShowFullLoadingLine: false,
  // 是第一次加载
  isFirstLoading: true,
  // 当前页面根元素html的font-size
  curFontSize: 50,
  // 自选基金 T3100066
  selfFunds: '',
  // 基金右侧的列的文字
  fundSortColumn: [
    {
      index: 3,
      text: '近一周'
    },
    {
      index: 4,
      text: '近一月'
    },
    {
      index: 5,
      text: '近三月'
    },
    {
      index: 6,
      text: '近六月'
    },
    {
      index: 7,
      text: '近1年'
    },
    {
      index: 8,
      text: '近2年'
    },
    {
      index: 9,
      text: '近3年'
    },
    {
      index: 10,
      text: '成立以来'
    }
  ]
}

export default {
  state,
  actions,
  getters,
  mutations
  // modules: {
  //   base
  // }
}
