import { createStore } from "vuex";

import homeFund from '@/store/vuex/prodDetail/modules/home.vuex.js'
import comModule from "@/store/vuex/moudels/comModule";

// 创建 store 实例
const store = createStore({
	modules: {
		homeFund: homeFund,
		comModule
		// 其他模块...
	},
});


export default store;
