export const getters = {
  prodCode: state => {
    return state.prodCode
  },
  sourceFlag: state => {
    return state.sourceFlag
  },
  prodData: state => {
    return state.prodData
  },
  prodInfoPd: state => {
    return state.prodInfoPd
  },
  serialNum: state => {
    return state.serialNum
  },
}
