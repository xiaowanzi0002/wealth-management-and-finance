// 提供公共T1000004相关服务，谨慎修改
import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'

const state = {
  // 产品代码
  prodCode: '',
  // 来源 1010:转让市场 2010:持仓 0010或空:正常来源 3010:策略中心 5010:百事通全连接
  sourceFlag: '',
  // 详情页查询T1000004存储，包含产品交易状态等
  prodData: {},
  // 仅T1000004接口出参
  prodInfoPd: {},
  serialNum:''
}

export default {
  state,
  getters,
  actions,
  mutations
}
