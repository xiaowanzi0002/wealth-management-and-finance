import * as types from './types'

export const mutations = {
  [types.PRODCODE] (state, {prodCode}) {
    state.prodCode = prodCode
  },
  [types.SOURCEFLAG] (state, {sourceFlag}) {
    state.sourceFlag = sourceFlag
  },
  [types.PRODDATA] (state, {prodData}) {
    state.prodData = prodData
  },
  [types.PRODINFO_PD] (state, {prodInfoPd}) {
    state.prodInfoPd = prodInfoPd
  },
    [types.SERIALNUM] (state, {serialNum}) {
      state.serialNum = serialNum
    }
}
