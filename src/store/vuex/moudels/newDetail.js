import * as types from '../mutation-types.js'

const state = {
  newsDetailData: {},
}

const getters = {
  newsDetailData: state => state.newsDetailData,
  
}

const mutations = {
  [types.NEWS_DETAIL_DATA] (state, obj) {
    state.newsDetailData = obj
  }
}

export default {
  state,
  getters,
  mutations
}
