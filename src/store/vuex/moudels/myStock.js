import * as types from '../mutation-types.js'

const state = {
  myStocks: [
    {
      stockCode: '601211',
      stkName: '国泰君安',
      zd: 0,
      zdf: 0,
      price: 0,
      market: 'SH',
      Order: 1,
      groupNo: '0'
    }
  ],
  myStocksEdit: false
}

const getters = {
  myStocks: state => state.myStocks,
  myStocksEdit: state => state.myStocksEdit
}

const mutations = {
  [types.UPDATE_MY_STOCKS] (state, arr) {
    state.myStocks = arr
  },
  [types.DELETE_MY_STOCKS] (state, obj) {
    for (let i = 0; i < state.myStocks.length; i++) {
      if (state.myStocks[i].stockCode === obj.stockCode && state.myStocks[i].market === obj.market) {
        state.myStocks.splice(i, 1)
      }
    }
  },
  [types.ADD_MY_STOCKS] (state, obj) {
    state.myStocks.unshift(obj)
  },
  [types.TOP_MY_STOCKS] (state, idx) {
    let topOne = state.myStocks.splice(idx, 1)
    state.myStocks.unshift(topOne[0])
  },
  [types.MY_STOCKS_EDIT] (state, bool) {
    state.myStocksEdit = bool
  }
}

export default {
  state,
  getters,
  mutations
}
