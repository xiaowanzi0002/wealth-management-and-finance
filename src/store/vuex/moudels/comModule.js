import * as types from '../mutation-types.js'
import utils from '@/utils/utils.js'
import newsAPI from '@/api/news.js'
import indexAPI from '@/api/index.js'

const state = {
	fkybInfo: {
		columnRights: false,
		newsRightsList: '',
		columnNames: '风口研报',
		columnDes: '每日速递4-6篇高含金量研报，挖掘风口行业龙头，机构和私募都在用的王牌VIP资讯，了解详情>>',
		endDate: '', // 到期日
		price: '', // 风口研报栏目价格
		perPrice: '' ,// 单篇文章价格,
		dateDiff: 999 // 到期日距今天时间差
	},
	userShareName: '小君',
	// yyzver: '1.0.1',
	// itemPurpose: '1',
	showTabList:['首页','我的'],
	gUserInfo: {
		telephone: '',
		followers: '',
		starGrade: 1
	},
	gManagerInfo: {
		employeeNo: null
	},
	gManagerInfoShow: {
		employeeNo: null
	},
	gMcardPicUrl:'',
	wxAuthStatus:true,
	isEditingStyle: false,
	loading: false,
	tradeDay: null,
	posterInfo: {
		bgUrl: ''
	},
	navInfo: {
		hasNavBack: true,
		navHeight: 64
	},
	scene: '',
	toolPosterInfo: {},
	sendMaterialInfo: {},
	shopInfo: {},
	customerInfo: {},
	materialRecord: {},
	previewDocList: [],
	componentState: {
		index_tab: [],
		index_tab01_one: [],
		index_tab01_two: [],
		index_tab01_three: [],
		index_tab02_one: [],
		index_tab03_one: [],
		index_tab03_two: [],
		index_tab03_three: [],
		index_tab04_one: [],
		index_tab05_one: [],
		index_tab05_two: [],
		index_tab05_three: [],
		index_tab06_one: [],
		index_tab06_two: [],
		specialNew_one: [], //专题资讯
		reportListPage_one: [], //专题资讯 二级页面
		honghuPage_one: [], //鸿鹄观市
		dayNewsPage_one: [], //每日日早报
		liveReportPage_one: [], //实时快讯
		coursePage_one: [], //课程模块列表页
		courseList_one: [], //课程二级目录
		junhongRoom_one: [], //君弘栏目房间列表
		junhongRoom_two: [], //君弘栏目房间列表
		junhongRoom_three: [], //君弘栏目房间列表
		vocation: [], // 客户经理岗位
		defaults: [] //某些list组件不需要睁眼闭眼，无实际用途
	},
	componentState_backup: {
		index_tab: [],
		index_tab01_one: [],
		index_tab01_two: [],
		index_tab01_three: [],
		index_tab02_one: [],
		index_tab03_one: [],
		index_tab03_two: [],
		index_tab03_three: [],
		index_tab04_one: [],
		index_tab05_one: [],
		index_tab05_two: [],
		index_tab05_three: [],
		index_tab06_one: [],
		index_tab06_two: [],
		specialNew_one: [], //专题资讯
		reportListPage_one: [], //专题资讯 二级页面
		honghuPage_one: [], //鸿鹄观市
		dayNewsPage_one: [], //每日日早报
		liveReportPage_one: [], //实时快讯
		coursePage_one: [], //课程模块列表页
		courseList_one: [], //课程二级目录
		junhongRoom_one: [], //君弘栏目房间列表
		junhongRoom_two: [], //君弘栏目房间列表
		junhongRoom_three: [], //君弘栏目房间列表
		vocation: [], // 客户经理岗位
		defaults: [] //某些list组件不需要睁眼闭眼，无实际用途
	}
}

const getters = {
	fkybInfo: state => {
		return state.fkybInfo
	},
	userShareName: state => {
		return state.userShareName
	},
	// yyzver: state => {
	// 	return state.yyzver
	// },
	// itemPurpose: state => {
	// 	return state.itemPurpose
	// },
	showTabList(state){
		return state.showTabList
	},
	gUserInfo(state) {
		if (utils.valideNull(state.gUserInfo)) {
			state.gUserInfo = utils.getUserFromLocal() //获取本地数据防止刷新无数据
		}
		return state.gUserInfo
	},

	gManagerInfo(state) {
		if (utils.valideNull(state.gManagerInfo.employeeNo)) {
			state.gManagerInfo = utils.getManagerFromLocal() //获取本地数据防止刷新无数据
		}
		return state.gManagerInfo
	},
	gManagerInfoShow(state) {
		let obj = {}
		if (utils.valideNull(state.gManagerInfo)) {
			obj = utils.getManagerFromLocal()
		} else {
			obj = state.gManagerInfo
		}
		console.log(obj)
		if (typeof obj == 'undefined' || obj == '' || obj == null) return null
		let ehrPicBase64 =obj.cardPicUrl?obj.cardPicUrl.split(',')[1]:''
		state.gManagerInfoShow = {
			employeeNo: obj.employeeNo,
			shopId: '01' + obj.employeeNo,
			tgFlag: obj.tgFlag,
			isQuality: obj.isQualify,
			userName: obj.isQualify ? obj.userName : (obj.provinceName == '总部' ? '国泰君安证券' : obj.branchName),
			telephoneNo: obj.isQualify ? obj.telephoneNo : (obj.provinceName == '总部' ? '95521' : obj.telephoneNo),
			mobileNo: obj.isQualify ? obj.mobileNo : (obj.provinceName == '总部' ? '' : (obj.telephoneNo || '95521')),
			wechatNo: obj.isQualify ? obj.wechatNo : '国泰君安微理财',
			postion: obj.isQualify ? obj.postion : '',
			provinceName: obj.isQualify ? obj.provinceName : obj.provinceName,
			provinceId: obj.isQualify ? obj.provinceId : obj.provinceId,
			branchName: obj.isQualify ? obj.branchName : '国泰君安证券',
			branchAddress: obj.isQualify ? obj.branchAddress : obj.branchAddress,
			email: obj.isQualify ? obj.email : '95521@gtjas.com',
			certificateNo: obj.isQualify ? obj.certificateNo : '',
			cardPicUrl: obj.isQualify ? obj.cardPicUrl : 'https://dl.app.gtja.com/public/wlc/xd/img/gtjaHeadImg.png',
			cardPicIsBase64: obj.isQualify && (!utils.valideNull(ehrPicBase64)),
			userProfile: obj.isQualify ? obj.userProfile : '国泰君安，中国证券行业长期、持续、全面领先的综合金融服务商。国泰君安跨越了中国资本市场发展的全部历程和多个周期，始终以客户为中心，深耕中国市场，为个人和机构客户提供各类金融服务，确立了全方位的行业领先地位。'
		}
		return state.gManagerInfoShow
	},
	loading: state => state.loading,
	posterInfo: state => state.posterInfo,
	isEditingStyle: state => state.isEditingStyle, 
	wxAuthStatus: state => state.wxAuthStatus, 
	tradeDay: state => state.tradeDay,
	navInfo: state => state.navInfo,
	scene: state => state.scene,
	toolPosterInfo: state => state.toolPosterInfo,
	sendMaterialInfo: state => state.sendMaterialInfo,
	shopInfo: state => state.shopInfo,
	customerInfo: state => state.customerInfo,
	materialRecord: state => state.materialRecord,
	componentState: state => state.componentState,
	previewDocList(state) {
		if (utils.valideNull(state.previewDocList)) {
			state.previewDocList = utils.getObjFromLocal('PREVIEWDOC_LIST') //获取本地数据防止刷新无数据
			if(utils.valideNull(state.previewDocList)) {
				state.previewDocList = []
			}
		}
		return state.previewDocList
	},
	componentState_backup: state => state.componentState_backup
}

const mutations = {
	[types.FKYB_INFO](state, obj){
		for (let key in obj) {
			state.fkybInfo[key] = obj[key]
		}
		// console.log('风口研报vuex信息', state.fkybInfo)
	},
	[types.USER_SHARE_NAME](state, obj) {
		state.userShareName = obj
	},
	// [types.YYZVER](state, {
	// 	yyzver
	// }) {
	// 	state.yyzver = yyzver
	// },
	// [types.ITEMPURPOSE](state, {
	// 	itemPurpose
	// }) {
	// 	state.itemPurpose = itemPurpose
	// },
	[types.USER_INFO](state, obj) {
		if (obj && !utils.valideNull(obj.telephone)) {
			actions.getFollowers(obj.telephone)
			// actions.qStarGrade(obj.telephone)
			// 
		}
		if (obj && !utils.valideNull(obj.openId)) {
			state.gUserInfo = obj
			if(state.gUserInfo.viewType==1){
				state.showTabList=['首页','客户','工具','我的']
			}else{
				state.showTabList=[]
			}
			utils.saveUserInfo(obj) //同步保存用户信息到本地
		}

	},
	[types.MANAGER_INFO](state, obj) {
		state.gManagerInfo = obj
		// state.gMcardPicUrl=obj.cardPicUrl
		utils.saveManagerInfo(obj) 
		if (typeof obj == 'undefined' || obj == '' || obj == null) return null
		let ehrPicBase64 =obj.cardPicUrl?obj.cardPicUrl.split(',')[1]:''
		state.gManagerInfoShow = {
			employeeNo: obj.employeeNo,
			shopId: '01' + obj.employeeNo,
			tgFlag: obj.tgFlag,
			isQuality: obj.isQualify,
			userName: obj.isQualify ? obj.userName : (obj.provinceName == '总部' ? '国泰君安证券' : obj.branchName),
			telephoneNo: obj.isQualify ? obj.telephoneNo : (obj.provinceName == '总部' ? '95521' : obj.telephoneNo),
			mobileNo: obj.isQualify ? obj.mobileNo : (obj.provinceName == '总部' ? '' : (obj.telephoneNo || '95521')),
			wechatNo: obj.isQualify ? obj.wechatNo : '',
			postion: obj.isQualify ? obj.postion : '',
			provinceName: obj.isQualify ? obj.provinceName : obj.provinceName,
			provinceId: obj.isQualify ? obj.provinceId : obj.provinceId,
			branchName: obj.isQualify ? obj.branchName : "国泰君安证券",
			branchAddress: obj.isQualify ? obj.branchAddress : obj.branchAddress,
			email: obj.isQualify ? obj.email : '95521@gtjas.com',
			certificateNo: obj.isQualify ? obj.certificateNo : '',
			cardPicUrl: obj.isQualify ? obj.cardPicUrl : 'https://dl.app.gtja.com/public/wlc/xd/img/gtjaHeadImg.png',
			cardPicIsBase64: obj.isQualify && (!utils.valideNull(ehrPicBase64)),
			userProfile: obj.isQualify ? obj.userProfile : '国泰君安，中国证券行业长期、持续、全面领先的综合金融服务商。国泰君安跨越了中国资本市场发展的全部历程和多个周期，始终以客户为中心，深耕中国市场，为个人和机构客户提供各类金融服务，确立了全方位的行业领先地位。'
		}
		console.log('gManagerInfoShow  commodule',state.gManagerInfoShow)
	},
	[types.MANAGER_INFO_SHOW](state, obj) {


	},
	[types.LOADING](state, obj) {
		state.loading = obj
	},
	[types.POSTER_INFO](state, obj) {
		state.posterInfo = obj
	},
	[types.TOOL_POSTER_INFO](state, obj) {
		let defultSetting= {"emName":true,"emPhone":true,"emCertNumber":true,"emNoPoster":false}
		let toolPoster = utils.getObjFromLocal('gtja.tool.posterInfo')
		if(!toolPoster){
			state.toolPosterInfo.emName = defultSetting.emName
			state.toolPosterInfo.emPhone = defultSetting.emPhone
			state.toolPosterInfo.emCertNumber = defultSetting.emCertNumber
			state.toolPosterInfo.emNoPoster = defultSetting.emNoPoster
			state.toolPosterInfo.poster = obj.poster
		}else{
			if(obj&&obj.hasOwnProperty('emName')){
				state.toolPosterInfo = obj
			}else if(obj&&obj.hasOwnProperty('poster')){ //只更新poster数据
				state.toolPosterInfo=toolPoster
				console.log(state.toolPosterInfo,obj.poster)
				state.toolPosterInfo.poster = obj.poster
			}
		}
	},
	[types.IS_EDITING_STYLE](state, obj) {
		state.isEditingStyle = obj
		
	},
	[types.WX_AUTH_STATUS](state, obj) {
		state.wxAuthStatus=obj
		
	},
	
	[types.TRADE_DAY](state, val) {
		state.tradeDay = val
	},
	[types.NAV_INFO](state, val) {
		state.navInfo = val
	},
	[types.SCENE](state, val) {
		state.scene = val
	},
	[types.SENDMATERIALINFO](state, val) {
		state.sendMaterialInfo = val
	},
	[types.SHOPINFO](state, val) {
		state.shopInfo = val
	},
	[types.CUSTOMER_INFO](state, val) {
		state.customerInfo = val
	},
	[types.MATERIAL_RECORD](state, val) {
		state.materialRecord = val
	},
	[types.PREVIEWDOC_LIST](state, obj) {
		state.previewDocList = obj
		utils.saveObjToLocal('PREVIEWDOC_LIST', obj) //同步保存用户信息到本地
	},
	[types.COMPONENT_STATE](state, val) {
		let content = val
		for (let key1 in state.componentState_backup) {
			let addFlag = true
			for (let key2 in content) {
				if (key1 == key2) {
					addFlag = false
					break
				}
			}
			if (addFlag) {
				content[key1] = []
			}
			if (content[key1].toString().indexOf('|') < 0) {
				if (content[key1].length > 0) {
					let mystr = '|' + content[key1].join("|,|") + '|'
					content[key1] = mystr.split(',')
				}
			}
		}
		state.componentState = content
		uni.setStorageSync('locComponentState', content)
	},
	[types.COMPONENT_STATE_BACKUP](state, val) {
		state.componentState_backup = val
	},



}
const actions = {
	addFilesLooked({
		commit,
		state
	}, tempFilePath) {
		console.log('~~~~~~~~~~~~~~~~~~~~~~~~', state)
		if (tempFilePath != state.previewDocList[0]) {
			state.previewDocList.unshift(tempFilePath)
		}
		return new Promise((resolve, reject) => {
			if (state.previewDocList.length == 1) {
				commit(types.PREVIEWDOC_LIST, state.previewDocList)
				resolve()
			}
			let len = state.previewDocList.length
			for (let i = len - 1; i > 0; i--) {
				if (state.previewDocList[i] != state.previewDocList[0]) { //重复预览同一文档
					wx.getFileSystemManager().unlink({
						filePath: state.previewDocList[i],
						complete: function(ee) {
							let hello = state.previewDocList.pop()
							console.log(hello)
							if (state.previewDocList.length == 1) {
								commit(types.PREVIEWDOC_LIST, state.previewDocList)
								resolve()
							}
						}
					})
				}
				if (i == 1) {
					commit(types.PREVIEWDOC_LIST, state.previewDocList)
					resolve()
				}
			}
		})
	},
	getFollowers(telphone) {
		newsAPI.queryFollowAct(telphone).then(res => {
			let followers = ''
			for (let item in res.list) {
				followers += '_' + res.list[item].TOPIC_CODE
			}
			state.gUserInfo.followers =  followers ||''
			utils.saveUserInfo(state.gUserInfo) //同步保存用户信息到本地
			console.log('followers同步保存用户信息到本地',state.gUserInfo.followers)
		})
	},
	qStarGrade(telphone) {
		indexAPI.qStarGrade(telphone).then(res => {
			if(res.data.length){
				state.gUserInfo.starGrade = res.data[0].userLevel ||'1'
				utils.saveUserInfo(state.gUserInfo) 
			}
		})
	}
}
export default {
	state,
	getters,
	mutations,
	actions
}
