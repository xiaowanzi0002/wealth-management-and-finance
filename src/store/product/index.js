import {defineStore} from 'pinia';

import {getters} from './getters'
import proDet from '@/api/funds/productDetails.js'

import {actions} from './actions'
import {mutations} from './mutations'
import state from "@/enterprise/pages/enterprise-type/state.vue";

export const useProductStore = defineStore({
    id: 'useProductStore',
    state: () => {
        return {
            prodCode: '',
            sourceFlag: '',
            prodData: {},
            prodInfoPd: {},
            serialNum: '',
        }
    },
    getters: {

    },
    actions: {
        setProdCode(prodCode) {
            this.$patch({
                prodCode: prodCode,
            });
            console.log(this.prodCode)
            // this.prodCode = prodCode;
        },
        setSourceFlag(sourceFlag) {
            this.$patch({sourceFlag});

            // this.sourceFlag = sourceFlag;
        },
        async getProdDetails(prodCode) {
            // 如果需要进行一些参数处理，你可以在这里完成
            // 例如，检查 params 是否为 undefined，然后设置默认值
            let params = {
                walletCode: '',
                outWalletCode: '',
                tgljInfo: '',//global.MGC.queryUriSingleParam('tglj_info'),
                prodCode: prodCode,
                sourceFlag: ''
            }

            let that = this

            return proDet.getProdDetails(params.prodCode, params.sourceFlag, params.walletCode, params.outWalletCode, params.tgljInfo).then(function (res) {
                that.$patch((state) => {
                    state.prodData = res
                })
                return res
            })
        },
    },
    // mutations: mutations,
});
