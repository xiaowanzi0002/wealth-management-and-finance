/**
 * 提供公共T1000004相关服务，谨慎修改
 */
import proDet from '@/api/funds/productDetails.js'

export const actions = {
  getProdDetails ({commit, state}, params) {
    // if (global.MGC.isBlank(params)) {
      params = {
        walletCode: '',
        outWalletCode:'',
        tgljInfo: '',//global.MGC.queryUriSingleParam('tglj_info'),
        prodCode: state.prodCode,
        sourceFlag: ''
      }
    // }
    return proDet.getProdDetails(params.prodCode, params.sourceFlag, params.walletCode, params.outWalletCode, params.tgljInfo).then(function (res) {
	  let data = res
      commit('PRODDATA', {prodData: data})
      return data
    })
  },
  getProdInfo ({commit, state}, prodCode) {
    // 产品代码可直接传值或放至vuex中
    // prodCode = global.MGC.isBlank(prodCode) ? state.prodCode : prodCode
    return proDet.getProdInfo(prodCode).then(function (res) {
      if (res.success !== false) {
        commit('PRODINFO_PD', {prodInfoPd: res})
      }
      return res
    })
  }
}
