import * as echarts from '@/components/echarts/echarts.min.js';

function drawCanvas(e, canvas, option) {
    // debugger
    let {
        width,
        height
    } = e;
    echarts.setCanvasCreator(() => canvas);
    let mChart = echarts.init(canvas, null, {
        width: width || uni.upx2px(750),
        height: height || uni.upx2px(420)
    })
    canvas.setChart(mChart)
    mChart.setOption(option, true)
}

export default {
    /*
    * 折线型：一条线。    参数说明如下：
    * obj: 画图挂载的元素
    * chartData:      画图对应的总数据对象
    * xAxisMarkData： xAxis的data值
    * seriesNameA：   series第1条数据的name值
    * seriesDataA:    series第1条数据的data值
    * seriesNameB：   series第2条数据的name值
    * seriesDataB:    series第2条数据的data值
    */

    chartClear(obj) { //yy
        var myChart = echarts.init(obj)
        myChart.clear()
    },
// 用估值画图
    chartLineOneS(e, canvas, chartData) { //yy
        // var myChart = echarts.init(obj)
        // myChart.clear()
        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line'
                },
                showContent: true,
                triggerOn: 'mousemove|click',
                confine: true,
                formatter: function (params) {
                    // console.log(option)
                    // 实时获取点击点的数据
                    chartData.date = params[0].name
                    chartData.currentDataA = params[0].data
                    chartData.currentDataB = params[1].data
                    // 数据为'--'时，不可获取焦点
                    // if (params[1].data === '--') {
                    //   option.tooltip.axisPointer.type = 'none'
                    // } else {
                    //   option.tooltip.axisPointer.type = 'line'
                    // }
                }
            },
            grid: {
                top: '5%',
                left: '15%',
                // right最大值不能无限大
                right: chartData.gridRight,
                bottom: '25%'
            },
            xAxis: {
                show: false,
                type: 'category',
                boundaryGap: false,
                axisLine: {show: false},
                axisLabel: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        width: 1,
                        opacity: 0.4
                    }
                },
                axisTick: {
                    show: false
                },
                data: chartData.xAxisMarkData
            },
            yAxis: {
                type: 'value',
                splitNumber: 4,
                boundaryGap: [0, '0%'],
                axisLine: {show: false},
                min: chartData.min,
                max: chartData.max,
                interval: chartData.interval,
                axisLabel: {
                    textStyle: {
                        color: '#999',
                        fontSize: 11,
                        fontFamily: 'Helvetica'
                    },
                    formatter: function (data) {
                        if (chartData.preNav === '') {
                            return data.toFixed(4)
                        } else {
                            return data.toFixed(1) + '%'
                        }
                    }
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        width: 1,
                        opacity: 0.4
                    }
                }
                // splitLine: {
                //   show: false,
                //   lineStyle: {
                //     color: 'rgba(0,0,0,0)'
                //   }
                // }
            },
            series: [
                // 涨幅
                {
                    name: chartData.seriesNameA,
                    type: 'line',
                    symbol: 'circle',
                    symbolSize: 1,
                    showSymbol: false,
                    itemStyle: {
                        normal: {
                            color: '#6c9bd2'
                        }
                    },
                    lineStyle: {
                        width: 1
                    },
                    areaStyle: {
                        color: '#309FEA',
                        opacity: 0.15,
                        origin: 'start'
                    },
                    data: chartData.seriesDataA
                },
                // 估值
                {
                    name: chartData.seriesNameB,
                    type: 'line',
                    symbol: 'circle',
                    symbolSize: 1,
                    showSymbol: false,
                    itemStyle: {
                        normal: {
                            color: '#6c9bd2'
                        }
                    },
                    lineStyle: {
                        width: 1
                    },
                    areaStyle: {
                        color: '#309FEA',
                        opacity: 0.15,
                        origin: 'start'
                    },
                    data: chartData.seriesDataB
                }
            ]
        }
        // let {
        // 	width,
        // 	height
        // } = e;
        // echarts.setCanvasCreator(() => canvas);
        // let mChart = echarts.init(canvas, null, {
        // 	width: width,
        // 	height: height
        // })
        // canvas.setChart(mChart)
        // mChart.setOption(option)
        drawCanvas(e, canvas, option)
    },
    chartLineOneSGrid(e, canvas, chartData) { //yy
        // var myChart = echarts.init(obj)
        // myChart.clear()
        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'none'
                },
                showContent: true,
                triggerOn: 'none',
                confine: true
            },
            grid: {
                top: '5%',
                left: '15%',
                right: 0,
                bottom: '25%'
            },
            xAxis: {
                show: true,
                type: 'category',
                boundaryGap: false,
                axisLine: {show: false},
                axisLabel: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                splitNumber: 4,
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        width: 1,
                        opacity: 0.4
                    }
                },
                data: chartData.xAxisMarkData
            },
            yAxis: {
                type: 'value',
                splitNumber: 4,
                boundaryGap: [0, '0%'],
                axisLine: {show: false},
                min: chartData.min,
                max: chartData.max,
                interval: chartData.interval,
                axisLabel: {
                    show: false,
                    textStyle: {
                        color: '#999',
                        fontSize: 11,
                        fontFamily: 'Helvetica'
                    },
                    formatter: function (data) {
                        if (chartData.preNav === '') {
                            return data.toFixed(4)
                        } else {
                            return data.toFixed(2) + '%'
                        }
                    }
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        width: 1,
                        opacity: 0.4
                    }
                }
            },
            series: [
                // 涨幅
                {
                    name: chartData.seriesNameA,
                    type: 'line',
                    symbol: 'circle',
                    symbolSize: 1,
                    showSymbol: false,
                    itemStyle: {
                        normal: {
                            color: 'rgba(0,0,0,0)'
                        }
                    },
                    lineStyle: {
                        width: 1
                    },
                    areaStyle: {
                        color: '#309FEA',
                        opacity: 0,
                        origin: 'start'
                    },
                    data: chartData.seriesDataA
                },
                // 估值
                {
                    name: chartData.seriesNameB,
                    type: 'line',
                    symbol: 'circle',
                    symbolSize: 1,
                    showSymbol: false,
                    itemStyle: {
                        normal: {
                            color: 'rgba(0,0,0,0)'
                        }
                    },
                    lineStyle: {
                        width: 1
                    },
                    areaStyle: {
                        color: '#309FEA',
                        opacity: 0,
                        origin: 'start'
                    },
                    data: chartData.seriesDataB
                }
            ]
        }
        drawCanvas(e, canvas, option)
    },
    chartLineOneS1(e, canvas, chartData) {//yy
        // var myChart = echarts.init(obj)
        // myChart.clear()
        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line'
                },
                showContent: true,
                triggerOn: 'mousemove|click',
                confine: true,
                formatter: function (params) {
                    // console.log(option)
                    // 实时获取点击点的数据
                    chartData.date = params[0].name
                    chartData.currentDataA = params[0].data
                    chartData.currentDataB = chartData.seriesDataB[params[0].dataIndex]
                }
            },
            grid: {
                top: '5%',
                left: '15%',
                // right最大值不能无限大
                right: chartData.gridRight,
                bottom: '25%'
            },
            xAxis: {
                show: false,
                type: 'category',
                boundaryGap: false,
                axisLine: {show: false},
                axisLabel: {
                    show: false,
                    margin: 15
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        width: 1,
                        opacity: 0.4
                    }
                },
                axisTick: {
                    show: false
                },
                data: chartData.xAxisMarkData
            },
            yAxis: {
                type: 'value',
                splitNumber: 4,
                boundaryGap: [0, '0%'],
                axisLine: {show: false},
                min: chartData.min,
                max: chartData.max,
                interval: chartData.interval,
                axisLabel: {
                    textStyle: {
                        color: '#999',
                        fontSize: 11,
                        fontFamily: 'Helvetica'
                    },
                    formatter: function (data) {
                        if (chartData.preNav === '') {
                            return data.toFixed(4)
                        } else {
                            return data.toFixed(2) + '%'
                        }
                    }
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        width: 1,
                        opacity: 0.4
                    }
                }
            },
            series: [
                // 涨幅
                {
                    name: chartData.seriesNameA,
                    type: 'line',
                    symbol: 'circle',
                    symbolSize: 1,
                    showSymbol: false,
                    smooth: false,
                    itemStyle: {
                        normal: {
                            color: '#6c9bd2'
                        }
                    },
                    lineStyle: {
                        width: 1
                    },
                    areaStyle: {
                        color: '#309FEA',
                        opacity: 0.15,
                        origin: 'start'
                    },
                    data: chartData.seriesDataA
                }
            ]
        }
        drawCanvas(e, canvas, option)
    },
    chartLineOneSGrid1(e, canvas, chartData) { //yy
        // var myChart = echarts.init(obj)
        // myChart.clear()
        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'none'
                },
                showContent: true,
                triggerOn: 'none',
                confine: true
            },
            grid: {
                top: '5%',
                left: '15%',
                right: 0,
                bottom: '25%'
            },
            xAxis: {
                show: true,
                type: 'category',
                boundaryGap: false,
                axisLine: {show: false},
                axisLabel: {
                    show: false
                },
                splitNumber: 4,
                splitLine: {
                    show: true,
                    // interval: 15,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        width: 1,
                        opacity: 0.4
                    }
                },
                axisTick: {
                    show: false
                },
                data: chartData.xAxisMarkData
            },
            yAxis: {
                type: 'value',
                splitNumber: 4,
                boundaryGap: [0, '0%'],
                axisLine: {show: false},
                min: chartData.min,
                max: chartData.max,
                interval: chartData.interval,
                axisLabel: {
                    show: false,
                    textStyle: {
                        color: '#999',
                        fontSize: 11,
                        fontFamily: 'Helvetica'
                    },
                    formatter: function (data) {
                        if (chartData.preNav === '') {
                            return data.toFixed(4)
                        } else {
                            return data.toFixed(2) + '%'
                        }
                    }
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        width: 1,
                        opacity: 0.4
                    }
                },
                axisTick: {
                    show: false
                }
            },
            series: [
                // 涨幅
                {
                    name: chartData.seriesNameA,
                    type: 'line',
                    symbol: 'circle',
                    symbolSize: 1,
                    showSymbol: false,
                    smooth: false,
                    itemStyle: {
                        normal: {
                            color: 'rgba(0,0,0,0)'
                        }
                    },
                    lineStyle: {
                        width: 1
                    },
                    areaStyle: {
                        color: '#309FEA',
                        opacity: 0,
                        origin: 'start'
                    },
                    data: chartData.seriesDataA
                }
            ]
        }
        drawCanvas(e, canvas, option)
    },
    chartLineOneSGrid2(e, canvas, chartData) { //yy
        // var myChart = echarts.init(obj)
        // myChart.clear()
        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line'
                },
                showContent: true,
                triggerOn: 'mousemove|click',
                confine: true,
                formatter: function (params) {
                    console.log(params)
                    // 实时获取点击点的数据
                    chartData.date = params[0].name
                    chartData.currentDataA = params[0].data
                }
            },
            grid: {
                top: '8.94%',
                left: 50,
                right: 15,
                bottom: 50
            },
            xAxis: {
                show: true,
                type: 'category',
                boundaryGap: false,
                axisLine: {show: false},
                splitNumber: 20,
                axisLabel: {
                    show: false
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: false
                },
                data: chartData.xAxisMarkData
            },
            yAxis: {
                type: 'value',
                boundaryGap: [0, '0%'],
                axisLine: {show: false},
                min: chartData.min,
                max: chartData.max,
                interval: chartData.interval,
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: '#999',
                        fontSize: 11,
                        fontFamily: 'Helvetica'
                    },
                    formatter: function (data) {
                        return parseInt(data)
                    }
                },
                axisTick: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        width: 0.3
                    }
                }
            },
            series: [
                // 涨幅
                {
                    name: chartData.seriesNameA,
                    type: 'line',
                    symbol: 'circle',
                    symbolSize: 1,
                    showSymbol: false,
                    // smooth: true,
                    itemStyle: {
                        normal: {
                            color: '#FF8420',
                            lineStyle: {
                                color: '#6c9bd2',
                                width: 1.1
                            }
                        }
                    },
                    areaStyle: {
                        color: '#309FEA',
                        opacity: 0.15,
                        origin: 'start'
                    },
                    data: chartData.seriesDataA
                }
            ]
        }
        // let {
        // 	width,
        // 	height
        // } = e;
        // echarts.setCanvasCreator(() => canvas);
        // let mChart = echarts.init(canvas, null, {
        // 	width: width,
        // 	height: height
        // })
        // canvas.setChart(mChart)
        // mChart.setOption(option)
        drawCanvas(e, canvas, option)

    },
    chartLineTwo(e, canvas, chartData) {
        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'line'
                },
                showContent: true,
                triggerOn: 'mousemove|click',
                confine: true,
                formatter: function (params) {
                    // console.log(params)
                    // 实时获取点击点的数据
                    chartData.date = params[0].name
                    chartData.currentDataB = params[0].data
                    chartData.currentDataA = params[1].data
                }
            },
            grid: {
                top: '4%',
                left: 60,
                right: 0
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                axisLine: {
                    show: true,
                    interval: 5,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        opacity: 0
                    }
                },
                splitNumber: 20,
                axisLabel: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        opacity: 0.4
                    }
                },
                /* axisLabel: {
                  show: true,
                  margin: 15,
                  textStyle: {
                    color: '#999',
                    fontSize: 11
                  }
                }, */
                axisTick: {
                    show: false
                },
                data: chartData.xAxisMarkData
            },
            yAxis: {
                type: 'value',
                boundaryGap: [0, '0%'],
                axisLine: {
                    show: false
                },
                splitLine: {
                    show: true,
                    lineStyle: {
                        color: '#BAC9D8',
                        type: 'dotted',
                        opacity: 0.4
                    }
                },
                // axisLine: {show: false},
                axisLabel: {
                    textStyle: {
                        color: '#999',
                        fontSize: 11,
                        fontFamily: 'Helvetica'
                    },
                    formatter: function (data) {
                        return data.toFixed(2) + '%'
                    }
                },
                axisTick: {
                    show: false
                }
            },
            series: [
                // 涨幅
                {
                    name: '111',
                    type: 'line',
                    symbol: 'circle',
                    symbolSize: 1,
                    showSymbol: false,
                    smooth: false,
                    itemStyle: {
                        normal: {
                            color: '#fac982'
                        }
                    },
                    lineStyle: {
                        width: 1.2
                    },
                    areaStyle: {
                        color: '#309FEA',
                        opacity: 0,
                        origin: 'start'
                    },
                    data: chartData.seriesDataA
                },
                // 估值
                {
                    name: '222',
                    type: 'line',
                    symbol: 'circle',
                    symbolSize: 1,
                    showSymbol: false,
                    smooth: false,
                    itemStyle: {
                        normal: {
                            color: '#6c9bd2'
                        }
                    },
                    lineStyle: {
                        width: 1.2
                    },
                    areaStyle: {
                        color: '#309FEA',
                        opacity: 0.15,
                        origin: 'start'
                    },
                    data: chartData.seriesDataB,
                    markPoint: {
                        symbol: 'buyPointVal',
                        symbolSize: 30,
                        symbolOffset: [0, '-10%'],
                        label: {
                            color: '#fff',
                            fontSize: 10,
                            show: true
                        },
                        // data这里用来设置买点和卖点
                        data: [
                            {
                                value: '买',
                                itemStyle: {
                                    color: '#F6463A'
                                },
                                label: {
                                    show: true
                                },
                                coord: [chartData.buyPoint, chartData.buyPointVal]
                            },
                            {
                                value: '卖',
                                itemStyle: {
                                    color: '#309FEA'
                                },
                                label: {
                                    show: true
                                },
                                coord: [chartData.sellPoint, chartData.sellPointVal]
                            }
                        ]
                    }
                }
            ]
        }
        // let {
        // 	width,
        // 	height
        // } = e;
        // echarts.setCanvasCreator(() => canvas);
        // let mChart = echarts.init(canvas, null, {
        // 	width: width,
        // 	height: height
        // })
        // canvas.setChart(mChart)
        // mChart.setOption(option)
        drawCanvas(e, canvas, option)
    },
    /*
    * 3：雷达图。      》》》》参数说明如下：
    * obj: 画图挂载的元素
    * 其它......
    */
    chartRadar(e, canvas, title, legendName, indicator, seriesA, seriesB) { //yy
        var optionRadar = {
            // 这里的title用于显示中间的综合得分值
            title: {
                text: title,
                x: 'center',
                top: '42%',
                textStyle: {
                    fontFamily: 'Helvetica',
                    color: '#fff',
                    fontWeight: 'bolder',
                    fontSize: 30,
                    textShadowColor: 'rgba(36,82,154,0.4)',
                    textShadowOffsetX: 2,
                    textShadowOffsetY: 0,
                    textShadowBlur: 3
                }
            },
            legend: {
                // data: legendName,
                selectedMode: false,
                itemWidth: 12,
                itemHeight: 13,
                padding: [0, 5, 20, 5],
                itemGap: 30,
                data: [
                    {
                        name: '本基金',
                        icon: 'rect',
                        textStyle: {
                            fontSize: 12,
                            color: '#555',
                            lineHeight: 12,
                            fontFamily: 'Helvetica'
                        }
                    },
                    {
                        name: '同类平均',
                        icon: 'rect',
                        textStyle: {
                            fontSize: 12,
                            color: '#555',
                            lineHeight: 12,
                            fontFamily: 'Helvetica'
                        }
                    }
                ]
            },
            radar: [
                {
                    // 1：indicator需要配置， max 取值为最大值
                    indicator: indicator,
                    center: ['50%', '50%'],
                    radius: 80,
                    startAngle: 90,
                    splitNumber: 4,
                    shape: 'circle',
                    name: {
                        formatter: '{value}',
                        textStyle: {
                            color: '#555',
                            fontFamily: 'Helvetica'
                        }
                    },
                    nameGap: 10,
                    axisLine: {
                        show: false
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#dfdfdf',
                            width: 0.4
                        }
                    },
                    splitArea: {
                        show: false
                    }
                }
            ],
            series: [
                {
                    type: 'radar',
                    data: [
                        {
                            // 2：需要配置
                            value: seriesB,
                            name: '同类平均',
                            symbol: 'rect',
                            symbolSize: 2,
                            itemStyle: {
                                normal: {
                                    color: '#A7BBD0'
                                }
                            },
                            lineStyle: {
                                normal: {
                                    color: '#A7BBD0',
                                    width: 1,
                                    fontFamily: 'Helvetica'
                                }
                            },
                            areaStyle: {
                                normal: {
                                    color: 'rgba(167,187,205,0.5)'
                                }
                            }
                        },
                        {
                            // 1：需要配置
                            value: seriesA,
                            name: '本基金',
                            symbol: 'circle',
                            symbolSize: 4,
                            itemStyle: {
                                normal: {
                                    color: '#2995e6'
                                }
                            },
                            lineStyle: {
                                normal: {
                                    color: '#2995e6',
                                    width: 1,
                                    fontFamily: 'Helvetica'
                                }
                            },
                            // areaStyle: {
                            //   normal: {
                            //     color: 'rgba(41,149,230,0.5)'
                            //   }
                            // }
                            areaStyle: {
                                normal: {
                                    color: {
                                        type: 'linear',
                                        x: 0,
                                        y: 0.5,
                                        x2: 0.3,
                                        y2: 0,
                                        colorStops: [{
                                            offset: 0, color: 'rgba(41,149,230,1)' // 0% 处的颜色
                                        }, {
                                            offset: 1, color: 'rgba(41,149,230,0.5)' // 100% 处的颜色
                                        }]
                                    }
                                }
                            }
                        }
                    ]
                }
            ]
        }
        // 绘图开始
        // myChartRadar.setOption(optionRadar, true)

        let {
            width,
            height
        } = e;
        echarts.setCanvasCreator(() => canvas);
        let radarChart = echarts.init(canvas, null, {
            width: width,
            height: height
        })
        canvas.setChart(radarChart)
        radarChart.setOption(optionRadar, true)
    },

}
