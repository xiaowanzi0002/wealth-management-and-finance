import utils from '../utils.js'
const tgljInfo = ''
let nativeAtrr = v => v === null || v === undefined ? '' : v
const prodTypeDict = {
    '1': '公募基金',
    '2': '资管',
    '3': '收益凭证',
    '4': '天汇宝',
    '6': '银行理财',
    '7': '基金专户',
    '8': '私募基金',
    '9': 'OTC资管',
    'X': '信托'
}
//产品子类型
const prodTypeChildDict = {
    '41': '其他',
    '45': '天汇宝2号',
    '46': '天汇宝1号',
    '61': '其他',
    '62': 'OTC银行理财',
    '63': '多金银行理财',
    '64': '光大银行',
    '81': 'otc私募',
    '82': '集中交易私募',
    '91': '其他',
    '92': '券商小集合',
    '93': '券商大集合',
    '94': '期货资管',
    'X1': '其他',
    'X2': '上海信托',
    'X3': '华润信托',
    'X4': '西部信托',
    'X5': '云南信托',
    'X6': '华宝信托',
    'X7': '五矿信托'
}
//产品风险等级
const riskTypeDict = {
    '1': '低风险',
    '2': '中低风险',
    '3': '中风险',
    '4': '中高风险',
    '5': '高风险'
}

// 首页与专区页面第3、4位数字与点击区域名称对应关系
let indexPageEventName = {
    '01': '页面一级栏目tab切换区',
    '02': '页面顶部KV区',
    '03': '定制化专区',
    '04': '文字链公告区',
    '05': '页面底部KV区',
    '06': '二级栏目区',
    '08': '持仓快捷概览（未登录）区',
    '09': '持仓快捷概览（已登录）区',
    '10': '到期提醒区',
    '11': '底部悬浮按钮区'
}
// 详情页面第3、4位数字对应点击事件名称
let detailPageEventName = {
    '01': '收益率概要区',
    '02': '走势图区',
    '03': '剩余额度提醒区',
    '04': '交易时间轴提醒区',
    '05': '详情首屏展示字段区',
    '06': '详情字段“更多”',
    '07': '交易信息提醒区',
    '08': '购买按钮',
    '09': '赎回按钮',
    '10': '定投按钮',
    '11': '转让按钮',
    '12': '预约下单',
    '13': '预约赎回',
    '14': '定投子菜单的普通定投按钮',
    '15': '定投子菜单的灵犀定投按钮',
    '16': '我的客户经理',
    '17': '财富号底部按钮区入口',
    '18': '详情页营销位',
    '19': '相关资讯区',
    '20': '加自选按钮',
    '21': '底部按钮区域'
}
/** sensors埋点,所有字段值传入字符型  */
/**
 * 首页、专区浏览及一级栏目点击; 弹窗事件(2019.12.19新增)
 * @param {string} type 事件类型 'fin_1stpage_click'-理财_一级栏目页面内容点击;'fin_region_view':-理财_专区浏览;'fin_view':-理财_频道浏览
 *                              'pop_up_expose'-app弹窗曝光;'pop_up_click'-app弹窗点击;'func_view'-功能_浏览;'func_key_operation'-功能_关键操作;
 *                              'func_click'-功能入口点击
 * @param {Map} content 入参说明如下
 * type为'fin_1stpage_click'--->eventId 事件id参照indexPageEventName;code:(产品/图片/资讯)传内容代码,其余传'';name:(产品/图片/资讯)传内容名称,tab切换传tab名称;prior:优先级,若内容无优先级则从1开始排序;lname:落地页名称,lurl:落地页url
 * type为'fin_region_view'--->专区属性数据,T1000001,字段包含[ITEM_NAME_SUPER、ITEM_CODE_SUPER、IS_FUNCTION]
 * type为'fin_view'--->is_default_yn:采集不到传false;1st_tab:传一级列表名
 * type为'pop_up_expose'--->pid:弹窗ID,无则传空;ptype:弹窗类型;contt:弹窗标题;content:弹窗内容;pname:所在页面
 * type为'pop_up_click'--->pid:弹窗ID,无则传空;ptype:弹窗类型;contt:弹窗标题;content:弹窗内容;pname:所在页面;operate:操作名称
 * type为'func_click'--->fname:功能名称(专区名称);secondTap:二级tap(专区名称);pname:所在页面(eg:理财_黄金宝)
 * type为'func_view'--->fname:功能名称;pname:所在页面
 * type为'func_key_operation'--->fname:功能名称;pname:所在页面;operate:操作名称;is_free:(暂时写死为true);func_price(暂时写死为0)
 */
// 无一级页面埋点
let sFin1stpageClick = function(type, content) {

    let attr
    attr = {
        'page_pos_name': indexPageEventName[content.eventId],
        'page_pos_id': '',
        'page_pos_type': '',
        'contt_id': content.code,
        'contt_name': content.name,
        'contt_prior': content.prior,
        'contt_user_code': ''
    }
    // switch (type) {
    // 	case 'fin_1stpage_click':

    // 		break
    // 	default:
    // 		attr = content
    // 		break
    // }
    sfinCommon(content, 'shopmp_material_detail_function_click', attr, 'prod')
}
/**
 * 产品详情页、购买页、下单页神策埋点
 * @param {Map} prodInfo T1000004产品详情,字段必须包含[PROD_CODE、SHT_NAME、PROD_TYPE、PROD_TYPE_CHILD、RISK_TYPE、ORIGIN_MONEY]
 * @param {string} type 事件英文变量名
 *     'fin_prd_page_view'-理财_产品详情页浏览;'fin_prd_page_click'-理财_产品详情页点击;'fin_match_view'-理财_下单页浏览
 *     'fin_match_click'-理财_下单页点击;'fin_match_result_view'-理财_下单结果页浏览;'fin_match_result_click'-理财_下单结果页点击
 *     'fin_prd_2ndpage_view'-理财_产品详情二级页浏览;'fin_prd_2ndpage_click'-理财_产品详情二级页点击
 * @param {string} eventId 点击事件id,详情页字典参照detailPageEventName，购买页参照buyAndResPageEventName,结果页参照resultPageEventName
 * @param {Map} content 入参说明如下:
 *    产品详情页点击事件--->code:营销位传内容代码,其余传'';name:营销位传内容名称,详情首屏展示字段区域传字段名称,其余传点击对应区域名称,如购买按钮文案;prior:优先级,若内容无优先级则从1开始排序;lname:落地页名称,lurl:落地页url
 *    产品详情二级页浏览--->name:理财详情二级页面名称,如产品档案
 *    产品详情二级页点击--->pname:二级页面名称;ppos:更多字段区域名称;name:传点击对应区域名称,如投资目标;prior:优先级,若内容无优先级则从1开始排序;sname:下级子内容名称,如协议名称,lurl:落地页url
 *    下单页点击事件--->name:内容名称,如果事件为协议查看链接,传所点协议名称;其余传点击区域名称,如购买按钮文案
 *    结果页浏览事件--->result:传交易结果,成功或失败;orderAmt:下单金额
 *    结果页点击事件--->code:营销位传内容代码,其余传'';name:营销位传内容名称,其余传点击对应区域名称,如购买按钮文案;prior:优先级,若内容无优先级则从1开始排序;lurl:落地页url
 *
 * @param {Map} secColumn 营销位传,二级栏目属性,T1000001数据,字段必须包含[ITEM_NAME、ITEM_CODE、PRIORITY]
 */

// 临时：下单结果页没有tglj_parameter参数，暂时传空，下单页按钮点击事件暂时传tglj_parameter参数--20191029
let sfinPrd = function(prodInfo, eventId, content, OperInfos) {
    let anotherAttr = {
        'page_pos_name': detailPageEventName[eventId],
        'page_pos_id': '',
        'page_pos_type': '',
        'contt_id': content.code,
        'contt_name': content.name,
        'contt_prior': content.prior,
        'contt_user_code': '',
        ...OperInfos
    }
    sfinCommon(prodInfo, 'shopmp_material_detail_function_click', anotherAttr, 'prod')
}
/**
 * 理财_产品详情页浏览时长
 * @param {Object} prodInfo
 * @param {Object} type
 * @param {Object} duration
 */
let prdPageViewDuration = function(prodInfo, duration, sTime, eTime) {
    let anotherAttr = {
        sTime,
        eTime,
        'sessionid':utils.getSessionId(),
        'duration': duration,
    }
    sfinCommon(prodInfo, 'shopmp_material_detail_view', anotherAttr, 'pageView')
}
/**
 * 理财_二级栏目点击
 * @param {string} region 栏目区域,传内容区、更多、栏目标题区、换一换
 * @param {Map} secColumn 二级栏目数据,T1000001数据,字段必须包含[ITEM_NAME_SUPER、ITEM_CODE_SUPER、IS_FUNCTION、ITEM_NAME、ITEM_CODE、SHELF_TYPE、SHELF_BROWSE_WAY、PROD_WITH_ENHANCED]
 * @param {Map} content  内容区域数据,点击区域为内容区需传.T1000002数据,字段包含[PROD_CODE、SHT_NAME、PRIORITY、USER_RULE_ID],T1000003数据,字段包含[ADVERTISE_ID、ADVERTISE_NAME、PRIORITY、USER_RULE_ID]
 */

/*
 page_pos_type	点击区域类型	字符串	1、如为点击二级栏目区域类型，则填写二级栏目区。
 2、其他类型还包括：详情更多展示字段区、按钮点击区、走势图区、联系客服区等，如实填写。

 page_pos_name	点击区域名称	字符串	1、如为点击二级栏目区域类型，则填写二级栏目名称。
 2、如为点击详情更多展示区类型，则对应为该字段名称，如基金经理、产品档案、基金诊断等。
 3、如为按钮点击区、走势图区、联系客服区等，则如实直接填写相应区域名称（不带区字）。

 page_pos_id	点击区域ID	字符串	1、当点击页面区域为“二级栏目”，则展示为二级栏目ID。
 2、当为其他区域且无ID，则无需填写

 contt_name	内容名称	字符串	1、当点击页面区域为“二级栏目”，则展示为对应二级栏目所下挂内容名称。
 2、如为点击详情更多展示字段且下方有具体展开内容，例如合同协议字段下面会有一个或多个协议时，根据用户点击具体的协议，记录相应协议名称。
 3、当为其他页面区域类型时，则展示该点击动作所对应区域名称，如按钮中文字“XXXX”
 contt_id	内容ID	字符串	1、当点击页面区域为“二级栏目”，则展示为对应二级栏目所下挂内容ID，如产品代码或资讯id或图片id。
 2、当为其他区域且无ID，则无需填写
 contt_prior	内容展示优先级	字符串	如有优先级则展示，如无则从1开始顺序编号，如仅有一条内容，则依然填入1
 contt_user_code	内容可见用户规则代码	字符串	排期优先级低

 */
// has_authorize	是否要授权	BOOL
// is_run_APP	是否唤起APP	BOOL
// run_APP_url	唤起APP地址	字符串
// has_popup	是否有弹窗	BOOL	老版本埋法，20年12月10日之后新埋点需求均不再埋此，存量不变
// button_name	按钮名称	字符串	老版本埋法，20年12月10日之后新埋点需求均不再埋此，存量不变
// key_word	关键字	字符串	老版本埋法，20年12月10日之后新埋点需求均不再埋此，存量不变
// link_type	链接类型	字符串	客服聊天，H5链接跳转，唤起App
// link_name	链接名称	字符串	即落地页名称，如有则填
// url	跳转链接地址	字符串	即落地页地址
//'page_pos': detailPageEventName[eventId],

let transOperaInfo = function(OperInfos) {
    //小程序引导类型 0唤起App  1直接跳转  2 客服回复 3 跳转其他小程序
    let btnType = ['唤起App', 'H5直接跳转', '客服回复', '跳转其他小程序']
    if (OperInfos.appGuideIsJhCall === '1') { // 如果唤起app设置为true 则为唤起app
        OperInfos.appGuideType = 0
    }
    return {
        'has_authorize': OperInfos.appGuideTelAuth || '',
        'is_run_APP': OperInfos.appGuideIsJhCall || '',
        'run_APP_url': OperInfos.appGuideJhCallUrl || '',
        'link_type': btnType[OperInfos.appGuideType] || '',
        'link_name': '',
        'url': (OperInfos.appGuideType == 1 ? OperInfos.appGuideH5LinkUrl : OperInfos.appGuideJhCallUrl) || '',
    }
}

let sFin2ndpageClick = function(index, region, secColumn, content, OperInfos) {
    let attr = {
        'page_pos_name': secColumn.ITEM_NAME,
        'page_pos_id': secColumn.ITEM_CODE,
        'page_pos_type': secColumn.SHELF_TYPE, //||'二级栏目区域'
        'contt_id': region === '内容区' ? content.PROD_CODE || content.ADVERTISE_ID : '', //
        'contt_name': region === '内容区' ? content.SHT_NAME || content.ADVERTISE_NAME : '', //
        'contt_prior': region === '内容区' ? content.PRIORITY : '', //
        'contt_user_code': region === '内容区' ? content.USER_RULE_ID : '', //可能要改成小店的  内容可见用户规则代码
        ...OperInfos
    }
    sfinCommon(secColumn, 'shopmp_material_detail_function_click', attr, 'prod')
}

/**
 * 设置公共变量的通用埋点方式,默认{}
 * @param {object} commonVar 公共变量设置时需要取的参数
 * @param {String} type 埋点事件类型
 * @param {object} anotherAttr 非公共属性集合
 * @param {String} commonType 取哪类公共变量设定
 */
function getMaterialType() {
    let pages = getCurrentPages();
    let curPage = pages[pages.length - 1].route;
    return curPage === 'pageFkybGmjj/gongMufunds/area/area' ? '场景页' : '产品'
}

let sfinCommon = function(commonVar, type, anotherAttr, commonType) {
    // coverPicUrl
    // describe
    if(commonVar){
        console.log('sfinCommon埋点：：', commonVar)
        let attr = {
            'tglj_parameter': tgljInfo,
            'material_id': commonVar.PROD_CODE || commonVar.ITEM_CODE_SUPER,
            'material_name': commonVar.SHT_NAME || commonVar.ITEM_NAME_SUPER,
            'material_author': '',
            'material_type': commonType == 'pageView' ? getMaterialType() : '产品',
            'page_level': '一级页面', //默认为“一级页”，可选设置“二级页”
            'material_second_cate': prodTypeDict[commonVar.PROD_TYPE] || '',
            'material_first_cate': prodTypeChildDict[commonVar.PROD_TYPE_CHILD] || '',
            'material_risk_level': commonVar.NAV_TYPE?.riskValue || riskTypeDict[commonVar.RISK_TYPE] || '', //riskTypeDict[commonVar.RISK_TYPE] || '',
            'min_match_amt': parseFloat(commonVar.NAV_TYPE?.ORIGIN_MONEY) || 0,
            'is_hot_material': false, //必须是bool值
            'coverPicUrl': commonVar.coverPicUrl || commonVar.SHOP_SHARE_PIC_URL || '', //
            'describe': commonVar.describe || '', //
        }
        Object.assign(attr, anotherAttr)
    }
}

export {
    prdPageViewDuration, //理财_产品详情页浏览时长
    sFin2ndpageClick, // sensors-理财_二级栏目点击
    sfinPrd, // sensors-产品详情页、购买页、下单页
    sFin1stpageClick, //sensors-理财_一级栏目页面内容点击
}
