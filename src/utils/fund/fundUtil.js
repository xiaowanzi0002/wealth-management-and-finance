// import Vue from '@/main'
import utils from '@/utils/utils.js'
import {	sFin2ndpageClick 	} from '@/utils/fund/scBuyPoint.js'
function didIncluded(str, child) {
	return utils.valideNull(str) ? false : str.includes(child)
}
let OperInfos={}
export function pageJump(url,item){
	if (item.MORE_TYPE === '2' || item.MORE_TYPE === '3') {
		sFin2ndpageClick(-1,'更多',item,item,{})
	}else{
		sFin2ndpageClick(-1,'内容区',item,item,{})
	}
	if(utils.valideNotNull(item.XIAODIAN_URL)||utils.valideNotNull(item.XIAODIAN_LINK)){
		let url=item.XIAODIAN_URL||item.XIAODIAN_LINK
		uni.navigateTo({
				url:url.indexOf('/')===0?url:"/"+url
		})
		
		// let url=utils.valideNotNull(item.XIAODIAN_URL)?item.XIAODIAN_URL:item.XIAODIAN_LINK
		// let urlArr=url.split('outerUrl=')//小店配置的内嵌页面
		// if(urlArr.length==2&&urlArr[1].contain('buy/')){
		// 	return
		// }else{
		// 	uni.navigateTo({
		// 		url:urlArr.length==2?  urlArr[0]+ 'outerUrl='+encodeURIComponent(urlArr[1]):url
		// 	})
		// }
		
	}else{
		if(!url||(url.indexOf('/oauth/')>-1)){//拦截为空和需要授权的内嵌页面
			return
		}
		let toUrl= trans2XDUrl(url)
		let pages = getCurrentPages();
		let curPage = pages[pages.length - 1].route;
		let options=pages[pages.length - 1].options
		let code= utils.getUrlParam('itemCode', url)
		if(!didIncluded(toUrl,curPage)){///
			uni.navigateTo({
				url:toUrl
			})
		}else if(code!==options.itemCode) {
			uni.navigateTo({
				url:toUrl
			})
		}
	}

}
export function trans2XDUrl(url) { //跳转小店页面路径转化
	console.log('非小店链接，需转换URL：：' + url)
	let resUrl = ''
	let urlMapList = {
		'fundsDetail': ['https://i.gtja.com/mall/eplus/prodDetails.eplus.html',
			'/pageFkybGmjj/gongMufunds/fundsDetail/fundsDetail'
		],
		'area': ['https://i.gtja.com/mall/eplus/area.eplus.html', '/pageFkybGmjj/gongMufunds/area/area']
	}
	// MORE_TYPE
	if (didIncluded(url, urlMapList['fundsDetail'][0])) {
		resUrl= urlMapList['fundsDetail'][1] + '?prodCode=' + utils.getUrlParam('prodCode', url)
	} else if (didIncluded(url, urlMapList['area'][0])) {
		resUrl= urlMapList['area'][1] + '?itemCode=' + utils.getUrlParam('itemCode', url)
	}
	//  else if (didIncluded(url, urlMapList['newFund'][0])) {
	// 	resUrl= urlMapList['newFund'][1] + '?prodCode=' + utils.getUrlParam('prodCode', url)
	// }
	 else {
		let outUrl = url
		if (didIncluded(url, 'gtjayyz://jumpfunc') && didIncluded(url, 'linkurl=')) {
			outUrl = url.split('linkurl=')[1]
			console.log('内嵌页转换 后URL：：' + outUrl)
		}else{
			console.log('内嵌页但未获取到 linkurl：：' + outUrl)
		}
		resUrl='' //'/pages/outerPage/outerPage?outerUrl=' + encodeURIComponent(outUrl) //不允许小店内嵌页面
	}
	
	return resUrl
}

export function dealOperInfo(operInfo, PrdAttr, options, type) { // 'area' 'fundsDetail'  'newFund'
	console.log(PrdAttr)
	let urlMapList = {
		'fundsDetail': ['APP_FLOAT_URL', //sceneUrl对应PrdAttr字段
			'pageFkybGmjj/gongMufunds/fundsDetail/fundsDetail', // 海报绘制二维码 页面路径
			'https://apicdn.app.gtja.com/public/wlc/xd/img/detailPosterImage.png', //海报背景图
			'https://apicdn.app.gtja.com/public/wlc/xd/img/detailShareImage.jpg', //分享缩略图
		],
		'area': ['ZONE_INFO_URL',
			'pageFkybGmjj/gongMufunds/area/area',
			'https://apicdn.app.gtja.com/public/wlc/xd/img/listPosterImage.png', //海报背景图
			'https://apicdn.app.gtja.com/public/wlc/xd/img/listShareImage.jpg', //分享缩略图
		]
	}
	let defaultPrd = {
		'SHOP_TO_OPEN_ACCOUNT': ''
	}
	try {
		const servicePopPic = 'https://apicdn.app.gtja.com/public/wlc/xd/img/popup.gif' //客服页面 气泡图片
		const servicePopPic1 = 'https://apicdn.app.gtja.com/public/wlc/xd/img/popup2.png' //客服页面 气泡图片
		let manager = utils.getManagerFromLocal() || {tgFlag:'016575'}
		const linkOpenApp = 'gtjayyzplus://jumpfunc?funid=10018&isreload=0&linkurl='
		let  anther='tglj_info=tglj_userCode:;tglj_activityCode:201903_GTJAXDKH;tglj_channel:wechat_XD;tglj_Code:'+manager.tgFlag
		// 删除理财频道tjlg_info
		let detailUrl = PrdAttr[urlMapList[type][0]]
		detailUrl = detailUrl.replace(/[?|&]tglj_info=(.*)(?=[&|$])?/, '')
		operInfo.appGuideJhCallUrl = linkOpenApp + detailUrl + (detailUrl.indexOf('?') >= 0 ? '&' :'?') +anther
		operInfo.appGuideJhCallSpecLink='0'
		if (utils.valideNotNull(PrdAttr['SHOP_TO_OPEN_ACCOUNT'])) {
			operInfo.appGuideType = 1
			operInfo.appGuideButton = '立即开户'
		}
	
		operInfo.sceneUrl = (PrdAttr['ITEM_NAME'] || '') + '_' + '' + '_' + ((PrdAttr[urlMapList[type][0]]) || '') //场景名称（列表名称或产品名称或活动名称或其他来源名称）_场景描述_场景链接，没有值都传空
		
		operInfo.materialType = 2 //2 产品
		operInfo.materialId = PrdAttr['ITEM_CODE']
		operInfo.appGuideKeyword = PrdAttr['ITEM_CODE']
		operInfo.title = PrdAttr['ITEM_NAME']
		operInfo.appGuideHasDefaultPic = 1
		// operInfo.appGuideDefaultPicUrl =Vue.prototype.$is_android?servicePopPic1:servicePopPic ,//android 用静态图片
		operInfo.appGuidePopupButton='确认'
		operInfo.showShare = !!PrdAttr['SHARE_PIC_ON_MOBILE_URL']
		operInfo.share = {
			title: PrdAttr['ITEM_NAME'],
			imageUrl: PrdAttr['SHARE_PIC_ON_MOBILE_URL'] || urlMapList[type][3],
			path: '/' + urlMapList[type][1]
		}
		OperInfos=operInfo
		// Vue.prototype.$store.commit('POSTER_INFO', { //提交海报绘制数据
		// 	bgUrl: PrdAttr['LOGO_URL'] || urlMapList[type][2],
		// 	shareUrl: PrdAttr['SHARE_PIC_ON_MOBILE_URL'] || urlMapList[type][3], //分享配图
		// 	type: 'poster',
		// 	posterType: 1, // PrdAttr['LOGO_URL']?1:2, //2 请求默认海报  有默认cdn 图像地址 不通过接口请求
		// 	posterShowManagerCard: 1, //是否绘制 客户经理头像昵称
		// 	defText: PrdAttr['ITEM_NAME'] || '', //绘制文案
		// 	isGongMuJJ: PrdAttr['LOGO_URL']?false:true,
		// 	router: urlMapList[type][1],
		// 	options: options
		// })
	} catch (e) {
		console.log(e)
		//TODO handle the exception
	}

	return operInfo
}

export function canvasOptionInit(option, IDKey) {
	const scene = decodeURIComponent(option.scene)
	let sceneArr = []
	if (!utils.valideNull(option.scene)) {
		sceneArr = scene.split('_')
		option.myManagerNo = sceneArr[0]
		option[IDKey] = sceneArr[1]
	}
	return option
}
