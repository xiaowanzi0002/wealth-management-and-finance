import utils from './utils.js'

// import md5 from './md5.min.js'
function User(data) {
    this.nickName = ''
    this.headImgUrl = ''
    this.employeeNo = ''
    this.userId = ''
    this.openId = ''
    this.unionId = ''
    this.telCode = ''
    this.telephone = ''
    this.tgFlag = ''
    this.loginStatus = false
    this.wxCode = ''
    this.followers = ''
    this.starGrade = 1
    // viewType 0是客户 1 员工
    this.viewType = 0
}

User.prototype.setUser = function (data) {
    this.loginStatus = data.loginStatus | false
    this.openId = data.openId
    this.unionId = data.unionId
    this.userId = data.userId
    this.employeeNo = data.employeeNo // md5(data.employeeNo)
    this.tgFlag = data.tgFlag
    this.nickName = data.nickName
    this.headImgUrl = data.headImgUrl
    this.telephone = utils.valideNull(data.phoneNumber) || data.phoneNumber == 'null' ? (utils.valideNull(data.telephone) ||
    data.phoneNumber == 'null' ? this.telephone : data.telephone) : data.phoneNumber || '111'
    //this.telephone=null
    this.wxCode = data.wxCode
    this.followers = data.followers || ''
    this.starGrade = data.starGrade || 1

    console.log("user-employeeNo:", utils.valideNull(data.employeeNo))
    this.viewType = (data.viewType !== 0 && utils.valideNull(data.viewType)) ? ((utils.valideNull(data.employeeNo) || !
        data.loginStatus) ?
        0 : 1) : data.viewType // utils.valideNull(data.employeeNo) ? 0 : 1,
    //data.viewType !== 0 && utils.valideNull(data.viewType) ? (utils.valideNull(data.employeeNo) ? 0 : 1) :data.viewType
    if (data.viewType == 0) {
        this.viewType = 0
    }
}
User.prototype.getUser = function () {
    // console.log('getUser',this)
    return {
        loginStatus: this.loginStatus,
        openId: this.openId,
        unionId: this.unionId,
        userId: this.userId,
        employeeNo: this.employeeNo,
        tgFlag: this.tgFlag,
        nickName: this.nickName,
        headImgUrl: this.headImgUrl,
        telephone: this.telephone,
        wxCode: this.wxCode,
        viewType: this.viewType,
        followers: this.followers,
        starGrade: this.starGrade
    }

}
User.prototype.changeView = function () {
    this.viewType = !this.viewType ? 1 : 0
}


export default {
    User
}
