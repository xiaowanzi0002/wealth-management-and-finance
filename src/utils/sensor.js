// 埋点初始化页面 区分微信端和H5端埋点方法 封装为统一调用入口 sensorEvt
// #ifdef MP-WEIXIN
import sensors from "sa-sdk-miniprogram";
import * as API from "../api/api";
var code;
var uniPlatform;
uni.getSystemInfo({
  success: (res) => {
    console.log(res, "onLaunchosName");
    uniPlatform = res.platform;
    uni.login({
      provider: "weixin", //使用微信登录
      onlyAuthorize: true,
      success: async (loginRes) => {
        code = loginRes.code;
        getAuth(code); //
      },
    });
  },
});
let serverUrl = "";
process.env.NODE_ENV === "development"
  ? (serverUrl = "https://newdsj.gtjadev.com:9091/sa?project=weixin")
  : (serverUrl = "https://newdsj.gtjadev.com/sa?project=weixin");

sensors.setPara({
  name: "sensors",
  allow_amend_share_path: true, //自动采集分享信息
  server_url: serverUrl,
  // server_url: "https://newdsj.gtjadev.com:9091/sa?project=weixin",
  autoTrack: {
    appLaunch: true, //是否采集 $MPLaunch 事件，true 代表开启。
    appShow: true, //是否采集 $MPShow 事件，true 代表开启。
    appHide: true, //是否采集 $MPHide 事件，true 代表开启。
    pageShow: true, //是否采集 $MPViewScreen 事件，true 代表开启。
    pageShare: true, //是否采集 $MPShare 事件，true 代表开启。
    mpClick: false, // 默认为 false，true 则开启 $MPClick 事件采集
    mpFavorite: true, // 默认为 true，false 则关闭 $MPAddFavorites 事件采集
  },
  show_log: true,
});
const getAuth = async (codeAuth) => {
  const params = {
    jsCode: codeAuth,
    storeFlag: "shandong",
    nickName: "",
    headImgUrl: "",
  };
  let res = await API.getAuth(params);
  if (res) {
    // debugger;
    const app = getApp();
    app.sensors._.info.currentProps.user_type = res.employeeNo
      ? "客户经理"
      : "客户";
    app.sensors._.info.currentProps.product_name = "山东分公司展业小程序";
    app.sensors._.info.currentProps.open_id = res.openId;
    app.sensors._.info.currentProps.union_id = res.unionId;
    app.sensors._.info.currentProps.union_id = res.unionId;
    app.sensors._.info.currentProps.cust_id = "";
    app.sensors._.info.currentProps.mobile = res.phoneNumber;
    app.sensors._.info.currentProps.platform_type =
      uniPlatform == "web" || uniPlatform == "h5" ? "H5" : "小程序";

    // sensors.registerApp({
    //   user_type: res.employeeNo ? "客户经理" : "客户",
    //   union_id: res.unionId,
    //   mobile: res.phoneNumber,
    //   cust_id: "",
    //   open_id: res.openId,
    //   platform_type:
    //     uniPlatform == "web" || uniPlatform == "h5" ? "H5" : "小程序",
    // });

    sensors.init();
  }
};

sensors.quick("autoTrackSinglePage", {
  platForm: "H5",
});

function sensorEvt(evtId, sendObj) {
  const app = getApp();
  app.sensors.track(evtId, sendObj);
  sensors.track(evtId, sendObj);
}
export default {
  sensorEvt: sensorEvt,
};
