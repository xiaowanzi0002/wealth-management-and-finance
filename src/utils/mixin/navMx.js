import Vue from 'vue'
import utils from '../utils.js'

export default {
	install(Vue) {
		Vue.mixin({
			data() {
				return {
					nHasBack: false,
					nBgColor: '#fff',
					ntextColor: '#222222',
					nTitle: ' '
				}
			},
			onHide(){
				this.nHasBack=true
			},
			onUnload() {
				this.nHasBack=true
			},
			onLoad() {
				utils.setNavInfo()
				let pages = getCurrentPages();
				this.nHasBack = pages.length > 1
				console.log('hello navMix', pages)
			},
			onShow() {

			},
			onReady() {
				const that = this;
			},
			methods:{
				setNavTitle(tit){
					console.log('title nv')
					this.nTitle=tit
				}
			}
		})
	}
}
