import utils from'../utils.js'
export default{
    data(){
        return {
            //设置默认的分享参数
            share:{
                title:'ALAPI',
                path:'/pages/tabbar/index/index',
                imageUrl:'https://dl.app.gtja.com/public/wlc/xd/img/real-time.png'
                // desc:'',
                // content:''
            },
			MXSaveSCData:{},//神策埋点数据
			options:{},
			test:'mytesttest'
        }
    },
	onLoad(){
		wx.showShareMenu({
		  withShareTicket: false,
		  menus: ['shareAppMessage', 'shareTimeline']
		})
		console.log('mixin----onLoad')
	},
	onShow(){
		console.log('onShow--mixinmixinmixin')
	},
	onReady(){
		const that = this;
		console.log('onReady--mixinmixinmixin')
	},
    onShareTimeline(res) {
		const app = getApp()
		app.sensors.para.autoTrack.pageShare =this.MXSaveSCData
		return utils.tranShareInfo('',this.share.title,this.share.imageUrl ,this.share.path,this.options,'isShareTimeline')
    },
	onShareAppMessage(res) {
		let sfrom = '右上角按钮'
		if (!utils.valideNull(res.target)) {
			sfrom = res.target.dataset.sfrom ? '页面内按钮' : ''
		}
		this.MXSaveSCData.share_method=sfrom
		const app = getApp()
		app.sensors.para.autoTrack.pageShare=this.MXSaveSCData
		// if(this.share.path.indexOf('/pageFkybGmjj/gongMufunds/fundsDetail/fundsDetail')>-1||this.share.path.indexOf('/pageFkybGmjj/gongMufunds/area/area')>-1){
		// 	this.options.tglj_timeStamp=new Date().getTime()
		// }
		return utils.tranShareInfo('',this.share.title,this.share.imageUrl ,this.share.path,this.options,'')
	}
	
}