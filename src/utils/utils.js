// import Vue from 'vue'
import { inject } from 'vue';

import User from './user.js'
import Manager from '@/utils/manager.js'
import indexAPI from '@/api/index.js'
import base from './base.js'
import JSEncrypt from 'jsencrypt-plus';

import store from '@/store/vuex'

const VUE_INSTANCE_KEY = Symbol();
export const provideVueInstance = (app) => {
  app.provide(VUE_INSTANCE_KEY, app);
};

// 在公共 JavaScript 文件中使用 Vue 实例
export const useVue = () => {
  const vueInstance = inject(VUE_INSTANCE_KEY);

  if (!vueInstance) {
    console.error('Vue instance not injected. Make sure to call provideVueInstance with a Vue instance.');
  }

  return vueInstance;
};

/* 格式化日期
 * @param {string} originDate 20180101
 * @param {string} seperator '/'
 */
const app = getApp();
let user = new User.User();


// 校验为空
function valideNull(val) {
  return (
    typeof val == "undefined" ||
    val == "" ||
    val == null ||
    JSON.stringify(val) == "{}"
  );
}

function valideNotNull(obj) {
  return !valideNull(obj);
}

/**
 * 补零
 */
function addZero(val) {
  return val >= 10 ? val : `0${val}`;
}

function getUrlParam(key, httpUrl) {
  let url = httpUrl.split("?")[1];
  var regExp = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
  var rep = url.match(regExp);
  if (rep != null) return unescape(rep[2]);
  else return "";
}

function getSessionId() {
  //shopid|unionid|time
  let manager = getManagerFromLocal() || {};
  let user = getUserFromLocal() || { unionId: "" };
  let shopId =
    "01" + (valideNull(manager.employeeNo) ? "DDDDDD" : manager.employeeNo);
  return shopId + (user ? user.unionId : "") + getApp().globalData.gTimeStamp;
}

function setNavInfo() {
  let pages = getCurrentPages();
  let curPage = pages[pages.length - 1].route;
  const systemInfo = uni.getSystemInfoSync();
  let myOptions = pages[pages.length - 1].options;
  console.log(getApp().globalData.scene);
  let sence = getApp().globalData.scene;
  if (curPage == "pages/tabbar/index/index") {
    getApp().globalData.needXD = false;
  }
  const Vue = useVue();
  if (pages.length > 1) {
    store.commit("NAV_INFO", {
      hasNavBack: true,
      needGoStore: !(
        valideNull(myOptions.scene) && valideNull(myOptions.fromCard)
      ),
      navHeight: systemInfo.statusBarHeight + 46,
    });
  } else {
    store.commit("NAV_INFO", {
      hasNavBack: false,
      needGoStore: !(
        valideNull(myOptions.scene) && valideNull(myOptions.fromCard)
      ),

      navHeight: systemInfo.statusBarHeight + 46,
    });
  }
}

function getObjFromLocal(key) {
  let ss = uni.getStorageSync(key);
  return valideNull(ss) ? null : JSON.parse(ss);
}

function getUserFromLocal() {
  let data = getObjFromLocal("GLOBAL.USER");
  if (valideNull(data)) {
    return;
  }
  return data;
}

function saveManagerInfo(managerInfo) {
  // if(managerInfo.cardPicIsBase64){
  // 	managerInfo.cardPicUrl='data:image/png;base64,'
  // }
  saveObjToLocal("GLOBAL.MANAGER", managerInfo);
}

let firstPage = "";

function getSendTitle(myPath, options) {
  let pages = getCurrentPages();
  let curPage = "";
  let myOptions = "";
  let curIdx = pages.length - 1;
  if (!myPath && !options) {
    curPage = pages[curIdx].route;
    myOptions = pages[curIdx].options;
  } else {
    curPage = myPath;
    myOptions = options;
  }
  let pageName = "";
  let pageType = "";
  let firstCate = "";
  let secondCate = "";
  switch (curPage) {
    case "pages/tabbar/index/index":
      let tabCur = 0;
      if (pages.length) {
        tabCur = pages[curIdx]["__data__"]["TabCur"];
      }
      pageName = "首页";
      pageType = tabCur === 1 ? "首页_产品" : "其他";
      break;
    case "pages/managerDetail/managerDetail":
      pageName = "客户经理详情页";
      pageType = "其他";
      break;
    case "pages/activity/activity":
      if (myOptions.pageType == "prdct") {
        pageName = "产品详情页";
        pageType = "产品";
      } else {
        pageName = "活动详情页";
        pageType = "活动";
      }
      break;
    case "pageArticles/liveReports/liveReports":
      pageName = "实时快讯";
      pageType = "资讯";
      break;
    case "pageArticles/xjAnalysis/xjAnalysis":
      pageName = "小君解盘";
      pageType = "资讯";
      break;
    case "pageArticles/reportsList/reportsList":
      if (myOptions.type == "dailyReport") {
        pageName = "每日早报";
        pageType = "资讯";
      } else {
        pageName = "鸿鹄观市";
        pageType = "资讯";
      }
      firstCate = "一级分类";
      break;
    case "pageArticles/specialNews/specialNews":
      pageName = "专题资讯一级页面";
      pageType = "资讯";
      firstCate = "一级分类";
      break;
    case "pageArticles/reportsList/reportsList":
      pageName = "专题资讯二级页面";
      pageType = "资讯";
      firstCate = "";
      secondCate = "二级分类";
      break;
    case "pageArticles/course/course":
      pageName = "课程一级页面";
      pageType = "课程";
      firstCate = "一级分类";
      break;
    case "pageArticles/courseList/courseList":
      pageName = "课程二级页面";
      pageType = "课程";
      secondCate = "二级分类";
      break;
    case "pages/newsDetail/NewsDetail":
      pageName = "资讯详情页";
      pageType = "资讯";
      break;
    case "pages/outerPage/outerPage":
      pageName = "H5内嵌页";
      pageType = "其他";
      break;
    case "pages/picView/picView":
      pageName = "图片预览";
      pageType = "其他";
      break;
    case "pages/createPoster/createPoster":
      pageName = "生成海报页";
      pageType = "其他";
      break;
    case "pages/wxAuthorize/wxAuthorize":
      pageName = "用户授权页";
      pageType = "其他";
      break;
    case "pages/login/login":
      pageName = "员工登录页";
      pageType = "其他";
      break;
      break;
    case "pages/tabbar/tool/tool":
      pageName = "工具海报";
      pageType = "其他";
      break;
    case "pageTool/tool/canvasPoster":
      pageName = "工具生成海报页";
      pageType = "其他";
      break;
    case "pageTool/level2/level2":
      pageName = "level2购买页";
      pageType = "其他";
      break;
    case "pagesTool/level2/buySuccess":
      pageName = "level2购买成功页";
      pageType = "其他";
      break;
    case "pagesTool/level2/Agreement":
      pageName = "level2服务协议页";
      pageType = "其他";
      break;
    case "pageArticles/junHongNews/junHongNews":
      pageName = "君弘专题文章列表页服务协议页";
      pageType = "资讯";
      break;
    case "pageArticles/junHongWriterList/junHongWriterList":
      pageName = "君弘作者文章列表页";
      pageType = "资讯";
      break;
    case "pageArticles/FkybIndex":
      pageName = "风口研报二级页面";
      pageType = "资讯";
      firstCate = "";
      secondCate = "二级分类";
      break;
    case "pageArticles/FkybNewsDetail/FkybNewsDetail":
      pageName = "风口研报详情页";
      pageType = "资讯";
      break;
    default:
      break;
  }
  return {
    pageName,
    pageType,
    firstCate,
    secondCate,
  };
}

function getManagerFromLocal() {
  let data = getObjFromLocal("GLOBAL.MANAGER");
  if (valideNull(data)) {
    // data.tgFlag='016575'
    // data.employeeNo='DDDDDD'
    return;
  }
  // data.cardPicUrl=store.getters.gMcardPicUrl
  // console.log('store.getters.gMcardPicUrl',store.getters.gMcardPicUrl)
  return data;
}

function timeHM(time) {
  if (valideNull(time)) {
    return "";
  }
  return time.split(" ")[1].substring(0, 5);
}

let tryManageer = 0;
var getCardInfo = function (myManagerNo, NoFrom) {
  const Vue = useVue();

  // DDBDMG
  const app = getApp();
  let pages = getCurrentPages();
  let curPg = pages.length > 0 ? pages[pages.length - 1].route : "";
  let myOptions = pages.length > 0 ? pages[pages.length - 1].options : "";
  let pgLgnToIdx = getApp().globalData.loginToIdx;
  let that = this;
  let user = getUserFromLocal() || {};
  let manager = getManagerFromLocal() || {};

  // // 分享使用
  // saveUserShare(user)

  console.log("debugger：：", myManagerNo, NoFrom, myOptions);
  if (!valideNull(myOptions.myManagerNo)) {
    // 页面有分享的客户经理员工号
    if (
      myOptions.myManagerNo != myManagerNo &&
      NoFrom != "register" &&
      NoFrom != "changeView"
    ) {
      // 以分享的客户经理员工号为准，不是页面分享的客户经理不请求数据 //授权操作除外
      return;
    }
  } else if (
    NoFrom == "appLaunch" &&
    user.employeeNo != manager.employeeNo &&
    myOptions.scene
  ) {
    //通过扫描二维码进去
    const scene1 = decodeURIComponent(myOptions.scene);
    let myNumber = scene1.split("_")[0];
    if (myNumber != user.employeeNo) {
      console.log("appLaunch  return  employeeNo", myNumber);
      return;
    }
  } else if (NoFrom == "appLaunch" && myManagerNo == manager.employeeNo) {
    return;
  }
  //公募测试 埋点初始化
  if (
    pages.length > 1 &&
    curPg != "pageFkybGmjj/gongMufunds/fundsDetail/fundsDetail" &&
    curPg != "pageFkybGmjj/gongMufunds/area/area" &&
    curPg != "pages/login/login" &&
    curPg != "pages/wxAuthorize/wxAuthorize" &&
    curPg != "pages/managerDetail/managerDetail"
  ) {
    console.log("curPg!=login", curPg != "pages/login/login");
    return;
  } else if (pgLgnToIdx) {
    //登录跳转到首页时不改变本地客户经理数据
    console.log("pagefrom=login", pgLgnToIdx);
    getApp().globalData.loginToIdx = false;
    Vue.prototype.$store.commit("MANAGER_INFO", manager);
    return;
  }
  let managerN = myManagerNo;
  if (valideNull(myManagerNo)) {
    if (!valideNull(manager.employeeNo)) {
      managerN = manager.employeeNo;
    } else {
      managerN = "DDDDDD"; //DDCCFM  DDDDDD  DEMDDB小君
    }
  } else {
    if (
      myManagerNo == user.employeeNo &&
      myManagerNo == manager.employeeNo &&
      app.globalData.initSendData &&
      NoFrom != "login" &&
      NoFrom != "register" &&
      NoFrom != "changeView" &&
      !valideNull(myOptions.myManagerNo)
    ) {
      //点击自己分享的仅切换视角
      console.log("NoFrom !=login", NoFrom != "login");
      // user.viewType = 0
      // TODO
      Vue.prototype.$store.commit("USER_INFO", user);
      return;
    } else if (myManagerNo == user.employeeNo) {
      managerN = myManagerNo;
    } else if (
      myManagerNo != user.employeeNo &&
      myManagerNo != manager.employeeNo &&
      user.employeeNo == manager.employeeNo
    ) {
      user.viewType = 0;
      Vue.prototype.$store.commit('USER_INFO', user)
    }
    if (NoFrom == "register" && !valideNull(myOptions.myManagerNo)) {
      //通过授权获取客户经理信息 如果页面有分享的客户经理员工号以分享的为主
      managerN = myOptions.myManagerNo;
    }
  }
  if (myManagerNo == user.employeeNo && NoFrom === "login") {
    managerN = user.employeeNo;
    user.viewType = 1;
  }
  if (!valideNull(user)) {
    if (
      user.loginStatus == 1 &&
      user.employeeNo.length > 0 &&
      user.employeeNo != managerN &&
      NoFrom === "login"
    ) {
      user.viewType = 0;
    } else if (user.employeeNo == managerN) {
      user.loginStatus = 1;
    }
  }

  try {
    // managerN = 'DDMLCM' //测试 DEMDDB凌卓  DDCCFM  DDCEFE DDDDDD吴文婷  DELMM@//邓锐
    indexAPI.queryKey().then((resData) => {
      console.log("resData", resData);

      let PUBLIC_KEY = resData.noStr.substring(0, resData.noStr.length - 10);
      let encryptContext = new JSEncrypt();
      encryptContext.setPublicKey(PUBLIC_KEY);
      let encryptData = encryptContext.encryptLong(managerN);
      console.log("加密后结果", encryptData);

      indexAPI.qCusMngerCard(encryptData).then((res) => {
        // managerN 'DDDDDD'
        if (res.result) {
          let data = res.listData;
          if (res.listData.length == 0) {
            res.listData = [
              {
                branchName: "国泰君安营业部",
                userName: "小君",
                certificateNo: "",
                employeeNo: "DDDDDD",
                provinceName: "国泰君安营业部",
              },
            ];
          }
          if (res.listData.length > 0) {
            if (
              curPg != "pages/managerDetail/managerDetail" ||
              NoFrom == "changeView"
            ) {
              indexAPI.costomHideData("01" + managerN).then(function (hideRes) {
                if (hideRes.success) {
                  console.log(
                    "hideRes.selCostomHideDate",
                    hideRes.selCostomHideDate
                  );
                  let content = !valideNull(hideRes.selCostomHideDate[0])
                    ? JSON.parse(hideRes.selCostomHideDate[0].content)
                    : Vue.prototype.$store.getters.componentState_backup;
                  // let content = ""
                  // if (!valideNull(hideRes.selCostomHideDate[0])) {
                  // 	content = JSON.parse(hideRes.selCostomHideDate[0].content)
                  // 	for (let key1 in Vue.prototype.$store.getters.componentState_backup) {
                  // 		let addFlag = true
                  // 		for (let key2 in content) {
                  // 			if (key1 == key2) {
                  // 				addFlag = false
                  // 				break
                  // 			}
                  // 		}
                  // 		if (addFlag) {
                  // 			content[key1] = []
                  // 		}
                  // 	}
                  // } else {
                  // 	content = Vue.prototype.$store.getters.componentState_backup
                  // }
                  Vue.prototype.$store.commit("COMPONENT_STATE", content);

                  // uni.setStorageSync('locComponentState',content)
                  // getApp().globalData.gComponentState=content
                  // Vue.prototype.$store.commit('COMPONENT_STATE_BACKUP', content)
                  // componentState_backup
                }
              });
            }
            indexAPI.workQualify(managerN).then(function (ans) {
              let isQualify = false;
              if (ans.success) {
                if (ans.staffSeniority.length > 0) {
                  isQualify =
                    ans.staffSeniority[0].custFlag == "1" &&
                    ans.staffSeniority[0].offlag == "1";
                }
              }
              let obj = res.listData[0];
              let manager = new Manager.Manager(
                obj.userName,
                obj.postion,
                obj.telephoneNo,
                obj.mobileNo,
                obj.wechatNo,
                obj.email,
                obj.cardPicUrl,
                obj.ehrPicBase64,
                obj.provinceName,
                obj.provinceId,
                obj.branchName,
                obj.branchNo,
                obj.branchAddress,
                obj.userProfile,
                obj.employeeNo,
                obj.tgFlag,
                obj.certificateNo,
                isQualify
              );
              if (obj.employeeNo == "DDDDDD") {
                manager = new Manager.Manager(
                  "小君",
                  "",
                  "",
                  "",
                  "国泰君安微理财",
                  "",
                  obj.cardPicUrl,
                  "",
                  obj.provinceName,
                  obj.provinceId,
                  "国泰君安证券",
                  obj.branchNo,
                  "",
                  "国泰君安,中国领先的综合金融服务商和全能投资银行,全业务链证券投资公司,为您提供股票开户,期货,证券,股票,债券,公募、私募基金交易;融资融券,港股通,理财产品等一站式财富管理服务,超20年品质服务，行业大券商，为您的每一个明天打算！",
                  obj.employeeNo,
                  obj.tgFlag,
                  "",
                  true
                );
              }
              if (!valideNull(user.openId)) {
                console.log("employee_branch-----" + manager.provinceName);
                if (myManagerNo == user.employeeNo) {
                  //&& NoFrom === 'login'
                  app.sensors.setProfile({
                    user_type: user.employeeNo ? "客户经理" : "客户",
                    shop_id: "01" + manager.employeeNo,
                    shop_name: manager.userName + "的小店",
                    employee_id: manager.employeeNo,
                    employee_branch: manager.provinceName,
                    employee_branch_code: manager.provinceId, //新增分公司id
                    employee_office: manager.branchName,
                    employee_office_code: manager.branchNo, //新增分支机构id
                  });
                }
                if (!app.globalData.initSendData) {
                  console.log(
                    "initSendData",
                    new Date(),
                    app.globalData.initSendData
                  );
                  // app.sensors.registerApp({
                  // 	user_type: user.employeeNo ? '客户经理' : '客户',
                  // 	product_name: '国君小店',
                  // 	platform_type: '小程序',
                  // 	open_id: user.openId,
                  // 	union_id: user.unionId,
                  // 	mobile: user.telephone,
                  // 	cust_id: '',
                  // 	shop_id: '01' + (valideNull(manager.employeeNo) ? 'DDDDDD' : manager.employeeNo), //目前一个客户经理对应一个小店
                  // 	shop_name: (valideNull(manager.userName) ? '小君' : manager.userName) + '的小店'
                  // });
                  /*
                   **22.6.2修改神策埋点公共属性初始化，sensors.registerApp需在App实例化之前设置，但App实例化之后才能获取到客户信息
                   **故使用 sensors._.info.currentProps.xx = xx 设置公共属性
                   */
                  app.sensors._.info.currentProps.user_type = user.employeeNo
                    ? "客户经理"
                    : "客户";
                  app.sensors._.info.currentProps.product_name = "国君小店";
                  app.sensors._.info.currentProps.platform_type = "小程序";
                  app.sensors._.info.currentProps.open_id = user.openId;
                  app.sensors._.info.currentProps.union_id = user.unionId;
                  app.sensors._.info.currentProps.mobile = user.telephone;
                  app.sensors._.info.currentProps.cust_id = "";
                  (app.sensors._.info.currentProps.shop_id =
                    "01" +
                    (valideNull(manager.employeeNo)
                      ? "DDDDDD"
                      : manager.employeeNo)), //目前一个客户经理对应一个小店
                    (app.sensors._.info.currentProps.shop_name =
                      (valideNull(manager.userName)
                        ? "小君"
                        : manager.userName) + "的小店");

                  app.sensors.init();
                  app.globalData.initSendData = true;
                  Vue.prototype.$store.commit("SHOPINFO", {
                    shop_id:
                      "01" +
                      (valideNull(manager.employeeNo)
                        ? "DDDDDD"
                        : manager.employeeNo),
                  });
                }
              }
              Vue.prototype.$store.commit("MANAGER_INFO", manager);
              if (
                user.loginStatus == 1 &&
                user.employeeNo.length > 0 &&
                user.employeeNo != manager.employeeNo
              ) {
                user.viewType = 0;
                Vue.prototype.$store.commit("USER_INFO", user);
              }
            });
          } else {
            if (user.employeeNo == managerN) {
              user.loginStatus = 0;
              user.viewType = 0;
              Vue.prototype.$store.commit("USER_INFO", user);
            }
            if (!tryManageer) {
              getCardInfo("DDDDDD");
              tryManageer = 1;
            }
          }
        } else {
          uni.showToast({
            title: res.message,
            icon: "none",
          });
        }
      });
    });
  } catch (e) {
    console.log(e);
  }
};

// 客户经理的分享是本人小店
function tranShareInfo(
  sfrom = "",
  title = "国泰君安小店",
  imageUrl,
  thePath = "/pages/tabbar/index/index",
  param,
  isTimeline
) {
  const Vue = useVue();

  let manager = getManagerFromLocal();
  let user = getUserFromLocal();
  let paraStr =
    "?myManagerNo=" +
    (user.employeeNo != "" ? user.employeeNo : manager.employeeNo) +
    "&shareUID=" +
    user.unionId +
    "&fromCard=1";
  let pages = getCurrentPages();
  let curPage = pages[pages.length - 1].route;
  let myOptions = pages[pages.length - 1].options;
  let shareTitle = title;
  let timestamp = (new Date().getTime() / 1000).toFixed(0);
  let shareTimestamp = timestamp + (timestamp % 9);
  console.log(shareTimestamp);
  if (valideNull(thePath)) {
    thePath = curPage;
  }
  let MyName = "";
  if (user.employeeNo != "") {
    MyName = Vue.prototype.$store.getters.userShareName;
    console.log("名字", MyName);
  } else {
    MyName = user.nickName;
  }
  switch (curPage) {
    case "pages/tabbar/index/index":
      shareTitle = `${MyName || "小君"}给您分享了国泰君安小店,快来看看吧.`;
      break;
    case "pages/managerDetail/managerDetail":
      if (user.viewType == 0) {
        shareTitle = `${user.nickName || "小君"}向你推荐了国泰君安证券的${
          manager.userName
        },快来看看吧.`;
      } else {
        shareTitle = `你好,我是国泰君安的${manager.userName},这是我的名片,很高兴认识你.`;
      }
      break;
    case "pages/activity/activity":
      if (myOptions.pageType == "prdct") {
        paraStr += "&tglj_timeStamp=" + shareTimestamp;
      } else {
      }
      shareTitle = `${MyName || "小君"}给您分享了${title},快来看看吧.`;
      break;
    case "pages/level2/buySuccess":
      shareTitle = title;
      break;
    default:
      // shareTitle = `${MyName||'小君'}给您分享了${title},快来看看吧.`
      shareTitle = `${title}`;
      break;
  }
  for (let itm in param) {
    if (
      itm != "myManagerNo" &&
      itm != "fromCard" &&
      itm != "shareUID" &&
      itm != "scene"
    ) {
      paraStr += "&" + itm + "=" + param[itm];
    }
  }
  let timeLineQuery = {
    ...param,
    myManagerNo: user.employeeNo != "" ? user.employeeNo : manager.employeeNo,
    shareUID: user.unionId,
    fromCard: 1,
  };
  console.log(thePath, paraStr);
  console.log(
    "分享人",
    isTimeline == "isShareTimeline"
      ? {
          title: shareTitle,
          imageUrl: imageUrl,
          query: paraStr.slice(1),
        }
      : {
          title: shareTitle,
          path: thePath + paraStr,
          imageUrl: imageUrl,
        }
  );
  return isTimeline == "isShareTimeline"
    ? {
        title: shareTitle,
        imageUrl: imageUrl,
        query: paraStr.slice(1),
      }
    : {
        title: shareTitle,
        path: thePath + paraStr,
        imageUrl: imageUrl,
      };
}

function commonPageLeave(startTime) {
  const Vue = useVue();

  console.log("commonPageLeave  onHide onHide");
  if (valideNull(startTime)) startTime = new Date();
  let duration = (new Date().getTime() - new Date(startTime).getTime()) / 1000;
  let pageInfo = getSendTitle();
  let manager = getManagerFromLocal() || {};
  let mergeAttr = {};
  let shopId =
    "01" + (valideNull(manager.employeeNo) ? "DDDDDD" : manager.employeeNo);
  if (pageInfo.pageType == "首页_产品") {
    mergeAttr["sessionid"] = getSessionId();
  }
  Vue.prototype.$sensorEvt("shopmp_material_detail_view", {
    material_type: pageInfo.pageType,
    material_id: "",
    material_name: pageInfo.pageName,
    material_first_cate: pageInfo.firstCate,
    material_second_cate: pageInfo.secondCate,
    is_hot_material: false,
    material_author: "",
    coverPicUrl: "",
    describe: "",
    ...mergeAttr,
    duration: duration,
    shop_id: shopId, //目前一个客户经理对应一个小店
    shop_name:
      (valideNull(manager.userName) ? "小君" : manager.userName) + "的小店",
  });
}

function saveObjToLocal(key, obj) {
  let ss = JSON.stringify(obj);
  uni.setStorageSync(key, ss);
}

function saveUserInfo(userInfo) {
  saveObjToLocal("GLOBAL.USER", userInfo);
}

var BASE64_MAPPING = [
  "A",
  "B",
  "C",
  "D",
  "E",
  "F",
  "G",
  "H",
  "I",
  "J",
  "K",
  "L",
  "M",
  "N",
  "O",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "U",
  "V",
  "W",
  "X",
  "Y",
  "Z",
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "u",
  "v",
  "w",
  "x",
  "y",
  "z",
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "+",
  "/",
];

/**
 *ascii convert to binary
 */
var _toBinary = function (ascii) {
  var binary = new Array();
  while (ascii > 0) {
    var b = ascii % 2;
    ascii = Math.floor(ascii / 2);
    binary.push(b);
  }
  /*
    var len = binary.length;
    if(6-len > 0){
        for(var i = 6-len ; i > 0 ; --i){
            binary.push(0);
        }
    }*/
  binary.reverse();
  return binary;
};

/** 解决js乘法误差 */
let accRes = function (arg1, arg2) {
  let m = 0;
  let s1 = arg1.toString();
  let s2 = arg2.toString();
  try {
    m += s1.split(".")[1].length;
  } catch (e) {}
  try {
    m += s2.split(".")[1].length;
  } catch (e) {}
  return (
    (Number(s1.replace(".", "")) * Number(s2.replace(".", ""))) /
    Math.pow(10, m)
  );
};

/**
 *binary convert to decimal
 */
var _toDecimal = function (binary) {
  var dec = 0;
  var p = 0;
  for (var i = binary.length - 1; i >= 0; --i) {
    var b = binary[i];
    if (b == 1) {
      dec += Math.pow(2, p);
    }
    ++p;
  }
  return dec;
};

/**
 *unicode convert to utf-8
 */
var _toUTF8Binary = function (c, binaryArray) {
  var mustLen = 8 - (c + 1) + (c - 1) * 6;
  var fatLen = binaryArray.length;
  var diff = mustLen - fatLen;
  while (--diff >= 0) {
    binaryArray.unshift(0);
  }
  var binary = [];
  var _c = c;
  while (--_c >= 0) {
    binary.push(1);
  }
  binary.push(0);
  var i = 0,
    len = 8 - (c + 1);
  for (; i < len; ++i) {
    binary.push(binaryArray[i]);
  }

  for (var j = 0; j < c - 1; ++j) {
    binary.push(1);
    binary.push(0);
    var sum = 6;
    while (--sum >= 0) {
      binary.push(binaryArray[i++]);
    }
  }
  return binary;
};

var __BASE64 = {
  /**
   *BASE64 Encode
   */
  encoder: function (str) {
    var base64_Index = [];
    var binaryArray = [];
    for (var i = 0, len = str.length; i < len; ++i) {
      var unicode = str.charCodeAt(i);
      var _tmpBinary = _toBinary(unicode);
      if (unicode < 0x80) {
        var _tmpdiff = 8 - _tmpBinary.length;
        while (--_tmpdiff >= 0) {
          _tmpBinary.unshift(0);
        }
        binaryArray = binaryArray.concat(_tmpBinary);
      } else if (unicode >= 0x80 && unicode <= 0x7ff) {
        binaryArray = binaryArray.concat(_toUTF8Binary(2, _tmpBinary));
      } else if (unicode >= 0x800 && unicode <= 0xffff) {
        //UTF-8 3byte
        binaryArray = binaryArray.concat(_toUTF8Binary(3, _tmpBinary));
      } else if (unicode >= 0x10000 && unicode <= 0x1fffff) {
        //UTF-8 4byte
        binaryArray = binaryArray.concat(_toUTF8Binary(4, _tmpBinary));
      } else if (unicode >= 0x200000 && unicode <= 0x3ffffff) {
        //UTF-8 5byte
        binaryArray = binaryArray.concat(_toUTF8Binary(5, _tmpBinary));
      } else if (unicode >= 4000000 && unicode <= 0x7fffffff) {
        //UTF-8 6byte
        binaryArray = binaryArray.concat(_toUTF8Binary(6, _tmpBinary));
      }
    }

    var extra_Zero_Count = 0;
    for (var i = 0, len = binaryArray.length; i < len; i += 6) {
      var diff = i + 6 - len;
      if (diff == 2) {
        extra_Zero_Count = 2;
      } else if (diff == 4) {
        extra_Zero_Count = 4;
      }
      //if(extra_Zero_Count > 0){
      //	len += extra_Zero_Count+1;
      //}
      var _tmpExtra_Zero_Count = extra_Zero_Count;
      while (--_tmpExtra_Zero_Count >= 0) {
        binaryArray.push(0);
      }
      base64_Index.push(_toDecimal(binaryArray.slice(i, i + 6)));
    }

    var base64 = "";
    for (var i = 0, len = base64_Index.length; i < len; ++i) {
      base64 += BASE64_MAPPING[base64_Index[i]];
    }

    for (var i = 0, len = extra_Zero_Count / 2; i < len; ++i) {
      base64 += "=";
    }
    return base64;
  },
  /**
   *BASE64  Decode for UTF-8
   */
  decoder: function (_base64Str) {
    var _len = _base64Str.length;
    var extra_Zero_Count = 0;
    /**
     *计算在进行BASE64编码的时候，补了几个0
     */
    if (_base64Str.charAt(_len - 1) == "=") {
      //alert(_base64Str.charAt(_len-1));
      //alert(_base64Str.charAt(_len-2));
      if (_base64Str.charAt(_len - 2) == "=") {
        //两个等号说明补了4个0
        extra_Zero_Count = 4;
        _base64Str = _base64Str.substring(0, _len - 2);
      } else {
        //一个等号说明补了2个0
        extra_Zero_Count = 2;
        _base64Str = _base64Str.substring(0, _len - 1);
      }
    }

    var binaryArray = [];
    for (var i = 0, len = _base64Str.length; i < len; ++i) {
      var c = _base64Str.charAt(i);
      for (var j = 0, size = BASE64_MAPPING.length; j < size; ++j) {
        if (c == BASE64_MAPPING[j]) {
          var _tmp = _toBinary(j);
          /*不足6位的补0*/
          var _tmpLen = _tmp.length;
          if (6 - _tmpLen > 0) {
            for (var k = 6 - _tmpLen; k > 0; --k) {
              _tmp.unshift(0);
            }
          }
          binaryArray = binaryArray.concat(_tmp);
          break;
        }
      }
    }

    if (extra_Zero_Count > 0) {
      binaryArray = binaryArray.slice(0, binaryArray.length - extra_Zero_Count);
    }

    var unicode = [];
    var unicodeBinary = [];
    for (var i = 0, len = binaryArray.length; i < len; ) {
      if (binaryArray[i] == 0) {
        unicode = unicode.concat(_toDecimal(binaryArray.slice(i, i + 8)));
        i += 8;
      } else {
        var sum = 0;
        while (i < len) {
          if (binaryArray[i] == 1) {
            ++sum;
          } else {
            break;
          }
          ++i;
        }
        unicodeBinary = unicodeBinary.concat(
          binaryArray.slice(i + 1, i + 8 - sum)
        );
        i += 8 - sum;
        while (sum > 1) {
          unicodeBinary = unicodeBinary.concat(binaryArray.slice(i + 2, i + 8));
          i += 8;
          --sum;
        }
        unicode = unicode.concat(_toDecimal(unicodeBinary));
        unicodeBinary = [];
      }
    }
    return unicode;
  },
};

export default {
  __BASE64,
  ...base,
  User: new User.User(),
  valideNull,
  valideNotNull,
  addZero,
  getManagerFromLocal,
  getCardInfo,
  timeHM,
  getSendTitle,
  getUrlParam,
  getSessionId,
  commonPageLeave,
  accRes,
  setNavInfo,
  saveObjToLocal,
  getObjFromLocal,
  getUserFromLocal,
  saveManagerInfo,
  saveUserInfo,
};
