// EventBus.js
import { ref } from 'vue';

const eventBus = ref({});

export function emitEvent(eventName, payload) {
    const handler = eventBus.value[eventName];
    if (handler) {
        handler(payload);
    }
}

export function onEvent(eventName, handler) {
    eventBus.value[eventName] = handler;
}