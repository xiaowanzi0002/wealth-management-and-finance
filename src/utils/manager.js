import utils from './utils.js'

// import md5 from './md5.min.js'
function Manager(userName, postion, telephoneNo, mobileNo, wechatNo, email, cardPicUrl, ehrPicBase64, provinceName, provinceId, branchName, branchNo, branchAddress, userProfile, employeeNo, tgFlag, certificateNo, isQualify) {
    this.userName = userName
    this.postion = postion// 职位
    this.telephoneNo = telephoneNo //座机号码
    this.mobileNo = mobileNo
    this.wechatNo = wechatNo
    this.email = email
    this.cardPicUrl = (function () {
        let imgUrls = cardPicUrl
        if (utils.valideNull(cardPicUrl)) {
            if (!utils.valideNull(ehrPicBase64)) {
                imgUrls = 'data:image/png;base64,' + ehrPicBase64
            } else {
                imgUrls = 'https://dl.app.gtja.com/public/wlc/xd/img/gtjaHeadImg.png'
            }
        }
        return imgUrls
    })()
    //cardPicUrl|| 'https://dl.app.gtja.com/public/wlc/xd/img/gtjaHeadImg.png'// 头像
    // this.ehrPicBase64=ehrPicBase64
    this.provinceName = provinceName
    this.provinceId = provinceId
    this.branchName = branchName
    this.branchNo = branchNo
    this.branchAddress = branchAddress // 营业部地址
    this.userProfile = userProfile
    this.employeeNo = employeeNo // md5(employeeNo) // 客户经理工号
    this.shopId = '01' + employeeNo // md5(employeeNo) // 客户经理工号
    this.tgFlag = tgFlag // 客户经理工号 未加密
    this.certificateNo = certificateNo // 从业资格证书
    this.cardPicIsBase64 = (utils.valideNull(cardPicUrl) && (!utils.valideNull(ehrPicBase64)))
    this.isQualify = isQualify
}

export default {
    Manager
}
