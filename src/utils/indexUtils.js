import {getRemainAmount} from '@/api/funds/prodDetail.js'
// import Vue from 'vue'
// import {regexReplaceUrlParam, initClipboardListen} from '@/assets/js/util.js'
import baseUtils from './base.js'
import {pageJump} from '@/utils/fund/fundUtil.js'

var global = {}
global.MGC = baseUtils
const tgljInfo = '' //global.MGC.queryUriSingleParam('tglj_info')
const isyyzshare = '' // global.MGC.queryUriSingleParam('isyyzshare')
/** 产品、专区、KV营业部、券码匹配逻辑。true匹配 */
let isMatch = function (userOrgId, orgId, userTrusteeBank, trusteeBank) {
    // 1.若接口返回的营业部orgId不空，用户的营业部userOrgId包含在orgId才可见，否则不可见
    // 2.若接口返回的券码trusteeBank不空，用户的券码userTrusteeBank存在trusteeBank才可见，否则不可见
    if (global.MGC.isNotBlank(orgId)) {
        let splitOrgId = orgId.split(',')
        let flag = false
        for (let i = 0; i < splitOrgId.length; i++) {
            if (userOrgId === splitOrgId[i]) {
                flag = true
                break
            }
        }
        if (!flag) {
            return false
        }
    }
    if (global.MGC.isNotBlank(trusteeBank)) {
        userTrusteeBank = global.MGC.isBlank(userTrusteeBank) ? '' : userTrusteeBank
        let splitUserTrusteeBank = userTrusteeBank.split(',')
        for (let i = 0; i < splitUserTrusteeBank.length; i++) {
            if (trusteeBank === splitUserTrusteeBank[i]) {
                return true
            }
        }
        return false
    }
    return true
}
/** itemIsDisplay：根据营业部、活动代码判断产品、专区、KV是否展示。false不展示 */
// store中isLogin为true表示已登录, user当前用户的登录信息
let itemIsDisplay = function (data, itemType, isLogin, user) {
    // 缓存中的用户信息
    // let userCache = global.MGC.wsCache.get('user')
    // 对于产品类型，若TICKET_NUM为1才需根据活动代码定向展示
    // 对于专区或KV,TRUSTEE_BANK为空不需根据活动代码定向展示，不为空只有匹配活动代码后才可以看到
    let trusteeBankItem = data.TRUSTEE_BANK
    if (itemType === 'product' || itemType === 'shelfBrowseWay13') {
        if (data.TICKET_NUM !== '1') {
            // 对于产品类型不需根据活动代码定向展示时，把trusteeBankItem赋值为空，表示后续判断不需匹配活动代码
            trusteeBankItem = ''
        }
    }
    if (global.MGC.isBlank(data.ORG_ID) && global.MGC.isBlank(data.TRUSTEE_BANK)) {
        return true
    }
    // if (isLogin) {
    //   // 若产品、专区、KV的营业部、活动代码为空直接展示
    //   if (global.MGC.isBlank(data.ORG_ID) && global.MGC.isBlank(data.TRUSTEE_BANK)) {
    //     return true
    //   }
    //   // 虽然是登录状态，但是由于登陆状态保持的问题，可能出现登录状态失常拿不到user信息，此时用缓存中的用户信息
    //   if (global.MGC.isBlank(user) || (global.MGC.isBlank(user.orgId) && global.MGC.isBlank(user.trusteeBank))) {
    //     let orgIdCache = global.MGC.isBlank(userCache) ? '' : userCache.orgId
    //     let trusteeBankCache = global.MGC.isBlank(userCache) ? '' : userCache.trusteeBank
    //     return isMatch(orgIdCache, data.ORG_ID, trusteeBankCache, trusteeBankItem)
    //   } else {
    //     // 根据用户登录的营业部、活动代码去判断是否展示
    //     return isMatch(user.orgId, data.ORG_ID, user.trusteeBank, trusteeBankItem)
    //   }
    // } else {
    //   // 未登录时，查看缓存中（上一次登录）的用户营业部、券码信息是否为空。
    //   // 不为空根据缓存中的进行匹配。为空，根据未登录是否可见，若不可见直接过滤掉
    //   if (global.MGC.isBlank(userCache) || (global.MGC.isBlank(userCache.orgId) && global.MGC.isBlank(userCache.trusteeBank))) {
    //     if (itemType === 'shelfBrowseWay13') {
    //       // 货架13还需要额外判断营业部
    //       return (data.NOLOGIN_IS_DISPLAY !== '2' && data.ORG_ID === '')
    //     } else {
    //       return data.NOLOGIN_IS_DISPLAY !== '2'
    //     }
    //   } else {
    //     return isMatch(userCache.orgId, data.ORG_ID, userCache.trusteeBank, trusteeBankItem)
    //   }
    // }
}

// /** 处理产品额度展示 */
let getRemainAmountUt = (prodList, prodFeatureList) => {
    if ((prodList.LIMIT_TYPE === '1' || prodList.LIMIT_TYPE === '2' || prodList.LIMIT_TYPE === '5') &&
        (prodList.PROD_TYPE === '3' || prodList.PROD_TYPE === '4' || (prodList.PROD_TYPE === '6' && prodList.PROD_TYPE_CHILD !== '63'))) {
        let remainAmount = []
        getRemainAmount(prodList.PROD_CODE, prodList.PROD_TYPE, prodList.PROD_TYPE_CHILD).then(result => {
            let data = result.data
            if (data !== undefined && Object.keys(data).length !== 0) {
                // 需要展示的是文字描述，则以20%为界，剩余额度小于等于20%则为额度紧张，否则为额度充裕
                if (prodList.LIMIT_TYPE === '1' && data[0].used_quota !== '') {
                    // 计算百分比
                    if (data[0].enable_quota !== '') {
                        var temp = parseFloat(data[0].enable_quota) / (parseFloat(data[0].enable_quota) + parseFloat(data[0].used_quota))
                    }
                    if (parseFloat(temp) > 0.2) {
                        prodFeatureList.push('额度充裕')
                    } else {
                        prodFeatureList.push('额度紧张')
                    }
                    // 需要展示的是准确额度，则直接放入标签
                } else if (prodList.LIMIT_TYPE === '2') {
                    // 转化为万元
                    if (data[0].enable_quota !== '') {
                        var temp2 = Math.round(parseFloat(data[0].enable_quota))
                    }
                    // 额度小于10000，四舍五入取整后添加
                    if (temp2 < 10000) {
                        prodFeatureList.push('剩余' + temp2 + '元')
                        // 额度大于10000展示万元单位，并且显示
                    } else {
                        prodFeatureList.push('剩余' + Math.round(temp2 / 10000) + '万元')
                    }
                }
                /* 产品剩余额度展示 */
                if (prodList.LIMIT_TYPE === '5') {
                    if (data[0].enable_quota !== '' && data[0].used_quota !== '') {
                        remainAmount.push('共' + Math.floor((parseFloat(data[0].enable_quota) +
                                parseFloat(data[0].used_quota)) / 10000) + '万元，剩余' +
                            Math.floor(parseFloat(data[0].enable_quota) / 10000) + '万元')
                        remainAmount.push('width:' + parseFloat(data[0].used_quota) * 100 / (parseFloat(data[0].enable_quota) +
                            parseFloat(data[0].used_quota)) + '%')
                    } else if (data[0].enable_quota === '' && data[0].used_quota !== '') {
                        remainAmount.push('剩余' + Math.floor(parseFloat(data[0].enable_quota) / 10000) + '万元')
                        remainAmount.push('width:0%')
                    }
                }
            }
        })
        prodList['remainAmount'] = remainAmount
        // Vue.set(prodList, 'remainAmount', remainAmount)
    }
    prodList['btnList'] = prodFeatureList
    // Vue.set(prodList, 'btnList', prodFeatureList)
}

/** 针对首页可以通过该方法获得埋点数据 ：客户代码custCode 用户代码usercode 注册手机号phoneNumber 营业部名称branchName */
/** 其他页面获得埋点参数方法：
 *  客户代码custCode      global.MGC.wsCache.get('custCode')
 *  用户代码usercode      global.MGC.wsCache.get('usercode')
 *  手机号phoneNumber     global.MGC.wsCache.get('phoneNumber')
 *  营业部orgId           global.MGC.wsCache.get('orgId')
 *  营业部名称branchName  global.MGC.wsCache.get('branchName')
 */
let buryPointData = function () {
    // let usercode = global.MGC.queryUriSingleParam('usercode')
    // let phoneNumber = global.MGC.queryUriSingleParam('phoneNumber')
    // let userCache = global.MGC.wsCache.get('user')
    // let custCode = ''
    // let branchName = ''
    // if (global.MGC.isNotBlank(userCache)) {
    //   custCode = global.MGC.isBlank(userCache.custCode) ? '' : userCache.custCode
    //   branchName = global.MGC.isBlank(userCache.branchName) ? '' : userCache.branchName
    // }
    // let data = {custCode, branchName, phoneNumber, usercode}
    return data
}
/** 产品货架跳转产品详情页 */
let toProdDetail = function (prodList, isLogin) {

    pageJump(prodList.DETAIL_URL, prodList)
    //let url = prodList.DETAIL_URL
    // let qij = global.MGC.queryUriSingleParam('qualifInvestorJump')
    // if (prodList.PROD_TYPE === '1' || prodList.PROD_TYPE === '4') {
    //   url = url + '&needShare=1'
    // } else {
    //   // @TODO hasTK仅用于OTC资管未改版详情页弹出合格投资者承诺函弹窗标识，表示详情页无需在弹起
    //   if (global.MGC.wsCache.get('qualifInvestorJumpConfirmed') === '1' || (global.MGC.isNotBlank(qij) && qij !== '0')) {
    //     url = url + '&hasTK=1'
    //   }
    // }
    // if (global.MGC.isNotBlank(tgljInfo)) {
    //   url = regexReplaceUrlParam(url, 'tglj_info', tgljInfo)
    // }
    // if (prodList.NO_OPEN_KEY === '2' && prodList.FORCE_SHOW_KEY === '2' && !isLogin) {
    //   if (isyyzshare === '1' && global.MGC.getAPPVer() === '') {
    //     initClipboardListen('#go-load-clipboard', url)
    //     window.location.href = global.MGC.getOutUri(process.env.NODE_ENV, 'openOrDownApp') + encodeURIComponent(url)
    //   } else {
    //     global.MGC.checkAndDoLogin(process.env.NODE_ENV, process.env.API_HOST, '1', url)
    //   }
    // } else {
    //   if (isyyzshare === '1' && global.MGC.getAPPVer() === '') {
    //     url = url + '&isyyzshare=1'
    //     initClipboardListen('#go-load-clipboard', url)
    //   }
    //   window.location.href = url
    // }
}
/** 货架更多地址跳转 */
let toMoreUrl = function (secColumn) {
    let url
    // if (secColumn.MORE_TYPE === '2') {
    //   url = secColumn.ITEM_URL
    // } else if (secColumn.MORE_TYPE === '3') {
    //   let cacheFlag = global.MGC.queryUriSingleParam('cacheFlag')
    //   url = global.MGC.getHtmlUrl(process.env.LOCAL_CONTEXT, '/area') + '?itemCode=' + secColumn.ITEM_URL + '&titleLightColor=1&cacheFlag=' + cacheFlag
    // }
    // if (secColumn.MORE_TYPE === '2' || secColumn.MORE_TYPE === '3') {
    //   if (global.MGC.isNotBlank(tgljInfo)) {
    //     url = regexReplaceUrlParam(url, 'tglj_info', tgljInfo)
    //   }
    //   if (isyyzshare === '1' && global.MGC.getAPPVer() === '') {
    //     url = regexReplaceUrlParam(url, 'isyyzshare', '1')
    //     initClipboardListen('#go-load-clipboard', url)
    //   }
    //   window.location.href = url
    // }
}
/** 非产品货架直接url跳转 透传tglj_info及分享外推页地址处理 */
let jumpDetailUrl = function (url) {
    // if (global.MGC.isNotBlank(url)){
    //   if (global.MGC.isNotBlank(tgljInfo)) {
    //     url = regexReplaceUrlParam(url, 'tglj_info', tgljInfo)
    //   }
    //   if (isyyzshare === '1' && global.MGC.getAPPVer() === '') {
    //     url = regexReplaceUrlParam(url, 'isyyzshare', '1')
    //   }
    //   window.location.href = url
    // }
}
/** 非增强净值型基金和资管产品收益率为正,且收益率标签不包含“净值”或“其他”时展示‘+’号 */
let handleRate = function (prodList) {
    if (((prodList.PROD_TYPE === '1' && prodList.INVEST_TYPE !== '3' && prodList.INVEST_TYPE !== '200012') ||
            (prodList.PROD_TYPE === '2' && prodList.NET_VALUE_TYPE === '3') || (prodList.PROD_TYPE === '9' && prodList.INCOME_TYPE_OTC === '3')) &&
        prodList.RATE_LABEL.indexOf('净值') < 0 && prodList.RATE_LABEL.indexOf('其他') < 0) {
        if (parseFloat(prodList.RATE) > 0) {
            return '+' + prodList.RATE
        } else {
            return prodList.RATE
        }
    } else {
        return prodList.RATE
    }
}
export {itemIsDisplay, getRemainAmountUt, buryPointData, toProdDetail, toMoreUrl, jumpDetailUrl, handleRate}
