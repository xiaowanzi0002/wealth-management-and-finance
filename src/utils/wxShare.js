export default {
  data() {
    return {
      share: {
        // 转发的标题 （默认标题）
        title: "",
        // 默认是当前页面，必须是以‘/’开头的完整路径
        path: "",
        //自定义图片路径，可以是本地文件路径、代码包文件路径或者网络图片路径，
        //支持PNG及JPG，不传入 imageUrl 则使用默认截图。显示图片长宽比是 5:4
        imageUrl: "",
        first_tab: "",
    
      },
    };
  },

  // 发送给朋友
  onShareAppMessage(res) {
    // 来自页面内的按钮的转发
    if (res.from == "button") {
      console.log("按钮转发--配置");
    }
    let sfrom = "右上角按钮";
    if (res.target == "undefined") {
      sfrom = res.target.dataset.sfrom ? "页面内按钮" : "";
    }
    const app = getApp();
    app.sensors.para.autoTrack.pageShare = {
      share_method: sfrom,
      first_tab: this.share.first_tab,
      page_name: this.share.title,
    };
    // 获取加载的页面
    let pages = getCurrentPages(),
      // 获取当前页面的对象
      view = pages[pages.length - 1];
    this.share.path = `/${view.route}`;
    //转发参数
    return this.share;
  },

  //分享到朋友圈
  onShareTimeline(res) {
    // 获取加载的页面
    let pages = getCurrentPages(),
      // 获取当前页面的对象
      view = pages[pages.length - 1];
    console.log("当前页面的对象", view);
    this.share.path = `/${view.route}`;
    //转发参数
    return this.share;
  },
};
