const base = {
	queryUriSingleParam: function (param) {
		let pages = getCurrentPages();
		let curPage = pages[pages.length - 1].route;
		const systemInfo = uni.getSystemInfoSync();
		let myOptions = pages[pages.length - 1].options;
		return myOptions[param] ? myOptions[param] : null;
	},
	isBlank: function (obj) {
		return (!obj || obj.toString().trim() === "");
	},
	isNotBlank: function (obj) {
		return !this.isBlank(obj);
	}
};

export default base;