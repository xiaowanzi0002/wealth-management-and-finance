import Http from './shopRequest' // 用flyio构建的请求 只支持H5、 微信小程序、 支付宝小程序

const shopURL = import.meta.env.VITE_BASE_SHOP_HOST

export default class Request {

    // 实时快讯
    static getRealTimeNews(pageNo, pageSize, orderUpdown) {
        return Http.Post(shopURL + '/news/getRealTimeNews.json', {
            pageNo: pageNo,
            pageSize: pageSize,
            orderUpdown: orderUpdown
        })
    }

    static queryFollowAct(telPhone) {
        return Http.Get(shopURL + 'jh/queryFollowAct.json', {
            telPhone: telPhone
        })
    }

    // static createFollowAct(status, telPhone, appId, generateNo, openId, roomId) {
    //     return Http.Get(shopURL + 'jh/createFollowAct.json', {
    //         status: status,//关注的status传00 取消关注传02
    //         telPhone: telPhone,
    //         appId: env.TESTKXDJ_SUBSCRIB ? 'wxba11108b7e8f36e3' : appId,
    //         generateNo: env.TESTKXDJ_SUBSCRIB ? 'gh_4c3bc41d3562' : generateNo,
    //         openId: openId,
    //         roomId: roomId//'3253'// roomId  //3253
    //     })
    // }
}
