import Http from './shopRequest' // 用flyio构建的请求 只支持H5、 微信小程序、 支付宝小程序
// import Http from '@/apiUtils/uniRequestUtils.js'  // 用 uni.request 构建的请求
const shopURL = import.meta.env.VITE_BASE_SHOP_HOST
export default class Request {
	/**
	   * 获取栏目-T1000001
	   * @param itemCodeSuper 上级栏目代码
	   * @param yyzver        易阳指版本号
	   * @param itemPurpose   栏目用途
	   * @param channelCode   渠道代码
	   */
	 static getColumns (itemCodeSuper, yyzver, itemPurpose, channelCode) {
	    return Http.Get(shopURL + '/eplus/api/index/get/columns.json', { itemCodeSuper: itemCodeSuper, yyzver: yyzver, itemPurpose: itemPurpose, cacheFlag: cacheFlag, channelCode: channelCode, itemCode: secItemCode })
	  }
	static getRadarInfo(t, e) {
		return Http.Get('https://i.gtja.com/mall' + "/eplus/api/prod/get/radarInfo.json", {
			prodCode: '002883',
			investType: 3
		})
	}
	static getNewsDetail(id, stockCode, market) {
		return Http.Post(shopURL + 'api/quotes/newsDetail.json', {
			id: id,
			stockCode: stockCode,
			market: market
		})
	}


}
