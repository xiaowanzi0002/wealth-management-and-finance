import Fly from "flyio/dist/npm/wx"

import qs from 'qs'

const fly = new Fly();

//定义公共headers
fly.config.headers = {
    "content-Type": "application/x-www-form-urlencoded;charset=UTF-8" //php的post传输请求头一定要这个 不然报错 接收不到值
}

//设置超时
fly.config.timeout = 50000

//设置请求基地址
fly.config.baseURL = "";
fly.config.withCredentials = true;

let loadingTimer = ""; //请求loading的定时器
fly.interceptors.request.use(request => {
    //给所有请求添加自定义header 这里需要后端支持
    // request.headers["X-Tag"]="flyio";
    // loadingTimer = setTimeout(() => {
    //     uni.showLoading({
    //         title: "加载中",
    //         mask: true
    //     });
    // }, 2000);
    return request;
});

// 添加响应拦截器，响应拦截器会在then/catch处理之前执行(获取数据后)
fly.interceptors.response.use(
    (response, promise) => {
        // clearTimeout(loadingTimer); // 请求结束就清掉定时器
        // Config.loading.loadingHide(); //隐藏loading
        response = typeof response === "string" ? JSON.parse(response) : response;
        if (response.data['code'] === -1) {
            // #ifdef  H5
            uni.showToast({
                title: response.data.msg,
                icon: "none",
                mask: true
            });
            setTimeout(() => {
                location.href = ""; //跳转到登录页面 测试
            }, 1000);
            // #endif
        }
        return promise.resolve(response.data); //这里直接输出了结果
    },
    (err, promise) => {
        clearTimeout(loadingTimer); // 请求结束就清掉定时器
        uni.hideLoading(); //隐藏loading
        //显示异常信息
        uni.showToast({
            title: (err && err.message) ||
                (err && err.reason) ||
                "服务器升级中，请稍后重试。",
            icon: "none",
            mask: true
        });
        return promise.reject(err);
    }
);

//
// const fly = new Fly();
//
// //定义公共headers
// fly.config.headers = {
//     "content-Type": "application/x-www-form-urlencoded;charset=UTF-8" //php的post传输请求头一定要这个 不然报错 接收不到值
// }
// //设置超时
// fly.config.timeout = 50000
// //设置请求基地址
// fly.config.baseURL = ''
// fly.config.withCredentials = true;

export default class Ajax {
    static request = (url, params = {}, loading = true, type = "post") => {
        return new Promise((resolve, reject) => {
            fly
                .request(url, qs.stringify(params), {
                    method: type
                })
                .then(response => {
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    };

    static Post = (url, params = {}) => {
        return new Promise((resolve, reject) => {
            fly
                .post(url, qs.stringify(params))
                .then(response => {
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    };

    static Get = (url, params = {}) => {
        return new Promise((resolve, reject) => {
            fly
                .get({url: url, params: params})
                .then(response => {
                    resolve(response);
                })
                .catch(error => {
                    reject(error);
                });
        });
    };

    // static Post(url, params = {}) {
    //     return new Promise((resolve, reject) => {
    //         uni.request({
    //             url: url,
    //             method: 'POST',
    //             data: qs.stringify(params),
    //             header: {
    //                 "content-Type": "application/x-www-form-urlencoded;charset=UTF-8" //php的post传输请求头一定要这个 不然报错 接收不到值
    //             },
    //             success(response) {
    //                 resolve(response);
    //             },
    //             fail(error) {
    //                 reject(error);
    //             }
    //         })
    //
    //         // fly
    //         //     .post(url, qs.stringify(params))
    //         //     .then(response => {
    //         //         resolve(response);
    //         //     })
    //         //     .catch(error => {
    //         //         reject(error);
    //         //     });
    //     });
    // }

    // static Get = (url, params = {}) => {
    //     return new Promise((resolve, reject) => {
    //         uni.request({
    //             url: url,
    //             method: 'GET',
    //             data: params,
    //             header: {
    //                 "content-Type": "application/x-www-form-urlencoded;charset=UTF-8" //php的post传输请求头一定要这个 不然报错 接收不到值
    //             },
    //             success(response) {
    //                 resolve(response);
    //             },
    //             fail(error) {
    //                 reject(error);
    //             }
    //         })
    //     });
    // };
}

