import Http from './shopRequest' // 用flyio构建的请求 只支持H5、 微信小程序、 支付宝小程序

const shopURL = import.meta.env.VITE_BASE_SHOP_HOST

export default class Request {
    static qCusMngerCard(employeeNo) {
        return Http.Post(shopURL + '/queryCustomerManagerCard.json', {
            employeeNo: employeeNo
        })
    }

    static queryKey() {
        return Http.Post(shopURL + '/getOneStr.json')
    }

    static workQualify(employeeNo) {
        return Http.Post(shopURL + '/news/getStaffSeniority.json', {
            workId: employeeNo
        })
    }

    // shopId: 		小店id
    // content:		保存内容json字符串 ，查询时可不传
    // flag：		操作标志，00：添加，查询可不传
    static costomHideData(shopId, content, flag) {
        return Http.Post(shopURL + '/news/matertialData/costomHideData.json', {
            shopId: shopId,
            content: JSON.stringify(content),
            flag: flag
        })
    }

    /*
      返回USER_LEVEL
    1 游客 下载君弘app包含手机入库未注册
    2 注册用户 下载君弘app且手机注册激活
    3 基础客户 资产<50w
    4 富裕客户 资产[50w,500w]
    5 高净值客户 资产>500w
     */
    static qStarGrade(telePhone) {
        return Http.Post(shopURL+ 'news/payment/queryStarGrade.json', {
            telePhone: telePhone
        })
    }
}
