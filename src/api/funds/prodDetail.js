import Http from '../shopRequest' // 用flyio构建的请求 只支持H5、 微信小程序、 支付宝小程序
// import Http from '@/apiUtils/uniRequestUtils.js'  // 用 uni.request 构建的请求
const hostMall = import.meta.env.VITE_BASE_HOST_MALL
const hostMall3 = import.meta.env.VITE_BASE_HOST_MALL3// 商城产品详情页
const hostQuotes = import.meta.env.VITE_BASE_HOST_QUOTES// 商城产品详情页

export function getProdNotice(prodCode, noticeType, pageSize, pageNum) {
    return Http.Get(hostMall + '/eplus/api/prod/get/queryProdNotice.json', {
        prodCode, noticeType, pageSize, pageNum
    })
}

/** 获取产品费率 */
export function getFundRate(prodCode, prodId, refSta) {
    return Http.Get(hostMall + '/eplus/api/prod/get/fundRate.json', {
        prodCode: prodCode, prodId: prodId, refSta: refSta
    })
}

// 获取基金重仓股票列表
export function getHoldStock(prodId) {
    return Http.Get(hostMall3 + '/eplus/api/prod/get/FundHoldStockInfo.json', {
        prodId
    })
}

/**
 * @param {Object} stkcodeList 获取日涨跌幅  stkcodeList:["SK112413","SK136344","SK102000155","SK155439","SK136493"]
 */
export function getStockBatchZG(stkcodeList) {
    return Http.Post(hostQuotes + '/quotes/quote/stockBatchZG.json', {stkcodeList: JSON.stringify(stkcodeList)})
}

/** 阶段排名 */
export function getPerformance(prodCode) {
    return Http.Get(hostMall3 + '/eplus/api/prod/get/performance.json', {
        prodCode: prodCode
    })
}

/** 基金历史净值、信托七日年化 */
export function getHistoryNav(prodCode, prodId, retRows, prodType) {
    console.log("dfasdfdasfasdffffase")
    console.log(prodCode, prodId, retRows, prodType)
    return Http.Get(hostMall3 + '/eplus/api/prod/get/historyNav.json',
        {
            prodCode: prodCode,
            prodId: prodId,
            retRows: retRows,
            prodType: prodType
        }
    )
}

/** 查询公募基金和资管产品经理信息 */
export function getProdManager(prodCode, prodId, prodType) {
    return Http.Get(hostMall3 + '/eplus/api/prod/get/prodManager.json', {
        prodCode: prodCode, prodId: prodId, prodType: prodType
    })
}

/** 查询基准指数 */
export function getBenchMarkIndex(prodCode) {
    return Http.Get(hostMall3 + '/eplus/api/prod/get/benchMarkIndex.json', {
        prodCode: prodCode
    })
}

/** 基金实时估值 */
export function getFundNewstNav(prodCode, prodId, prodType) {

    return Http.Get(hostMall3 + '/eplus/api/prod/get/fundNewstNav.json', {
        prodCode, prodId, retRows: "", prodType, // flag: "2",
        // prodCd: "000300",
        // queryType: "YJZS",
        // endDate: "20200923"
    })
}

/** 公募基金业绩走势图 */
export function getChartData(prodCode, prodId, retRows, prodType, flag, prodCd, queryType, endDate) {
    return Http.Get(hostMall3 + '/eplus/api/prod/get/historyNav.json', {
        prodCode: prodCode,
        prodId: prodId,
        retRows: retRows,
        prodType: prodType,
        flag: flag,
        prodCd: prodCd,
        queryType: queryType,
        endDate: endDate
    })
}

/** 获取用户某段时间某只基金产品的首次买卖时间点 */
export function getBuyAndSellPoint(prodCode, type, prodId, flag, prodCd, beginDate, endDate) {
    return Http.Get(hostMall3 + '/eplus/api/prod/get/buyAndSellPoint.json', {
        prodCode,
        type,
        prodId,
        flag,
        prodCd,
        beginDate,
        endDate
    })
}

/** 基金诊断雷达图全部 */
// https://i.gtja.com/mall/eplus/fundation/detail/data.json
// https://i.gtja.com/mall3/eplus/api/prod/get/radarInfo.json?prodCode=000216&investType=6
export function getRadarInfoAll(prodCode, investValue) {
    return Http.Get(hostMall3 + '/eplus/fundation/detail/data.json', {
        investValue: investValue, prodCode: prodCode
    })
}

/** 获取产品剩余额度 */
export function getRemainAmount(prodCode, prodType, prodTypeChild) {
    return Http.Get(hostMall3 + '/eplus/api/index/get/remianAmount.json', {
        prodCode: prodCode, prodType: prodType, prodTypeChild: prodTypeChild
    })
}

/** 交易规则时间轴 */
export function getTradeTimeLine() {
    return Http.Get(hostMall3 + '/eplus/api/prod/get/tradeTimeLine.json')
}

// 获取公募基金合同协议
export function getPublicOnlyShowAgreement(prodCode, prodType) {
    return Http.Get(hostMall + '/eplus/api/agreement/get/publicOnlyShowAgreement.json', {prodCode, prodType})
}

// 获取基金规模
export function getFundScale(prodCode) {
    return Http.Get(hostMall + '/eplus/api/prod/get/queryFundScale.json', {prodCode})
}

// 获取公募基金基本资料
export function getFundIntroduce(prodId, prodCode) {
    return Http.Get(hostMall + '/eplus/api/prod/get/queryFundIntroduce.json', {prodId, prodCode})
}

/** 通用 获取产品信息T1000004 */
export function getProdInfo(prodCode) {
    return Http.Get(hostMall3 + '/eplus/api/prod/get/prodInfo.json', {
        prodCode: prodCode
    })
}

// 获取基金分红数据
export function getShareBonus(prodId, prodCode, curPage) {
    return Http.Get(hostMall + '/eplus/api/publicFund/get/shareBonus.json', {prodId, prodCode, curPage})
}

// 获取基金公司列表
export function getFundCompany(comId, comName) {
    return Http.Get(hostMall + '/eplus/api/prod/get/queryFundCompany.json', {comId, comName})
}

// export default class prodDetail {
//
//     /** 获取产品费率 */
//     getFundRate(prodCode, prodId, refSta) {
//         return Http.Get(hostMall + '/eplus/api/prod/get/fundRate.json', {
//             prodCode: prodCode, prodId: prodId, refSta: refSta
//         })
//     }
//
//     /** 获取资管费率 */
//     getZgRateStructure(prodCode, prodId) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/zgRateStructure.json', {
//             prodCode: prodCode, prodId: prodId
//         })
//     }
//
//
//     /** 私募资管产品合格投资者权限查询 */
//     getInvestorRight() {
//         return Http.Get(hostMall3 + '/eplus/api/index/get/investorRight.json')
//     }
//
//
//     // 高端弹窗记录流水
//     recordLogOfGDTC(prodCode) {
//         // TODO 接口base？？
//         return http.Post('/eplus/api/pems/get/recordYanqianSerial.json', qs.stringify({
//             prodCode: prodCode
//         }))
//     }
//
//
//     /** 获取新发资管产品简介和投资范围 */
//     getZgProDescription(prodCode, prodId, prodType, prodTypeChild) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/zgProDescription.json', {
//             prodCode: prodCode, prodId: prodId, prodType: prodType, prodTypeChild: prodTypeChild
//         })
//     }
//
//
//     /** 阶段排名 */
//     getPerformance(prodCode) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/performance.json', {
//             prodCode: prodCode
//         })
//     }
//
//
//     /** 基金历史净值、信托七日年化 */
//     getHistoryNav(prodCode, prodId, retRows, prodType) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/historyNav.json', {
//             prodCode: prodCode, prodId: prodId, retRows: retRows, prodType: prodType
//         })
//     }
//
//

//
//
//     /** 基金诊断雷达图 */
//     getRadarInfo(prodCode, investType) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/radarInfo.json', {
//             prodCode: prodCode, investType: investType
//         })
//     }
//
//

//
//

//
//
//     // /**
//     //    * 获取分享小程序详情信息-T1000023
//     //    * @param shareCode   分享代码
//     //    * @param auditStatus 审核状态 (1：审核通过 2：待审核 3：未通过审核)
//     //    * @returns
//     //    */
//     // getSharingAppletDetails(shareCode, auditStatus) {
//     // 	return Http.Get(env.API_HOST_MALL + '/eplus/api/weChatStore/post/getSharingAppletDetails.json', {
//     // 		investValue: investValue,
//     // 		prodCode: prodCode
//     // 	})
//     // },
//     //https://i.gtja.com/mall/eplus/api/weChatStore/post/getSharingAppletDetails.json  //详情页面 海报等数据
//     /** 查询协议附件 */
//     getProtocol(prodCode, prodId) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/protocol.json', {
//             prodCode: prodCode, prodId: prodId
//         })
//     }
//
//
//     /** 高端产品咨询功能--客户响应人查询 */
//     getClientResponser() {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/clientResponser.json')
//     }
//
//
//     /** 查询产品简介和产品交易规则 */
//     getTradeRule(prodCode, prodType, prodTypeChild, prodSponsor) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/tradeRule.json', {
//             prodCode: prodCode, prodType: prodType, prodTypeChild: prodTypeChild, prodSponsor: prodSponsor
//         })
//     }
//
//

//
//

//
//
//     /** 基金实时估值误差 */
//     getFundNewstNavError(prodCode) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/fundNewstNavError.json', {
//             prodCode: prodCode
//         })
//     }
//
//
//     /** 查询公募基金和资管产品经理信息 */
//     getProdManager(prodCode, prodId, prodType) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/prodManager.json', {
//             prodCode: prodCode, prodId: prodId, prodType: prodType
//         })
//     }
//
//

//
//

//
//

//
//

//
//
//     // 是否为C95用户
//     getIsC95Account(prodCode, prodType, prodTypeChild, prodtaNo, businessFlag) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/checkYhlcAccount.json', {
//             prodCode, prodType, prodTypeChild, prodtaNo, businessFlag
//         })
//     }
//
//
//     // 收益即将开售倒计时
//     getSaleTime(refBegDate, refEndDate, buyWay, buyDate, buyBgnDate, buyEndDate, buyBgnTime) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/preSaleTime.json', {
//             refBegDate, refEndDate, buyWay, buyDate, buyBgnDate, buyEndDate, buyBgnTime
//         })
//     }
//
//
//     // 指数行情历史数据
//     getIndexMarket(proCode, dayNum) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/indexMarket.json', {
//             proCode, dayNum
//         })
//     }
//
//
//     // 收益凭证挂钩标的走势图
//     getSypzTrendChart(prodCode, dayNum) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/sypzTrendChart.json', {
//             prodCode, dayNum
//         })
//     }
//
//
//     // 收益凭证挂钩标的涨跌平获取
//     getSypzTrendFlag(prodCode) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/sypzTrendFlag.json', {
//             prodCode
//         })
//     }
//
//
//     // 判断logo是否存在 财富号入口
//     getCfhEnter(managerId) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/fortuneAccountHomeInfo.json', {
//             managerId
//         })
//     }
//
//

//
//
//     // 检查用户是否需要风险测评、CRS补录、适当性补录
//     checkUserSuitabilityInfo() {
//         return Http.Get(hostMall3 + '/eplus/api/pems/get/checkUserSuitabilityInfo.json')
//     }
//
//
//     // 检查信托产品是否需要在线视频双录
//     checkVideoOnline(prodCode, prodType, prodTypeChild, prodtaNo) {
//         return Http.Get(hostMall3 + '/eplus/api/pems/get/checkVideoOnline.json', {
//             prodCode, prodType, prodTypeChild, prodtaNo
//         })
//     }
//
//     // 预约产品额度信息
//     // prodKind 0-公墓基金 1-OTC 信托
//     getReserveProductQuotaInquiry(prodCode, prodKind) {
//         return Http.Get(hostMall3 + '/eplus/api/prod/get/reserveProductQuotaInquiry.json', {
//             prodCode, prodKind
//         })
//     }
//
//
//     // 获取产品公告列表
//     static getProdNotice(prodCode, noticeType, pageSize, pageNum) {
//         return Http.Get(hostMall + '/eplus/api/prod/get/queryProdNotice.json', {
//             prodCode, noticeType, pageSize, pageNum
//         })
//     }
//
//
//     // 产品实时估值
//     getFundNewstNavError(prodCode) {
//         return Http.Get(hostMall + '/eplus/api/prod/get/fundNewstNavError.json', {prodCode})
//     }
//
//

//
//

//
//

//
//

//
//
//
//
//     // 获取基金分红
//     getFundsBonusList(prodCode, prodId, prodType, curPage) {
//         return Http.Post(hostMall + '/eplus/cache/rest/getShareBonus.json', {prodCode, prodId, prodType, curPage})
//     }
// }
