import qs from 'qs'
import Http from '../shopRequest' // 用flyio构建的请求 只支持H5、 微信小程序、 支付宝小程序
// import Http from '@/apiUtils/uniRequestUtils.js'  // 用 uni.request 构建的请求
const hostMall = import.meta.env.VITE_BASE_SHOP_HOST
const hostMall3 = import.meta.env.VITE_BASE_SHOP_HOST3// 自选基金
export default {
    /**
     * 自选基金查询
     * @param groupNo   群组代码
     * @param acctCode  手机号
     * @returns {*}
     */
    querySelfFund(groupNo, acctCode) {
        return Http.Post(hostMall3 + '/eplus/api/selfFund/post/querySelfFund.json', qs.stringify({groupNo, acctCode}))
    },

    /**
     * 自选基金查询今日估值等信息--T2000057
     * @param prodCode      基金代码
     * @param oFundTatalNet 插入基金时的复权累计净值
     * @returns {*}
     */
    querySelfFundTodayValuation(prodCode, ofundTatalNet) {
        return Http.Post(hostMall3 + '/eplus/api/selfFund/post/querySelfFundTodayValuation.json', qs.stringify({
            prodCode,
            ofundTatalNet
        }))
    },

    /**
     * 基金收益率查询
     * @param codeList 基金数组
     * @returns {*}
     */
    queryFundProfit(codeList) {
        return Http.Post(hostMall3 + '/eplus/api/selfFund/post/queryCumulativeNetValueAndIncome.json', {codeList})
    },

    /**
     * 持仓收益查询
     * @param codeList 基金数组
     * @returns {*}
     */
    queryCustomerHoldRate(codeList) {
        return Http.Post(hostMall3 + '/eplus/api/selfFund/post/queryCustomerHoldRate.json', {codeList})
    },

    /**
     * 热销基金查询
     * @param requestNum 请求数量
     * @param requestNum 请求类型
     * @param requestNum 统计天数
     * @returns {*}
     */
    querySalesProducts(requestNum, prodType, recentDay) {
        return Http.Post(hostMall3 + '/eplus/api/selfFund/post/querySalesProducts.json', qs.stringify({
            requestNum,
            prodType,
            recentDay
        }))
    },

    /**
     * 自选基金管理
     * batchFlag      批量标志 (00：单条插入 01:单条删除 02：单条更新 04:排序更新 10：批量插入 11:批量覆盖 12：批量删除  )
     * acctType       账号类型 挂在资金账户下，账户类型传1，账户号是资金账号；挂在手机号下，账户类型传6，账户号是手机号
     * acctCode       账号号码
     * OFUND_CODE     基金代码
     * SHOW_ORDER     显示顺序，越大越靠前
     * COMPANY_NO     基金公司代码
     * GROUP_NO       群组代码，默认为0
     * OFUND_TATAL_NET基金累计净值，更新操作时，要传插入时候的值
     * CREATE_DATE    创建日期 添加新的自选基金时需要获取服务器当前时间 前端不传日期和时间
     * CREATE_TIME    创建时间
     * batchDataList  基金代码,显示顺序,公司代码,群组代码,基金累计净值,创建日期,创建时间
     *                入参格式：000001,1,SZ,0,0.9010,20191001,100159|000002,1,SZ,0,1.0001,20191001,100159
     * remark         备注 如果来源是手机，备注可以存放安卓、IOS等
     *
     * @param paramsMap 参数map集合
     */
    selfFundManage(paramsMap) {
        return Http.Post(hostMall3 + '/eplus/api/selfFund/post/selfFundManage.json', {paramsMap})
    },

    /**
     * <pre>
     *     自选基金-为你推荐产品列表-T2000059
     *      该接口最终返回自选基金推荐产品列表
     *      从功能号足迹中查询产品代码作为入参，去掉新发基金产品，如果不足则查询销量最高的产品补足产品数量
     * </pre>
     *
     * @param prodCode      产品代码列表 以“,”分割，如 "000873,01938,519021"
     * @param requestNum    请求行数
     * @param prodType      产品类型
     * @param recentDay     近多少天
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.String>>
     * @author CP_liuaqiang
     * @date 2020/4/8 17:01
     **/
    queryRecommendedProductList(prodCode, requestNum, prodType, recentDay) {
        return Http.Post(hostMall3 + '/eplus/api/selfFund/post/queryRecommendedProductList.json', qs.stringify({
            prodCode,
            requestNum,
            prodType,
            recentDay
        }))
    },

    /*
      https://i.gtja.com/mall/eplus/api/publicFund/get/prodSearch.json
      基金搜索接口
    */
    queryAllFunds() {
        return Http.Post(hostMall + '/eplus/api/publicFund/get/prodSearch.json')
    }
}
