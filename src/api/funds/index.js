import qs from 'qs'
import Http from '../shopRequest' // 用flyio构建的请求 只支持H5、 微信小程序、 支付宝小程序
// import Http from '@/apiUtils/uniRequestUtils.js'  // 用 uni.request 构建的请求
const hostMall = import.meta.env.VITE_BASE_HOST_MALL
const hostMall3 = import.meta.env.VITE_BASE_HOST_MALL3

let cacheFlag = ''
// 预览精准匹配二级栏目，secItemCode待匹配的二级栏目
let secItemCode = ''

// 商城首页
/**
 * 获取栏目-T1000001
 * @param itemCodeSuper 上级栏目代码
 * @param yyzver        易阳指版本号
 * @param    栏目用途
 * @param channelCode   渠道代码
 */
// lm013613 测试 lm002584   lm034710
// eplus/api/index/get/columns.json?itemCodeSuper=lm002584&yyzver=8.26.0&itemPurpose=1&cacheFlag=
export function getColumns(itemCodeSuper = 'lm034710', yyzver = '8.26.0', itemPurpose = '1', channelCode) {
    return Http.Get(hostMall + '/eplus/api/index/get/columns.json', {
        itemCodeSuper: itemCodeSuper,
        yyzver: yyzver,
        itemPurpose: itemPurpose,
        cacheFlag: cacheFlag,
        channelCode: channelCode,
        itemCode: secItemCode
    })
}

/** 获取栏目下的广告图片  ?itemCode=lm002586&yyzver=8.26.0&itemPurpose=1&cacheFlag=&channelCode=*/
export function getAdImgs(itemCode, yyzver = '8.26', itemPurpose = '1', channelCode = '', cachFlagVar, custCode) {
    cachFlagVar = cachFlagVar || cacheFlag
    return Http.Get(hostMall3 + '/eplus/api/index/get/adImgs.json', {
        itemCode: itemCode,
        yyzver: yyzver,
        itemPurpose: itemPurpose,
        cacheFlag: cachFlagVar,
        channelCode: channelCode,
        custCode: custCode
    })
}

/** 获取二级栏目下产品列表 */
export function getProdList(itemCode, yyzver = '8.26', itemPurpose = '1', channelCode = '') {
    return Http.Get(hostMall + '/eplus/api/index/get/prodList.json', {
        itemCode: itemCode, yyzver: yyzver, itemPurpose: itemPurpose, cacheFlag: cacheFlag, channelCode: channelCode
    })
}


// 获得自定义专区浮窗属性-T1000021
export function getAreaAttribute(itemCode) {
    return Http.Get(hostMall3 + '/eplus/api/shelf/get/columnAttribute.json', {itemCode})
}

// 获得股票行情数据
export function getStockMarket(xdxrType, stkcodeList) {
    // return http.GetMarket('/quotes/quote/stockBatch.json', {xdxrType, stkcodeList})
}

// 获得资讯浏览量
export function getBrowserNum(actionTargetId, actionTargetType) {
    return Http.Get(hostMall3 + '/eplus/api/shelf/get/userBehavior.json', {actionTargetId, actionTargetType})
}

// 获取分享口令
export const getShareToken = function (longUrl) {
    return Http.Get(hostMall + 'jh/queryShortUrl.json', {longUrl})
}
