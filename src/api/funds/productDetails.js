import qs from 'qs'
import Http from '../shopRequest' // 用flyio构建的请求 只支持H5、 微信小程序、 支付宝小程序
// import Http from '@/apiUtils/uniRequestUtils.js'  // 用 uni.request 构建的请求
const hostMall3 = import.meta.env.VITE_BASE_HOST_MALL3

// 产品详情T1000004公共请求，为所有需要查询产品详情业务服务，请谨慎修改
export default {
  /**
   * 产品详情页获取产品详情
   * @param prodCode 产品代码
   * @param sourceFlag 产品来源 1010:转让市场 2010:持仓 0010或空:正常来源 3010:策略中心 5010:百事通全连接
   * @param walletCode 活动钱包代码
   * @param outWalletCode 活动外部卡券代码
   * @param tgljInfo 推广信息
   */
  getProdDetails (prodCode, sourceFlag, walletCode, outWalletCode, tgljInfo) {
    return Http.Get(hostMall3 +'/eplus/api/prod/get/prodDetails.json', {prodCode: prodCode, sourceFlag: sourceFlag, walletCode: walletCode, outWalletCode: outWalletCode, tgljInfo: tgljInfo})
  },
  /** 通用 获取产品信息T1000004 */
  getProdInfo (prodCode) {
    return Http.Get(hostMall3 +'/eplus/api/prod/get/prodInfo.json', {prodCode: prodCode})
  }
}
