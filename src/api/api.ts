import {request} from './request'
const getKvVideoInfo = (data: object | any)=>request('https://i.gtja.com/wxapp/shandong/getKvVideoInfo.json', 'GET', data)
// 7*24小时查用户中心接口展示；
const getRealTimeNews =(data: object | any)=>request('https://i.gtja.com/wxapp/wechatShop/news/getRealTimeNews.json', 'GET', data)
// 7*24小时资讯详情接口
const getNewsDetail =(data: object | any)=>request('https://i.gtja.com/wxapp/wechatShop/news/fornum/detail.json', 'GET', data)//资讯详情接口
//首席说人员列表接口
const selPersonList =(data: object | any)=>request('https://i.gtja.com/wxapp/shandong/selPersonList.json', 'GET', data)
//首席说人员的文章列表
const getNewsList =(data: object | any)=>request('https://i.gtja.com/wxapp/shandong/getNewsList.json', 'GET', data)

// 数据解盘(高宇)
const getChart =(data: object | any)=>request('https://api.app.gtja.com/gt/sjjp/v2/1036', 'POST', data)//资讯详情接口
/*
* 选股诊股
*/
// 搜索股票测试接口
const search =(data: object | any)=>request('https://i.gtja.com/wxapp/shandong/search.json', 'GET', data)//资讯详情接口
/*
*选基诊基
*/
// 甄选100接口
const prodListInfo =(data: object | any)=>request('https://i.gtja.com/mall/eplus/api/lightMallHome/get/prodListInfo.json', 'GET', data)//资讯详情接口
// 指数基金
const prodList =(data: object | any)=>request('https://i.gtja.com/mall/eplus/api/index/get/prodList.json', 'GET', data)
// 获取文章内容详情
const getContent =(data: object | any)=>request('https://i.gtja.com/wxapp/wechatShop/news/fornum/detail.json', 'GET', data)
// 获取ip属地
const getRegion =(data: object | any)=>request('https://i.gtja.com/wxapp/wechatShop/jh/getAutherNo.json', 'GET', data)
// 首席说文章列表查询
const getArticles =(data: object | any)=>request('https://i.gtja.com/wxapp/wechatShop/jh/article.json', 'GET', data)
// 首席说人员文章列表查询
const getArtList =(data: object | any)=>request('https://i.gtja.com/wxapp/wechatShop/jh/getJhNewsList.json', 'GET', data)

// 授权接口
const getAuth =(data: object | any)=>request('https://i.gtja.com/wxapp/auther/getWxappUser.json', 'GET', data)

export{
    getKvVideoInfo,
    getRealTimeNews,
    selPersonList,
    getNewsList,
    getNewsDetail,
    getChart,
    search,
    prodListInfo,
    prodList,
    getContent,
    getRegion,
    getArticles,
    getArtList,
    getAuth

}
