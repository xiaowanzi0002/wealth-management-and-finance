let ONLINE_DOMAN_NAME = ""; //当前域名
// #ifdef  H5
ONLINE_DOMAN_NAME=window.location.host
// #endif
// #ifdef  MP-WEIXIN
ONLINE_DOMAN_NAME = "填写你的域名";
// #endif


export default {
  // 关于接口loading的配置
  loading: {
    limitTime: 2000, // 接口请求在xxxms内完成则不展示loading
    loadingShow: () => {
      uni.showLoading({
        title: "加载中",
        mask: true
      });
    },
    loadingHide: () => {
      uni.hideLoading();
    }
  },
  // 接口请求的默认配置
  reqConfig: {
    isLoading: true // 是否展示loading，默认为true
  },
  // fly的默认配置
  flyConfig: {
    headers: {
      "content-Type": "application/x-www-form-urlencoded;charset=UTF-8" //php的post传输请求头一定要这个 不然报错 接收不到值
    },
    timeout: 50000,
    baseURL: '', //process.env.NODE_ENV === "development" ? "" : "",
    withCredentials: true
  },
  //登录失败的
  resLoginFail: {
    key: "code", // 与后台规定的表示登录失败的变量
    value: -1 // 与后台规定的表示登录成功的code
  },
  // 异常情况
  resError: {
    // 异常默认提示的方法
    tipShow: err => {
      uni.showToast({
        title: (err && err.message) ||
          (err && err.reason) ||
          "服务器升级中，请稍后重试。",
        icon: "none",
        mask: true
      });
    }
  }
};