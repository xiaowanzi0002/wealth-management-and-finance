//服务器接口地址
// @ts-ignore
const baseURL : string = import.meta.env.VITE_BASE_HOST

//本地调试接口地址
// const baseURL:string='http://xxxxxx'
// 封装公共请求方法
function request(url : string, method : "GET" | "POST" | undefined, data : object | any) {
	return new Promise(function (resolve, reject) {
		let header : any
		if (uni.getStorageSync('token') !== undefined && uni.getStorageSync('token') !== "") {
			header = {
				'content-type': 'application/json',
				'Token': uni.getStorageSync('token')
			};
		} else {
			header = {
				'content-type': 'application/json',
			};
		}
		uni.request({
			// url: baseURL + url,
			url:url,
			method: method,
			data: data,
			header: header,
			success(res : any) {
				uni.hideLoading()
				
				if (res.statusCode === 200) {

					resolve(res.data);
				} else {
					//其他异常
					uni.showToast({
						title: res.data.msg,
						icon: 'none'
					})
					reject(res.data);
				}
			},
			fail(err) {
				uni.hideLoading()
				//请求失败
				uni.showToast({
					title: '无法连接到服务器',
					icon: 'none'
				})
				reject(err)
			}
		})
	})
}

export { request, baseURL }
