import { createSSRApp } from "vue";
import App from "./App.vue";
import * as Pinia from "pinia";
import "@/static/common.scss";

import { provideVueInstance } from "@/utils/utils";

import store from "./store/vuex/index";
import sensor from "./utils/sensor.js";
//分享设置
import share from './utils/wxShare.js'

export function createApp() {
  const app = createSSRApp(App);
  app.use(Pinia.createPinia());
  app.use(store);
  app.mixin(share)
  provideVueInstance(app);
  app.config.globalProperties.$sensorEvt = sensor.sensorEvt;
  return {
    app,
  };
}
