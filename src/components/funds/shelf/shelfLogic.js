
import {itemIsDisplay, getRemainAmountUt, toProdDetail, toMoreUrl, handleRate, jumpDetailUrl} from '@/utils/indexUtils.js'
import {sFin2ndpageClick, sfinPrd} from '@/utils/fund/scBuyPoint.js'
import {pageJump} from '@/utils/fund/fundUtil.js'
// import {regexReplaceUrlParam, initClipboardListen} from '@/assets/js/util.js'
import {mapGetters, mapActions} from 'vuex'

export default {
  props: ['secColumn', 'index', 'isFrom', 'prodInfo'],
  data () {
    return {
      prodList: [],
      allProdList: [],
      // 设定当前展示数据条数
      groupSize: 0,
      // 是否展示剩余额度
      showRemainAmountFlag: false,
      // 收益率是否添加 “+”
      handleRateFlag: false,
      // detailUrl: global.MGC.getHtmlUrl(process.env.API_HOST, '/eplus/prodDetails'),
      // 浮层货架显示控制
      isFloatShow: true,
      tgljInfo: '',//global.MGC.queryUriSingleParam('tglj_info'),
      isyyzshare:''// global.MGC.queryUriSingleParam('isyyzshare')
    }
  },
  computed: {
    ...mapGetters({
      adProds: 'adProds',
      select: 'select',
      channelCode: 'channelCode',
      theme: 'theme',
      adImgs: 'adImgs'
    }),
    selectedIndex () {
      return this.select.indexOf(true)
    }
  },
  mounted () {
    // 产品类货架
    if (this.secColumn.SHELF_TYPE === '1') {
      // 换一批功能 每次展示几条数据
      if (this.secColumn.SHELF_BROWSE_WAY === '4' || this.secColumn.SHELF_BROWSE_WAY === '9') {
        this.groupSize = 1
      } else if (this.secColumn.SHELF_BROWSE_WAY === '2' || this.secColumn.SHELF_BROWSE_WAY === '5') {
        this.groupSize = 2
      } else if (this.secColumn.SHELF_BROWSE_WAY === '6' || this.secColumn.SHELF_BROWSE_WAY === '13' || this.secColumn.ANOTHER_BATCH === '1') {
        this.groupSize = 3
      }
      // 展示额度标签: 横向一列无伴随增强、横向一列有伴随增强、纵向一列、纵向一列特点标签增强、纵向二、三列无增强
      // 处理收益率是否展示‘+’号: 横向一列无伴随增强、纵向二、三列无增强、纵向一列、纵向一列特点标签增强
      if (this.secColumn.SHELF_BROWSE_WAY === '1' || this.secColumn.SHELF_BROWSE_WAY === '4' || this.secColumn.SHELF_BROWSE_WAY === '9' ||
      ((this.secColumn.SHELF_BROWSE_WAY === '6' || this.secColumn.SHELF_BROWSE_WAY === '5') && this.secColumn.PROD_WITH_ENHANCED === '1')) {
        this.showRemainAmountFlag = true
        if (this.secColumn.SHELF_BROWSE_WAY === '1' && this.secColumn.PROD_WITH_ENHANCED !== '1') {
          this.handleRateFlag = false
        } else {
          this.handleRateFlag = true
        }
      }
    } else if (this.secColumn.SHELF_TYPE === '2') {
      // 图片类货架
      // 换一批功能 每次展示几条数据
      if (this.secColumn.SHELF_BROWSE_WAY === '4') {
        this.groupSize = 1
      } else if (this.secColumn.SHELF_BROWSE_WAY === '5') {
        this.groupSize = 2
      } else if (this.secColumn.SHELF_BROWSE_WAY === '6' || ((this.secColumn.SHELF_BROWSE_WAY === '1' ||
      this.secColumn.SHELF_BROWSE_WAY === '17') && this.secColumn.ANOTHER_BATCH === '1')) {
        this.groupSize = 3
      }
    }
    this.getData()
  },
  methods: {
    ...mapActions([
      'getProds',
      'getBrowserNum',
      'getStockMarket',
      'getAdImgs'
    ]),
    getData () {
      // 获取货架数据
      if (this.secColumn.SHELF_TYPE === '2') {
        // T1000003
        this.getImgData()
      } else {
        //  T1000002
        this.getProdList()
      }
    },
    getImgData () {
      // 图片货架
      let cacheData = this.adImgs[this.secColumn.ITEM_CODE]
      if (cacheData !== undefined && Object.keys(cacheData).length !== 0) {
        this.prodListHandle(cacheData)
      } else {
        let itemObj = {
          itemCode: this.secColumn.ITEM_CODE,
          channelCode: this.channelCode,
          shelfBrowseWay: this.secColumn.SHELF_BROWSE_WAY
        }
        this.getAdImgs(itemObj).then(data => {
          console.info(this.secColumn.ITEM_CODE, this.secColumn.ITEM_NAME)
          console.info(data)
          this.prodListHandle(data)
        })
      }
    },
    getProdList () {
      if (this.secColumn.SHELF_TYPE === '5') {
        // 浮层货架已关闭浮层在本次回话期间不再显示
        if (sessionStorage.getItem(this.secColumn.ITEM_CODE + 'closeFloatShelf') === '1') {
          return
        }
      }
      let cacheData = this.adProds[this.secColumn.ITEM_CODE]
      if (cacheData !== undefined && Object.keys(cacheData).length > 0) {
        this.handleData(cacheData)
      } else {
        let secColumnCode = {}
        secColumnCode[this.secColumn.ITEM_CODE] = this.secColumn.SHELF_TYPE
        let itemObj = {
          itemCode: JSON.stringify(secColumnCode),
          channelCode: this.channelCode
        }
        this.getProds(itemObj).then(data => {
          console.info(this.secColumn.ITEM_CODE, this.secColumn.ITEM_NAME)
          console.info(data)
          if (data.success !== false) {
            this.handleData(data)
          }
        })
      }
    },
    handleData (data) {
      if (this.secColumn.SHELF_TYPE === '1') {
        // 产品货架数据处理
        this.prodListHandle(data)
      } else if (this.secColumn.SHELF_TYPE === '3') {
        // 资讯货架数据处理
        this.newsListHandle(data)
      } else if (this.secColumn.SHELF_TYPE === '5') {
        // 浮层货架数据处理
        this.floatListHandle(data)
      } else if (this.secColumn.SHELF_TYPE === '4') {
        // 聚合类货架
        this.prodList = data
        this.handleShelfData(data)
      }
    },
    floatListHandle (data) {
      // 定时浮层
      if (this.secColumn.PROD_WITH_ENHANCED === '10') {
        // 控制浮层的显示和隐藏 方法
        setTimeout(() => {
          this.isFloatShow = false
          sessionStorage.setItem(this.secColumn.ITEM_CODE + 'closeFloatShelf', '1')
        }, 5000)
      }
      let list = []
      for (let index in data) {
        // 根据用户营业部、券码定制化展示
        if (!itemIsDisplay(data[index], 'produce', this.hasLogin, this.user)) {
          continue
        }
        list.push(data[index])
      }
      this.prodList = list
    },
    newsListHandle (data) {
      if (this.secColumn.SHELF_BROWSE_WAY === '12' ||
      (this.secColumn.SHELF_BROWSE_WAY === '1' && this.secColumn.PROD_WITH_ENHANCED === '5')) {
        // 横向一列文字平铺式、横向一列右侧方图伴随式增强  查询阅读数
        let actionTargetId = ''
        data.forEach(ele => { actionTargetId += ele.PROD_CODE + ',' })
        actionTargetId = actionTargetId.slice(0, -1)
        if (actionTargetId) {
          this.getBrowserNum(actionTargetId).then((browserNumRes) => {
            console.log('browserNumRes', browserNumRes)
            if (browserNumRes.success !== false) {
              data.forEach((ele, index) => {
                ele.BROWSER_NUM = browserNumRes[index].ACTION_COUNT
              })
            }
            this.prodList = data
          }).catch(() => { this.prodList = data })
        } else {
          this.prodList = data
        }
      } else if (this.secColumn.SHELF_BROWSE_WAY === '1' && this.secColumn.PROD_WITH_ENHANCED === '7') {
        // 横向一列小色块儿宽松式伴随式增强
        let stockList = []
        data.forEach((ele, index) => {
          if (ele.SECU_SHT) {
            let stockCodeList = ele.SECU_SHT.split('||')[0].split(',')[0].split('.')
            stockList.push(stockCodeList[1].toLowerCase() + stockCodeList[0])
          } else {
            stockList.push('')
          }
        })
        this.getStockMarket(JSON.stringify(stockList)).then((riseRes) => {
          if (riseRes.success !== false) {
            data.forEach((ele, index) => {
              ele.RISE = riseRes.result[index].rise
            })
          }
          this.prodList = data
        }).catch(() => { this.prodList = data })
      } else {
        this.prodList = data
      }
    },
    prodListHandle (data) {
      let list = []
      let filterRule = this.secColumn.SHELF_BROWSE_WAY === '13' ? 'shelfBrowseWay13' : 'product'
      for (let index in data) {
        // 根据用户营业部、券码定制化展示
        if (!itemIsDisplay(data[index], filterRule, this.hasLogin, this.user)) {
          continue
        }
        if (this.showRemainAmountFlag) {
          let btnList = []
          if (data[index].PROD_FEATURE_LABEL === '') {
            data[index].btnList = []
          } else {
            btnList = data[index]?.PROD_FEATURE_LABEL?.split('，')
          }
          getRemainAmountUt(data[index], btnList)
        }
        // 处理收益率是否展示‘+’号
        if (this.handleRateFlag) {
          data[index].yieldFormat = handleRate(data[index])
        }
        // 纵向一列特点标签增强货架 保留2个特点增强标签
        if (this.secColumn.SHELF_TYPE === '1' && this.secColumn.SHELF_BROWSE_WAY === '9') {
          data[index].PROD_FEATURE_LABEL_ENHANCED = data[index].PROD_FEATURE_LABEL_ENHANCED.slice(0, 2)
        }
        list.push(data[index])
      }
      if (this.groupSize > 0 && Object.keys(list).length > 0) {
        this.prodList = list.slice(0, this.groupSize)
        this.allProdList = list
      } else {
        this.prodList = list
      }
    },
    /** 控制二级栏目更多标识跳转 埋点   jumpUrl*/
    jumpUrl () {
      if (this.secColumn.MORE_TYPE === '2' || this.secColumn.MORE_TYPE === '3') {
        sFin2ndpageClick(this.selectedIndex, '更多', this.secColumn)
        if (global.MGC.isNotBlank(this.isFrom) && global.MGC.isNotBlank(this.prodInfo)) {
          if (this.isFrom === '详情页') {
            sfinPrd(this.prodInfo, 'fin_prd_page_click', '18', {'code': this.secColumn.ITEM_CODE, 'name': this.secColumn.ITEM_NAME, 'prior': '01', 'lname': '', 'lurl': ''})
          } else if (this.isFrom === '结果页') {
            sfinPrd(this.prodInfo, 'fin_match_result_click', '12', {'code': this.secColumn.ITEM_CODE, 'name': this.secColumn.ITEM_NAME, 'prior': '01', 'lurl': ''}, this.secColumn)
          }
        }
      }
      sFin2ndpageClick(this.selectedIndex, '栏目标题区', this.secColumn)
      if (global.MGC.isNotBlank(this.isFrom) && global.MGC.isNotBlank(this.prodInfo)) {
        if (this.isFrom === '详情页') {
          sfinPrd(this.prodInfo, 'fin_prd_page_click', '18', {'code': this.secColumn.ITEM_CODE, 'name': this.secColumn.ITEM_NAME, 'prior': '01', 'lname': '', 'lurl': ''})
        } else if (this.isFrom === '结果页') {
          sfinPrd(this.prodInfo, 'fin_match_result_click', '12', {'code': this.secColumn.ITEM_CODE, 'name': this.secColumn.ITEM_NAME, 'prior': '01', 'lurl': ''}, this.secColumn)
        }
      }
      this.columnPoint()
      // toMoreUrl(this.secColumn)
    },
    /** 点击产品货架跳转产品详情页 */
    jumpDetail (prodList, index) {
		let prod=JSON.parse(prodList)
		// this.columnProPoint(prod)
		pageJump(prod.DETAIL_URL,prod)
    },
    /** 直接url跳转 */
    goToDetail (item) {
		if(Object.prototype.toString.call(item)==='[Object String]' ){
			item=JSON.parse(item)
		}
		// this.columnProPoint(item)
      if (this.secColumn.SHELF_TYPE === '2') {
		pageJump(item.ADVERTISE_URL,item)
        // jumpDetailUrl(item.ADVERTISE_URL,item)//lm020513
      } else {
		  pageJump(item.DETAIL_URL,item)
        // jumpDetailUrl(item.DETAIL_URL,item)
      }
    },
    /** 跳转购买地址 */
    jumpBuy (pro) {
      let url = ''
      // if (pro.BUTTON_LINK !== '') {
      //   if (global.MGC.isNotBlank(this.tgljInfo)) {
      //     url = regexReplaceUrlParam(pro.BUTTON_LINK, 'tglj_info', this.tgljInfo)
      //   } else {
      //     url = pro.BUTTON_LINK
      //   }
      // } else {
      //   url = this.detailUrl + '?prodCode=' + pro.PROD_CODE
      //   if (global.MGC.isNotBlank(this.tgljInfo)) {
      //     url = url + '&tglj_info=' + this.tgljInfo
      //   }
      // }
      // if (pro.NO_OPEN_KEY === '2' && pro.FORCE_SHOW_KEY === '2' && !this.hasLogin) {
      //   if (this.isyyzshare === '1' && global.MGC.getAPPVer() === '') {
      //     initClipboardListen('#go-load-clipboard', url)
      //     window.location.href = global.MGC.getOutUri(process.env.NODE_ENV, 'openOrDownApp') + encodeURIComponent(url)
      //   } else {
      //     global.MGC.checkAndDoLogin(process.env.NODE_ENV, process.env.API_HOST, '1', url)
      //   }
      // } else {
      //   if (this.isyyzshare === '1' && global.MGC.getAPPVer() === '') {
      //     url = regexReplaceUrlParam(url, 'isyyzshare', '1')
      //     initClipboardListen('#go-load-clipboard', url)
      //   }
      //   window.location.href = url
      // }
    },
    /** 点击换一批变化渲染list */
    changeBanch () {
      var remainList = this.allProdList.slice(this.groupSize, this.allProdList.length + 1)// 获得除当前展示数据外的数据
      var newProdList = remainList.concat(this.allProdList.slice(0, this.groupSize))// 将当前展示数据移到最后
      this.allProdList = newProdList
      this.prodList = newProdList.slice(0, this.groupSize)
      sFin2ndpageClick(this.selectedIndex, '换一换', this.secColumn)
    },
    /** 返回是否展示请登录查看 */
    showLogin (prodList) {
      if (prodList.NO_OPEN_KEY === '2' && prodList.FORCE_SHOW_KEY === '2' && prodList.NO_OPEN_CONTXT !== '') {
        return true
      } else {
        return false
      }
    },
    /** 显示更多或者'>'判断 */
    showMore () {
      if (this.secColumn.MORE_TYPE === '2' || this.secColumn.MORE_TYPE === '3') {
        if (this.secColumn.MORE_WORD !== '' && this.secColumn.MORE_WORD === '>') {
          return true
        } else {
          return false
        }
      }
    },
    /** 设置了非公开展示内容并且名称和描述置换时不展示 */
    showDescribe (prodList) {
      if (this.showLogin(prodList) && !this.hasLogin && prodList.NAME_DESCRI_DISPLACE === '1') {
        return false
      } else {
        return true
      }
    }
  }
}
