import {defineConfig} from "vite";
import uni from "@dcloudio/vite-plugin-uni";
import path from "path";
import {visualizer} from "rollup-plugin-visualizer";

// https://vitejs.dev/config/
export default defineConfig({
    build: {
        // sourcemap: true,
    },
    plugins: [
        uni(),
        // visualizer(
        //     {
        //         // emitFile: true,
        //         // filename: "stats.html",
        //         open: true,		// 打包后自动打开页面
        //         gzipSize: true,		// 查看 gzip 压缩大小
        //         brotliSize: true	// 查看 brotli 压缩大小
        //     }
        // )
    ],
    resolve: {
        alias: {
            "@": path.resolve(__dirname, "src"), // 相对路径别名配置，使用 @ 代替 src
        },
    },
    css: {
        preprocessorOptions: {
            less: {
                javascriptEnabled: true,
            },
        },
    },
});
